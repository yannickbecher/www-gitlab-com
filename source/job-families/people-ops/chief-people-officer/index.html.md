---
layout: job_family_page
title: "Chief People Officer"
---

The GitLab team has grown quickly, and plans to continue grow while maintaining our culture and [remote working](/blog/2015/04/08/the-remote-manifesto/), embracing development, and building scalable people-oriented policies. The Chief People Officer is a strategic leader and consultant that oversees People Operations, Recruiting, and Learning & Development functions. In this role you'll be empowered to grow teams, making data driven decisions, and delivering transparent and people first programs.

## Responsibilities

- Provide strategy and oversight of the administration of compensation, benefits, HRIS system
- Work to refine and improve our global compensation frameworks including the [Global Compensation Calculator](/handbook/people-group/global-compensation/)
- Advise the executive team on relevant compliance and employment law and introduce best practices to protect the organization
- Develop and implement effective training programs for GitLab employees and community
- Harness the existing culture and values when building out internal people related policies while devising new ways to reinforce and improve upon the culture
- Manage talent initiatives from recruiting and onboarding to employee retention and offboarding
- Identify and measure organizational KPIs and OKRs for all of People Operations
- Implement global program for evaluating performance and providing feedback
- Advise company on all legal issues related to employment
- Evaluate and manage all third party People Ops vendors and partners
- Manage and grow a people operations team
- Report directly to the CEO


## Requirements

People Operations Experience
- 5+ years experience in a People Operations executive positions or a HR executive position with at least 3 levels reporting to you (directors, managers, and IC).
- Deep understanding and competence around US employment law and best practices, experience with international employment law is preferred.
- Lead a complex organization.
- Improve the effectiveness of the executive team.
- Dealt with complex employer relation issues.
- Deep understanding of recruiting tactics, metrics, and capacity planning.
- Helped develop career paths, promotion reviews and compensation cycles.
- Familiar with the issues that public companies face.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#e-group)
- Ability to use GitLab


Scaling

- Able to plan ahead for future hires and hire according to plan.
- Growth experience, worked at organizations that more than doubled year of year in number of people.

Values

- Share our values, and work in accordance with those values, and an ambassador of our values externally.
- Help reinforce our [values](/handbook/values/#how-do-we-reinforce-our-values ) internally.
- Dogfooding: Willingness to work with git and GitLab, using GitLab workflows within the People Ops team.
- Collaboration: Respond to company criticism quickly, effectively, and visibly.
- Results: Do what we promised to each other, customers, users, and investors.
- Efficiency: Concise in written and spoken communication.
- Diversity: Increase the diversity and inclusion at GitLab.
- Iteration: quickly ship after defining an issue.
- Transparency: Commitment to making People Operations as open and transparent as possible.

All-remote

- An ambassador for the employer brand of their organization.
- Enthusiasm for and broad experience with quickly learning new software tools.
- Become known as the company for all remote and other adopt it.
- Find ways to hire people in as many locations around the world.
- International HR or People Ops experience.
- Efficient result based training programs that also train non-team-members.
- You help reinforce our [communication guidelines](/handbook/communication/).
- Open to doing things differently that at former engagements.

Analytical

- Able to select the right metrics to focus on.
- Experience with how to efficiently collect metrics.
- Make data informed business cases and tradeoffs for decisions.
- Able to articulate and quantify a compensation philosophy logic.
- Able to achieve company wide consistency in bonuses, promotions, raises, refreshes, leadership development.
- Able to measure the effectiveness of interviewers and the caliber of new hires.
- Analytical and methodical approach to performance and compensation reviews.
- Quantitative approach to identifying high-potentials and underperformance.

Table Stakes

- Excellent written and verbal communication skills.
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).

## Performance Indicators

- [12 month team member retention](/handbook/people-group/people-group-metrics/#team-member-retention)
- [12 month voluntary team member turnover](/handbook/people-group/people-group-metrics/#team-member-turnover)
- [12 month PIP Success Rate](/handbook/people-group/people-group-metrics/#regrettable-attrition)
- [Discretionary bonus per employee per month](/handbook/incentives/#discretionary-bonuses)
- [Engagement survey inclusion questions](/company/culture/inclusion/#performance-indicators)
- [New Hire Location Factor](/handbook/hiring/metrics/#new-hire-location-factor)
- [Percent of team members over compensation band](/handbook/people-group/people-group-metrics/#percent-over-compensation-band)
- [Plan vs Actual](/handbook/finance/financial-planning-and-analysis/#plan-vs-actual)
- [Women in company](/company/culture/inclusion/#performance-indicators)
- [Women in leadership](/company/culture/inclusion/#performance-indicators)
- [Women voluntary attrition](/company/culture/inclusion/#performance-indicators)

## Hiring Process

Candidates for this position can expect the interview process to follow the stages below. 
Please keep in mind that candidates can be declined from the position at any stage of the process. 

- 2 interviews with CEO/Co-Founder 
- Interviews with VP, Recruiting & Sr Director, People 
- Interview with Chief Legal Officer
- Interview with CFO
- Interview with EVP R&D (or EVP Product)
- Interview with Chief Revenue Officer (or CMO)
- Interview with BOD member 