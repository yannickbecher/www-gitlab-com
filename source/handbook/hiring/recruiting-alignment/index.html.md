---
layout: markdown_page
title: "Recruiting Alignment"
---

## Recruiter, Coordinator and Sourcer Alignment by Department

| Department                    | Recruiter       | Recruiter's Lead/Manager | Coordinator |Coordinatior's Lead/Manager    |Sourcer     | Sourcer's Lead/Manager|
|--------------------------|-----------------|-----------------|-----------------|----|----|---|
| Board of Directors          | April Hoffbauer   | Dave Gilbert | | |||
| Executive          | Rich Kahn   |Dave Gilbert| Kalene Godoy |April Hoffbauer | Chriz Cruz/ Another Sr. Sourcer depending on the alignment |Anastasia Pshegodskaya|
| Enterprise Sales, North America | Kelly Murdock |Dave Gilbert   | Taharah Nix (Ashley Jones interim) | April Hoffbauer| Susan Hill/Loredana Iluca |Anastasia Pshegodskaya|
| Commercial Sales,	NA/EMEA | Marcus Carter| Kelly Murdock   | Ashley Jones|April Hoffbauer|Susan Hill      | Anastasia Pshegodskaya|
| Commercial Sales, APAC | Simon Poon |Kelly Murdock | Lea Hanopol | April Hoffbauer |Viren Rana |Anastasia Pshegodskaya|
| Channel Sales | Stephanie Keller |Kelly Murdock | Shiloh Barry | April Hoffbauer| Kanwal Matharu |Anastasia Pshegodskaya|
| Field Operations,	NA/EMEA/APAC | Kelly Murdock   |Dave Gilbert| Taharah Nix (Ashley Jones interim)|April Hoffbauer |Susan Hill/Viren Rana - APAC         | Anastasia Pshegodskaya|
| Customer Success, NA/SA | Stephanie Garza  |Kelly Murdock | Corinne Sapolu |April Hoffbauer|J.D. Alex | Anastasia Pshegodskaya|
| Customer Success, EMEA | Debbie Harris  | Kelly Murdock |Bernadett Gal |April Hoffbauer|Kanwal Matharu |Anastasia Pshegodskaya|
| Customer Success, APAC | Simon Poon |Kelly Murdock | Lea Hanopol |April Hoffbauer| Viren Rana |Anastasia Pshegodskaya|
| Federal Sales, Customer Success, Marketing | Stephanie Kellert   |Kelly Murdock | Shiloh Barry |April Hoffbauer|Susan Hill |Anastasia Pshegodskaya|
| Marketing, North America | Steph Sarff   |Dave Gilbert   | Shiloh Barry |April Hoffbauer|J.D. Alex |Anastasia Pshegodskaya|
| Marketing, EMEA | Sean Delea   |Dave Gilbert   | Kike Adio |April Hoffbauer|Viren Rana |Anastasia Pshegodskaya|
| G&A | Maria Gore   | Dave Gilbert   |Heather Francisco |April Hoffbauer|Loredana Iluca |Anastasia Pshegodskaya|
| Quality                   | Rupert Douglas   |Cyndi Walsh                                       | Kike Adio  |  April Hoffbauer  | Caesar Hsiao      |Anastasia Pshegodskaya|
| UX        &   Technical Writing               | Rupert Douglas  |Cyndi Walsh                                        | Kike Adio   |  April Hoffbauer      | Zsusanna Kovacs      |Anastasia Pshegodskaya|
| AMER Support                   | Chantal Rollison    |Cyndi Walsh                                        | Heather Francisco   |  April Hoffbauer    | Alina Moise  |Anastasia Pshegodskaya|
| EMEA Support  | Trust Ogor        |Liam McNally                                      | Bernadett Gal  |  April Hoffbauer       | Joanna Michnievicz       |Anastasia Pshegodskaya|
| APAC Support  | Eva Petreska         |Liam McNally                                     | Taharah Nix (Shiloh Barry interim)   |  April Hoffbauer      | Zsuzsanna Kovacs       |Anastasia Pshegodskaya|
| Security                  | Ryan Demmer    |    Liam McNally                                     | Ashley Jones  |  April Hoffbauer     | Caesar Hsiao      |Anastasia Pshegodskaya|
| Development - Secure/Defend      | Ryan Demmer     |Liam McNally                                      | Lea Hanopol |  April Hoffbauer        | Caesar Hsiao       |Anastasia Pshegodskaya|
| Infrastructure            | Matt Allen     |Cyndi Walsh                                        | Ashley Jones |  April Hoffbauer     | Chris Cruz |Anastasia Pshegodskaya|
| Development - Dev         | Catarina Ferreira   |Liam McNally                                    | Corinne Sapolu  |  April Hoffbauer       | Joanna Michnievicz       |Anastasia Pshegodskaya|
| Development - Dev  | Eva Petreska  |Liam McNally                                          | Taharah Nix (Shiloh Barry interim)|  April Hoffbauer     | Zsuzsanna Kovacs      |Anastasia Pshegodskaya|
| Development - Dev      | Trust Ogor    | Liam McNally                                          | Bernadett Gal   |  April Hoffbauer      | Alina Moise       |Anastasia Pshegodskaya|
| Product Management  | Matt Allen    |Cyndi Walsh                  | Kike Adio |  April Hoffbauer |  Chris Cruz |Anastasia Pshegodskaya|

## Recruiting Operations and Talent Branding Alignment

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Betsy Church and Erich Wegscheider |
| Comparably | Content Management | Betsy Church |
| Comparably | Reporting | Erich Wegscheider |
| Glassdoor | Admin  | Betsy Church and Erich Wegscheider |
| Glassdoor | Responding to Reviews  | Betsy Church |
| Glassdoor | Job Slots | Betti Gal |
| Glassdoor | Content Management | Betsy Church |
| Glassdoor | Reporting | Erich Wegscheider |
| LinkedIn | Admin - Recruiter  | Erich Wegscheider |
| LinkedIn | Seats | Erich Wegscheider |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Recruiting | Betsy Church |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Betsy Church |
| Sponsored Job Boards  | Requests | @domain |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |
