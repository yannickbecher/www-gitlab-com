---
layout: handbook-page-toc
title: "Interviewing"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to Apply for a Position

### External Applicants

The best way to apply for a position with GitLab is directly through our [jobs page](https://about.gitlab.com/jobs/), where our open positions are advertised. If you do not see a job that aligns with your skillset, please keep your eye on the jobs page and check back in the future as we do add roles regularly. Please be advised that we do not retain unsolicited resumes on file, so you will need to apply directly to any position you are interested in now or in the future.

<details>
  <summary markdown='span'>
  To apply for a current vacancy
  </summary>

Please refer to the [country hiring guidelines](https://about.gitlab.com/jobs/faq/#country-hiring-guidelines/jobs/faq/#country-hiring-guidelines) to see if we are able to hire in your location.

1. Go to our [jobs page](https://about.gitlab.com/jobs/) and [view our open opportunities](https://about.gitlab.com/jobs/apply/).
1. Click on the position title that interests you!
1. You will be redirected to the vacancy description and application form, where you will be asked to fill out basic personal information, provide your resume, LinkedIn, and/or cover letter, and answer any vacancy-specific application questions, as well as answer a voluntary Equal Employment Opportunity questionnaire. While the EEO questionnaire has `US` in its title, it's open to all applicants from around the world.
1. Once you have finished, click "Submit Application" at the bottom.
1. Should you reach out to any GitLab team member on any social media platform, that you have not worked with previously or do not know, requesting to be referred, instead of or in addition to applying to our jobs page, you'll receive the following reply: "Thank you for your interest in GitLab. We would much rather prefer you apply for the position you have in mind directly via our [Jobs page](https://about.gitlab.com/jobs/). This will ensure the right GitLab team member reviews your profile and replies back to you! Unfortunately at this time, I can not refer you for the position as we have not had a chance to work together. To ensure we stay [inclusive](https://about.gitlab.com/company/culture/inclusion/), I can also not influence your application".
1. If at any point during the interview process, you send threatening emails, emails containing profanity, vulgar language, or [hurtful labels](https://about.gitlab.com/community/contribute/code-of-conduct/), we will close your application and inform you. We will also not respond any further to you. We aim to hire only people who align with our [values](https://about.gitlab.com/handbook/values/).
1. If your application is rejected because you are insufficiently qualified for the role, you are welcome to reapply to the same position once 6 months have passed. If you gain the skills, experience, or knowledge we outlined in our [feedback to you](https://about.gitlab.com/handbook/hiring/interviewing/#rejecting-candidates), you are welcome to reapply at any time.

</details>

<details>
  <summary markdown='span'>
    Average Time-to-Hire
  </summary>

We strive to be as expeditious as possible in our hiring process. However, the speed of the process can and does vary. The following is our average *time-to-hire* (i.e. Apply to Offer Accept). The *80th Percentile* has been included to account for outliers.

- Overall: **53.06 days**
- Engineering: 60.17 days
- *80th Percentile: 51.17 days*
- G&A: 46.13 days
- *80th Percentile: 29.00 days*
- Marketing: 42.32 days
- *80th Percentile: 33.56 days*
- Product: 73.33 days
- *80th Percentile: 68.60 days*
- Sales: 51.63 days
- *80th Percentile: 37.79 days*

Source: Greenhouse (ATS); September 2019   
</details>

<details>
  <summary markdown='span'>
    Reimbursement for Interviewing with GitLab
  </summary>

If you are invited for an interview with GitLab and you need financial support for your interview, you may be reimbursed for the following:

- Childcare or adultcare for dependents
- Rental costs associated with laptop or computer if you don't own or have access to one
- Transportation to a facility with internet access if your home does not have reliable internet service
- Meeting room costs in a co-working space if your environment is not conducive to an interview
- Reimbursement for loss of hourly pay
- Hearing impaired translation services
- The interest on any credit you obtain to pay for these items upfront
  

To initiate the reimbursement process please email `interviews@gitlab.com`. Additional details found [here](https://about.gitlab.com/handbook/hiring/recruiting-framework/coordinator/#interview-reimbursement-process).
</details>

### Internal Applicants

If you are a [current team member and are interested in applying](/handbook/people-group/promotions-transfers/#department-transfers) for a current vacancy, follow the below

<details>
  <summary markdown='span'>
    Steps
  </summary>

1. Log in to your [Greenhouse account](https://gitlab.greenhouse.io/dashboard) to view "My Dashboard".
1. At the top right corner of your dashboard, click "Hi [Name]" and select "[Internal Job Board](https://gitlab.greenhouse.io/internal_job_board)" from the dropdown. You can also access the internal job board under the ["Helpful Links" section](/handbook/hiring/greenhouse/#your-dashboard) on your dashboard. For more information (and screenshots!), feel free to read the [Greenhouse help article](https://support.greenhouse.io/hc/en-us/articles/200913945-Employee-Access-to-Internal-Job-Board).
1. Click on the position title that interests you!
1. You will be redirected to the vacancy description and application form, where you will be asked to fill out basic personal information, provide your resume, LinkedIn, GitLab username, and/or cover letter, and answer any vacancy-specific application questions, as well as answer a voluntary Equal Employment Opportunity questionnaire. While the EEO questionnaire has `US` in its title, it's open to all applicants from around the world.
1. Once you have finished, click "Submit Application" at the bottom.

</details>

### Adjustments to our interview process

We want our interview process to be accessible to everyone. You can inform us of any adjustments we can make to better accommodate your needs by writing in the text box labeled `Please let us know if there are any adjustments we can make to assist you during the hiring and interview process.` found in the application form.

If you’ve begun the process without sending in an application form, you can ask the Recruiter who’s supporting you through the process to make these adjustments, so we accommodate your needs.

## Typical Hiring Timeline

These steps may vary role-to-role, so please review the hiring process per vacancy.

<details>
  <summary markdown='span'>
    Steps
  </summary>

1. Prior to interviewing, the recruiting team will utilize our Applicant Tracking System (ATS), [Greenhouse](https://www.greenhouse.io/), to identify the most qualified candidates for the vacancy. The hiring team will also source for candidates that may not be actively looking. There are many factors to consider when reviewing resumes. Some of those factors can be aided through technology within the ATS, others require human eyes to evaluate the qualifications. There are several posts that reveal suggestions for reviewing resumes that our team may utilize. [Greenhouse](https://www.greenhouse.io/blog/in-review-whats-the-right-way-to-read-a-resume), [Zip Recruiter](https://www.ziprecruiter.com/blog/10-crucial-things-to-look-for-in-a-resume/) and [The Balance Careers](https://www.thebalancecareers.com/gone-in-thirty-seconds-how-to-review-a-resume-1919139) are three examples.
1. The employment team does a **first round of evaluations** by reviewing candidate resumes. The employment team will also refer to the [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) before moving candidates forward. Disqualified candidates will be sent a note informing them of the [rejection](#rejecting-candidates). There are templates in Greenhouse to assist, but messages can be tailored as appropriate. Make sure the message is professional and respectful.
1. **Pre-screening Questionnaire**: Some candidates will be sent a pre-screening questionnaire by the employment team relating to the position to complete and return to the sender. The questionnaire and answers are kept within the candidate's Greenhouse profile.
  1. Team members who review the pre-screening questionnaire answers should refer to the private GitLab [applicant-questionnaires project](https://gitlab.com/gitlab-com/people-ops/applicant-questionnaires) that holds guides on how to review each of the questionnaires. Candidates who receive an assessment are moved to the "Assessment" stage in Greenhouse by a member of the Recruiting team and sent the questionnaire. The recruiting team also chooses a member of the hiring team to review the responses once they are submitted.
  1. When a candidate returns their assessment, the recruiting team member who sent the assessment and the hiring team member who was chosen to review it will receive a notification. Once a reviewer submits the feedback for the assessment in Greenhouse, the recruiting team will be notified.
  1. Candidates that have satisfactory assessment results may be invited to a screening call. Disqualified candidates will be sent a note informing them of the rejection.
1. [**Screening call**](#conducting-a-screening-call):
  1. If the candidate qualifies for continued consideration, one of our [recruiters](/job-families/people-ops/recruiter/) will conduct a screening call using Zoom and scheduling it via Greenhouse.
  1. A member of the employment team will move the candidate to the "Screening" stage in Greenhouse. They will reach out to the candidate to collect their availability and then send out calendar invitations to both the interviewer and candidate.
  1. Our [recruiters](/job-families/people-ops/recruiter/) will do a screening call;
depending on the outcome of the call, the recruiting team or manager may either [reject a candidate](#rejecting-candidates) or move the candidate to the team interview stages in Greenhouse. Note: A resume is required before scheduling candidates for Customer Success roles.
  1. The recruiter will wait 5 minutes for the candidate to show up to the appointed video call link, which is always shared with the candidate via email. If the candidate does not show up to the interview or reach out in advance to reschedule, the candidate will be classified as a "no show" and be disqualified.
  1. The recruiter, hiring manager, or candidate can terminate the discussion early at any point during the interview if either party determines that it isn’t a fit. Be as transparent and honest as possible and provide feedback.
  1. After the screening call, the recruiter will verify that the candidate is not on any known [Denied Party List](https://www.export.gov/csl-search). If the candidate is on a list, the application process will end.
1. **Technical interview (optional)**: Certain positions also require [technical interviews](/handbook/hiring/interviewing/#considerations-for-interviews-with-technical-applicants).
1. **Behavioral interview**: Some roles include a behavioral interview with a team peer or leader. Behavioral interviews may be conducted as [panel interviews](/handbook/hiring/interviewing/panel).
1. **Further interviews**: All interviewers will assess the candidate's values alignment by asking behavioral questions and scoring the values alignment as part of their feedback form in Greenhouse. Additional interviews would typically follow the reporting lines up to the CEO. For example the technical interview may be conducted by an individual contributor, with subsequent interviews being conducted by the manager, director, executive team member, and then potentially the CEO.
  * **Interviewers will follow the same "no show" policy as the recruiters.** If a candidate does not show up or reach out to the team, they will be disqualified.
  * **All interviewers will complete interviewing training,** which will be assigned to them from someone on the recruiting team; generally the Candidate Experience Specialist. Interview training issues can be found in the [People Group Training issue tracker](https://gitlab.com/gitlab-com/people-ops/Training/issues).
  *   The individual who created the issue will receive a notification when the issue is closed. The issue should be checked to ensure all tasks are completed. If an issue is closed before all tasks are finished the issue will need to be reopened and tag the assignee that items are missing.
1. **References**: The hiring manager or the hiring team will contact [references](/handbook/hiring/recruiting-framework/hiring-manager/#step-19hm-complete-references) for promising candidates. References will be collected towards the end of the interview stage for final candidates, and they must be checked before an offer is made.
    * Three references will be requested, but at least two references need to be completed, and at least one needs to be a past manager.
    * The recruiting team will move the candidate to the "Reference Check" stage in Greenhouse, and email the candidate to request their references' contact details.
    * After the reference checks are completed, the person performing the reference check will input a scorecard in Greenhouse with their findings.
1.  At the same time as starting the reference check process, the recruiting team will [start the background check process](/handbook/people-group/code-of-conduct/#background-checks).
1.  **Offer package**: After reference calls are completed successfully, the recruiting team moves the candidate to the "Offer" stage and submits the [offer package](/handbook/hiring/offers/#offer-package-in-greenhouse) in Greenhouse for [approval](/handbook/hiring/greenhouse/#approval-flows).
1.  **CEO interviews**: The CEO may choose to interview candidates in a last round interview after reviewing the offer package.
1.  The recruiter, hiring manager, executive, or CEO should make an **offer** verbally during a call with the candidate, and it will be followed with an official contract as described in [preparing offers and contracts](/handbook/contracts/#how-to-use).
1.  The recruiting team will, if applicable, add language to the contract that states that employment or engagement is contingent on a valid work permit or visa. A start date should factor in that the approval of a new work permit may take several weeks.
  * Note that, when scheduling a start date, People Ops Specialists requires at least 4 days notice from the receipt of an executed offer until the GitLab team-member's proposed first day.
1.  The manager follows up to ensure that the offer is accepted and that the contract is signed.
1.  Candidate Experience Specialists [starts the onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).
1.  The hiring manager considers [closing the vacancy](/handbook/hiring/vacancies/#closing-a-vacancy).

</details>

## Inclusive Interviewing

The GitLab team understands the importance of inclusive interviewing, and we strive to ensure our hiring team is well versed in every aspect of diversity, inclusion, and cultural competence. A positive and comfortable candidate experience is priority.

<details>
  <summary markdown='span'>
    Best practices
  </summary>

* **We will make our best effort so that each candidate's full interview panel has at least one non-male GitLab team-member on it.**
* **The following teams will _always_ have one non-male team member in the same department as the candidate on the interview panel.** We are working to make it so that every team practices our value of diversity and inclusion in their hiring practices. We are improving, but we aren't there yet. According to our value of iteration, we aren't waiting for a system to be in place for the entire company, but instead are practicing diversity and inclusion now by listing the teams that can currently commit to this practice. Hiring managers can make MRs to add their team to this list when they are able to meet this requirement.
  * Business Operations
  * Content Marketing
  * Corporate Events Marketing
  * Digital Marketing Programs
  * Field Marketing
  * Marketing Ops
  * UX
  * Quality

</details>

## Conducting a Screening Call

Calls can last anywhere between 10 and 30 minutes, depending on the conversation.

<details>
  <summary markdown='span'>
    Screening calls
  </summary>

Example questions include:

1. Why are you looking for a new position?
1. Why did you apply with GitLab?
1. What are you looking for in your next position?
1. Why did you join and leave your last three positions?
1. What is your experience with X? (for each of the skills listed in the position description)
1. [STAR Method](https://www.themuse.com/advice/star-interview-method) questions and simple technical or skills-related questions
1. What is your current location and do you have any plans to relocate? (relevant in context of compensation, country-hiring guidelines, and in case an offer would be made)
1. Do you require visa sponsorship or a work permit to work for GitLab? Or do you require the work permit to be transferred to GitLab? If the answer is yes, we will not be able to proceed. You can refer to this [page](/handbook/people-group/visas/) for further clarity
1. What is the notice period you would need if you were hired?
1. At GitLab, we are committed to paying competitively and equitably. Therefore, we set our offers based on market pay rather than a candidate's pay history. So that we can address any gaps in expectations early on, could you share your compensation expectations?

If a candidate is either sourced or referred, please note this in the `Public Notes` section of the scorecard.  This will give future interviewers more context for questions like "Why are you interested in GitLab".

At the end of the screening call, you will tell the candidate what the next steps will be. You will also inform the candidate of the hiring timeline and the average number of days that it takes from application to acceptance for the role being discussed.
</details>

## Moving Candidates Through The Process

In an effort to streamline the hiring process, improve the candidate experience, and hire talent faster, best practice is to coordinate interview times so that candidates can complete the process within 2 weeks. Just as if we were to interview candidates in-person at an office, we wouldn’t make them come back 3, 4, or even 5 times. The initial screening call and optional CEO interview are not considered to be part of the 2-week goal.

<details>
  <summary markdown='span'>
    Best practices
  </summary>

**Those on the interview team should prioritize the interview in their schedules.** If it means you have to miss an already scheduled or recurring meeting, please consider participating in the interview a priority and reviewing notes from the missed meeting agenda afterwards instead. Hiring an amazing team is critical for GitLab, and how we spend our time shows where our priorities are.

**Maintain candidate confidentiality.** All candidate names and details are kept confidential within the hiring team to avoid bias or the potential to jeopardize a candidate's current employment as well as to maintain data protection. The only people who should have access to details about candidates are Recruiting, People Ops, the hiring manager(s), approved interviewers or reviewers within that team, the executive of the department, the legal team, the CFO, and the CEO.
* Exceptions to this rule include when an existing team member refers the candidate or when the candidate intentionally reaches out to someone at GitLab. Even then, the team member should know only their name, that they are interested in GitLab, and, if they are a referral, what stage they are in.
* Do not include identifying personal details in your feedback notes for a candidate.
* Anytime you want to discuss a current, past, or potential candidate, please do so privately (whether in a private Slack channel/message, email, or within Greenhouse). If you have access to it, you can also provide the direct Greenhouse link and avoid mentioning names or identifying details.
* Emails from the candidate are synced on our ATS, and for that reason, the entire hiring team for that position has access to it. Remember to ensure any sensitive information is marked as secret/private in the candidate profile.

**Remember to inform candidates about what stage they are in.** For example, if in the hiring process for the particular position / team you've agreed that there will be four stages, be sure to inform the candidate of where they are in the process during each call / stage. To better manage candidates’ expectations, at the end of the interview, let them know what stage they are in as well as what the next step/stage will be **if** they do pass this interview.  Considering we are speaking with other candidates, they can expect to hear back within a couple of days.  Some brief feedback from the previous stage can also be included to help the candidate gauge their progress. If there will be additional or fewer stages than expected, be sure to let the candidate know so they are aware of where they are in the process.

**The process can differ from team to team and from position to position.** If a candidate submits a resume to a particular open position and is being considered for another open position, send a short note to update the candidate and get their approval as well as to inform them that their process may be slightly different than previously expected or delayed. If the roles are on different teams, the candidate will ideally only move forward with one, depending on their interests and qualifications. If the candidate is being rejected for one or all of the positions they applied for, they will be notified of which vacancies they are being rejected for.
  * If at some point during the interview process it is uncovered that a candidate is better suited for another open position the interviewer should @ mention the Recruiter assigned to the candidate and provide feedback regarding what other position(s) the candidate should be considered for.  The Recruiter will then loop in the new Recruiter(s) and Hiring Manager(s) for the said position(s) and if there is interest complete a warm handoff of the candidate to the new Recruiter(s). 

**Recruiters will schedule the next person in the process.** Someone on the recruiting team will move candidates forward to the next person in the hiring process if the candidate has received positive feedback.

**Compensation is discussed at start and end but not in between.** Compensation expectations are asked about during the [screening call](#conducting-a-screening-call). If the expectations seem unworkable to the manager or recruiter (based on what had been approved by the compensation committee at the [creation of the vacancy](/handbook/hiring/vacancies/#vacancy-creation-process)), then the recruiter can send a note to the candidate explaining that salary expectations are too far apart, but they should also ask how flexible the candidate is and if they would consider adjusting their expectations. If expectations are aligned, then the topic of compensation should not re-surface until an [offer is discussed internally](#offer-authorization). Following this guideline avoids conflating technical and team interviews with contract discussions and keeps the process flowing smoothly.

If the manager has a question about compensation, please ping the People Ops Analyst for review. If the question needs to be escalated, the People Ops Analyst will add the Chief People Officer to the conversation.

**An approval team authorizes all offers.** The manager proposes a suggestion for an offer (including bonus structure if applicable, etc., using the [global compensation framework](/handbook/people-group/global-compensation)) as a private comment in Greenhouse and informs the recruiting team on its details depending on what is applicable. The recruiting team will create an [offer package](/handbook/hiring/offers/#offer-package-in-greenhouse) to present to an approval chain consisting of the People Business Partner, executive of the division, and Chief People Officer for approval. Verbal offers should not be extended to the candidate until the offer is approved. The CEO may choose to interview the candidate, and any offers given before the CEO's approval are premature.
</details>

## Conducting a GitLab Interview

Interviewing is hard for both sides. In less than one hour, you both need to get to know each other and make a decision about whether or not you would want to work with this person. The following is an effort to provide a set of guidelines to make interviewing a bit less traumatizing for all involved parties.

<details>
  <summary markdown='span'>
    Interview Training and Preparation
  </summary>

So you are about to interview folks for a job at GitLab? Please take a moment to carefully read
[this document on keeping it relevant and legal, including a self-test](https://docs.google.com/document/d/1JNrDqtVGq3Y652ooxrOTr9Nc9TnxLj5N-KozzK5CqXw) (please note this document is internal to GitLab while we edit it to make it fit for general audiences). Be aware of the kinds of questions you can and can't ask. For example, if there is a gap in employment history on a CV, you **may** ask the candidate what they did during that time to keep their skills current. You may not ask why they were absent from work as it may be related to a medical or family issue, which is protected information.

Keep in mind, if a candidate is located in the same location as an interviewer, in-person interviews or meetings are reserved for candidates if an offer is approved or if the candidate is hired.
**Anyone wanting to do in-person interviews should reach out to People Business Partners to discuss before hand and have a clear reason which should be documented in their Greenhouse profile.**

When discussing the interview process with candidates, it is encouraged to set the context for the interview by providing links to the handbook, explaining if the interview will be a behavioral or technical in nature, etc. GitLab team-members should not, however, prep candidates for specific interview questions.

New internal interviewers will partake in [interviewing training](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/interview_training.md), which will be assigned by the recruiting team. As part of the training, team members will shadow an interviewer and be shadowed by one in order to make sure all GitLab team-members are following our interviewing processes and creating an excellent candidate experience. The interviewer who will work with the team member should be aligned with either their timezone or the role they'll be helping interview for. Feel free to ping `@gl-hiring` in your training issue if you are not sure which interviewer to contact, or send a message in the `#recruiting` channel in Slack.

Interviews should not be recorded. For interview training, we encourage our GitLab Hiring Managers to conduct mock interviews internally, or have no more than one GitLab team member at a time shadowing live interviews.

It is typically expected for new hires to focus on and complete their onboarding for at least two weeks before being part of an interview team for any vacancies. There may be extenuating circumstances where a team member needs to participate in interviewing sooner than this, but they should always complete the [interviewing training](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/interview_training.md) and discuss the vacancy thoroughly with their manager and the recruiter prior to being on an interview team.

Remember, interviewing candidates is everyone's job as part of our collaboration value! You may be asked to participate on an interview team, as we continue to hire great talent.
</details>

<details>
  <summary markdown='span'>
    Best practices
  </summary>

#### Before The Interview

* Review the Job Family 
* Brief with the Hiring Manager on topics/questions to cover during the interview
* Review the scorecard in Greenhouse and make sure it covers the topics/questions you plan to cover during the interview
* Review the candidate's resume and note areas you may want to dig into for clarification - writing a good resume is an art, and not many people master it. When you read a resume, look for evolution rather than buzzwords, and, if something sparks your curiosity, prepare to ask about it during the interview.
* If the process before the interview is taking too long, reach out to the candidate, apologize, and explain what is going on. It is really frustrating to not hear anything from the other side just to have the conversations resume later as if nothing had happened. Show respect for the candidate's time.
* Make sure you're up to date on our latest headcount, notable awards, messaging, and other facts about working at GitLab. Take a look at these [talent brand resources](/handbook/people-group/employment-branding/#talent-brand-resources) to help guide your conversation with candidates.

#### Illegal Interview Questions

* Refrain from asking any questions related to protected classes (for example: age, race, religion, sexual orientation, marital status, pregnancy status)
* Protected classes differ based on the country of the candidate
* To find out more about discrimination laws specific to the country of a candidate you're interviewing, you can reach out to the legal and/or recruiting team
* Asking questions related to any of the protected classes is not only illegal, but shouldn’t be asked to determine if someone is qualified for a role 
    * Some examples of illegal interview questions include: 
        * So you mentioned you live with your partner earlier, are you married? 
        * How old are you by the way? 
* In general, stay away from any questions and conversations around protected classes, and focus instead on the job duties 
* If a candidate does bring up personal information during an interview related to a protected class, it's OK to acknowledge their comment and get the interview back on track
    * Some suggestions on steering the interview back to appropriate topics: 
        * So, we were just talking about your role’s current responsibilities. How technical are you hoping to stay in your next role? 
        * I have a few more questions that I’d love to get to during our call today. Tell me a little bit more about how you’ve achieved quota in the past. 
    * This information should not be included in your Greenhouse feedback as it does not pertain to the functions and responsibilities of the role 
    * This information should not be used to determine if you are a yes or a no to their candidacy for the role
    * If protected class information was to be documented in a candidate's Greenhouse profile, that’s findable information and could be used against GitLab in court. 

#### During The Interview

* Show up prepared and on time. (See "Before The Interview" section above)
* Build rapport and put the candidate at ease.  Introduce yourself, tell your GitLab story, ask how the candidate is doing- banter/small talk is ok.  
* Talk a bit about the role (why it is open, what the job entails, etc.)
* Set expectations on how the interview will run 
* As candidates move through the interviewing process, interviewers take notes within Greenhouse. As they move through the process, interviewers have the opportunity to review any specific notes previous interviewers have left for them, although the full feedback notes from previous interviewers are obscured from current ones in an effort to avoid creating bias. Hiring managers, executives, and people ops are able to see all feedback notes at any time.
     * Let the candidate know you are taking notes
     * Be an active listener
          * Concentrate on the candidate - remove all distractions (turn off  notifications on your phone, slack, email, etc.)
          * Look at the candidate - make sure to look not your camera - not another screen
          * Try not to interrupt - Validate your assumptions by explaining what you understood, and allow the candidate to correct your understanding of the story.
          * Silence is golden 
* Cover the prepared topics/questions without being mechanical
     * Setup the interview plan in GreenHouse ahead of the interview to ensure all the areas you want to cover are in the scorecard 
     * Try to cover the prepared topics, but keep the conversation smooth - it is ok to skip around 
     * Redirect the conversation as needed - you are in charge of keeping the conversation focused and timely 
* There is an unbalanced power relationship during the interview, and interviewers should be mindful of this fact. The interviewer is in a powerful position: they will decide if the candidate will move forward or not. Be as friendly and approachable as you can. Be frank about what is going on, and set clear expectations: tell it like it is. This has the added value of getting people comfortable (over time) and allows you to get much better data.
* Communication is really hard, so don't expect perfect answers. Every person is different, and they may say things differently than what you expect or how you might say them. Work on interpreting what they are trying to say rather than demanding them to explain it to you. Once you have an answer, validate your assumptions by explaining what you understood, and allow the candidate to correct your understanding of the story.
* Don't go checking for perfect theoretical knowledge that the interviewee can google when needed during regular work or expect them to master over the course of a 30-minute conversation a problem that took you 2 months to dominate. Be fair.
* Aim to know if, at the end of this interview, you want to work with this person.
* Interview for soft skills. Really, do it! Pick some behavioral questions to get data on what the candidate has done before and how their behavior aligns with the [company values](/handbook/values/). We are all going to be much happier if we naturally agree on how things should be. You will be asked to evaluate how the candidate's values align with our own in your feedback form, and asking behavioral questions is the best way to assess this.
* Consider having more people interviewing with you since different people see and value different things. More data helps you make better decisions and is a better use of interview time for both the candidate and the company.
* Always encourage the interviewee ask questions at the end, and be frank in your answers.
* Be willing to discuss what went well and any concerns you may have 
* Manage expectations - Discuss next steps and timelines 
* Thank the candidate for their time

#### Considerations for Interviews With Technical Applicants

[Tips On How To Prepare For Your Technical Interview](/handbook/hiring/interviewing/technical-interview/)

1. Try to get a real sample of work, which we
typically do for developers during a technical interview. Avoid puzzles or weird algorithm testing questions. Probing for data structures is fine as long as it is relevant to the job the person is going to do.
1. Be mindful of the background of the candidate. Someone who knows 10 languages already (and some languages in particular, Perl for example) may pick up Ruby in a second if given the right chance. Don't assume that someone with a Java background will not be capable of moving to a different stack. Note that individual positions may have stricter requirements; the Backend Engineer position [requires Ruby experience](/job-families/engineering/backend-engineer/), for example.
1. Consider including non-engineering GitLab team-members in the interview to ask soft skills questions. Because technical people should be capable of talking to non-engineering people just fine, we should assess the candidate's ability to do so.

#### If a Candidate Re-Applies After Rejection

There may be situations where a candidate goes through the interview process but is not offered a position at GitLab, but we are interested in revisiting the candidate later on for either the same or different position, or a candidate reapplies later on and is a better fit at that time than they were earlier. If the recruiter for the role and the hiring manager are aligned that the previous rejection reason is not still applicable, the recruiting process may resume from where it previously was, with the addition of a few interviews at the discretion of the recruiter and hiring manager. If it has been more than 6 months or the interview process has significantly changed, it may be more prudent to restart the interview process entirely. Meanwhile, due to GDPR, we anonymize all personal candidate data 60 days after they are archived in Greenhouse. With the candidate's consent, it is possible to search for the anonymized candidate's previous profile by using filters/keywords to review the previous rejection reason and interview feedback and to un-anonymize the candidate in order to continue the hiring process.
</details>

<details>
  <summary markdown='span'>
    Candidate Performance Evaluation
  </summary>

The goal of behavioral questions is to get the candidate to share data on past experiences. Previous behavior is considered the most effective indicator of how a person is going to act in the future. It is important to remember that skills and knowledge can be learned easier than habitual behaviors can be changed, especially when candidates are unaware of the impact of the undesired behaviors.

The questions are usually in the form of:

>"Can you tell me about a time when...?"

The kind of answer that we are looking for is to get a story that is structured following the **Situation, Task, Action, and Result (STAR)**. Ask for an overview, an executive summary, of the case at hand. Try to avoid lengthy answers from the candidate at this stage.

Some things to pay attention to:

* What the candidate chose to highlight in their response as important
* Is it clearly explained? Is the story well told? If it is a technical story and you are a non-technical interviewer, are things being explained in a way that is easy to understand?
* Is there a result or was the story left unfinished? Is it still going on?
* Was the result measured in any way? How does the candidate validate the result matched the expectation? Was there an expectation set to begin with?

There is no right answer; what matters here is to listen to the candidate and gather data on how they are telling the story.

Once you have your notes, tell the candidate what you understood, repeat the story, and let them correct you as needed.

After gaining a high-level understanding of the case, we will want to dive deeper into the details. The objective of this step is to understand and detail the exact contributions a candidate has made to an effort which led to results. We will take a reverse approach to the STAR question structure presented earlier.

The key to analyzing each of the reverse-STAR steps is to ask _What, Why, How, and Who_ at each step of the process. This will let the candidate paint a very clear picture of the situation, their ownership of the idea/solution, and their decision process in key pivotal moments. Reverse the order of the STAR structure, and drill up from results to the situation as a whole. Find the answer to the following questions:
1. What was the goal to achieve or the problem to overcome? What was the expectation? Was the goal defined from the get-go?
1. How was the result measured? Why was it measured that way?
1. What steps or process was followed to achieve the result? List them together with the candidate
1. Who else was working with the candidate? Was the candidate working alone?
1. What role did the candidate have in the team if they did not work alone on the project? Was the candidate in charge of specific tasks? Who decided on task assignments? What was their impression of the tasks? How were the tasks decided on?
1. For the tasks discussed above, understand if there were resources that helped the candidate and at what capacity. How were those chosen and why?

These questions can be quite unbalancing and can increase the stress during the interview. Again, be kind and help the candidate understand what you are looking for, and provide an example if one is needed when you notice the candidate is blocked.

It can also happen that the candidate does not have a story to share with you; that is okay. It's just another data point that should be added to the feedback (I failed to get data on ...). Just move to the next question and be sure to have a few questions as a backup.
</details>


#### Interview feedback

In Greenhouse, you will use an "interview kit" when interviewing a candidate, which has text for feedback and scorecards for skills and values. 

The bottom of the feedback form will ask for an overall recommendation on if you want to hire this person or not; please do leave a score for each candidate, and read our [handbook page discussing the scorecards and best practices](/handbook/hiring/greenhouse/#scorecards).

Scoring is defined as follows:

##### All divisions but Engineering

* `Strong Yes` - Very likely to hire (meets most requirements, aligns with values)
  * You have no significant questions and are confident (to the best of your knowledge with the information at hand) that the candidate would succeed in the role.
  * Candidates who receive Strong Yes feedback may have later steps in the hiring process parallelized by the hiring manager and should almost always be extended an offer in the absence of negative feedback or concerns.
* `Yes` - Semi-inclined to Hire (may meet some requirements, has some yellow flags)
  * You think the candidate is qualified for the role, but you are either not certain, or still have some outstanding questions that should be addressed at a later stage. 
  * By default, Yes candidates should move forward in the interview process, although hiring managers should use their judgement and look for patterns in any outstanding questions or areas where it's difficult to get insight into a particular candidate. It may be appropriate to reject a candidate who does not receive stronger feedback through our process.
* `No` - Not likely to hire (meets few requirements, has many yellow flags, may not align with values well)
  * You think the candidate is unqualified for the role, but have some doubts or would be willing to be swayed by some strong feedback from other interviews. 
  * Hiring managers should use their discretion to determine whether or not to continue the interview process with any candidate that receives a No vote.
* `Strong No` - Would not hire (does not meet requirements, red flags, not aligned with values)
  * You are certain this candidate is not qualified and/or not a good fit for the role. 
  * Candidates who receive a Strong No vote should almost always be rejected immediately by the hiring manager.

##### Engineering division

* `Strong Yes`
  * Extends `Yes`
  * Meets an unusually large proportion of our "nice to have” criteria for the role
  * Brings interesting qualities that we were not necessarily looking for
* `Yes`: All must-haves criteria that were evaluated in the interview were present
* `No`: One, or more, must-have criteria that were evaluated were found to be missing
* `Strong No`
  * Extends `No`
  * The candidate demonstrated clear opposition to either our Collaboration, Diversity, or Results values
  * The candidate demonstrated unwillingness to learn our Efficiency, Iteration, or Transparency values


## Rejecting Candidates

<details>
  <summary markdown='span'>
    Rejecting and Feedback
  </summary>

1. At any time during the hiring process the candidate can be rejected.
1. At application review stage, the employment team along with the hiring manager (where applicable) will provide a specific reason for the rejection rather than a default rejection email. These specific reasons may be via a template or a personalised note. Examples could be, tenure, technical skills fit or resume missing must have requirements for the role. 
1. If a situation arises in which the role has been filled within 24 hours of a candidate's interview, we will **not** cancel the interview. The interviewer will take the call and be transparent with the candidate about the situation upfront. It would be a good use of time to discuss other roles, future roles or questions about GitLab in general.
    - If the candidate's interview is outside 24 hours, the interview will be deleted in Greenhouse before the candidate is rejected and notified.
1. The candidate should always be notified if and when they've been rejected. The employment team is primarily responsible for declining the candidate, but the hiring manager should be prepared to let the candidate know why they were declined if they had progressed to the team or manager interviews. The hiring manager can also share this feedback with the recruiting team, who will relay it to the candidate.
1. When rejecting a candidate in Greenhouse, use the scheduling option to send out the rejection letter 2 days after you reviewed the resume or performed the interview to ensure that the candidate feels that their application was properly considered.
1. We only provide feedback for candidates who have passed the first interview stage and met with the team or hiring manager. If the candidate asks for further feedback, only offer frank feedback. This is hard, but it is part of our [company values](/handbook/values).
    * All feedback should be constructive and said in a positive manner. Keep it short and sweet.
    * Feedback should always be applicable to the skill set and job requirements of the position the candidate applied and interviewed for.
    * If you feel uncomfortable providing feedback for whatever reason, reach out to the recruiting team for assistance.
    * Suggested feedback format: "The reason we don't think you're the best match for this position is __ . We are impressed with your skill in __ . That we decline you doesn't mean that you are not a good fit for this position. We receive over 1000 applications per month and have to decline almost all candidates. Both Facebook and Twitter [rejected the founder of Whatsapp](https://www.forbes.com/sites/georgeanders/2014/02/19/he-wanted-a-job-facebook-said-no-in-a-3-billion-mistake/#92e776752441). We don't think we'll do any better and look forward to hearing from you after landing a better job or starting a successful company. Thanks for your interest in working at GitLab." This format can be used as a guideline to help candidates understand our decision, but should be personalized / customized to fit each situation. Personalization in communication with candidates is encouraged.
1. If people argue with the feedback that we provided:
    * Do not argue with or acknowledge the validity of the contents of the feedback.
    * Share their feedback with the people involved in the interviews and the decision.
    * Template text: "I've shared your feedback with the people involved in the interviews and the decision. We do not expect to revert the decision based on your feedback. In our hiring process we tend to err on the side of being too cautious. We would rather reject someone by mistake than hire someone by mistake since a wrong hire is much more disruptive. Organizations can reject people with great potential (https://www.adweek.com/digital/whatsapp-facebook-twitter-brian-acton/) so please don't be discouraged from seeking a great job."
1. The employment team may send out an inquiry to candidates to gather feedback after they have exited the hiring process.
  * The recruiting team will review all feedback and use it to improve the hiring process.

</details>

## Candidate Experience

We [recorded a training](https://www.youtube.com/embed/ng_VQseo5vo) on the subject of Candidate Experience.

## How We Conduct Remote Interviews on a Global Scale

<details>
  <summary markdown='span'>
    How We Work During Holidays
  </summary>
  
  As an all-remote, global company, GitLab gives all team members the flexibility to determine their [time away from work](/handbook/paid-time-off/), including the holidays they observe. 
  There will likely be holidays that our candidates observe that a recruiter, hiring manager, or candidate experience specialist may not. 
  If there's a time during the year where a significant portion of the company will be taking time off, we'll be sure to send a communication to all active candidates to let them know of a possible delay in communication. 
  Please note that not every member of the team will be observing that holiday, so you may still receive emails from them.   
</details> 

<details>
  <summary markdown='span'>
    How We Schedule Interviews on a Global Scale
  </summary>
  
   1. Once a candidate has completed the a screening call with the recruiter, the candidates are moved onto the Team Interview stage. This is the most difficult stage to schedule for as it involves many different people most of the time spread all across the world. 
   1. The candidate will receive an email from either the Recruiter or the Candidate Experience Specialist requesting availablility to schedule interviews. The best thing a candidate can do is to give us many different days and times spread across at least 2 weeks. This will prevent us having to reach out again asking for more availability. **Note** Sometimes if the interviewer has a schedule that is difficult to fit interviews into, there will already be suggested times for the interview to take place in the availability link. 
   1. After receiving the candidates availability, the Candidate Experience Specialist will compare it to the interviewers working hours. This is why it is very important for candidates to give multiple times and days for availability as sometimes we are only able to find 1-2 hours that overlap timezones.
   1. Once a good time is found on the interviewers calendar, the Candidate Experience Specialist will send the interviewer an invite via the calendar and the candidate an interview confirmation via email, all using our Application Tracking System (ATS); Greenhouse. 
   1. There is one section of the interview confirmation email the candidate should pay extra close attention to, the timezone. Greenhouse, our Applicant Tracking System (ATS), defaults the timezone in the interview confirmation to the timezone the Candidate Experience Specialist is located in. **However, there will always be an attached calendar invite that shows the interview time in the candidates home timezone so they can add it to their own calendar.**  
   1. If the interviewer or candidate should need to reschedule, they should email their Candidate Experience Specialist, or follow the special instructions in the confirmation email if the interview is within 24 hours.

</details>

## Reference Check Process

The recruiting team will ask candidates for references via email. The hiring manager may also decide to conduct backchannel references. More information on how we conduct reference checks can be found on the [Recruiting Process - Hiring Manager Tasks](/handbook/hiring/recruiting-framework/hiring-manager/#step-19hm-complete-references) page.

## Background checks

Concurrently with the reference checks, the recruiting team will begin a [background check](/handbook/people-group/code-of-conduct/#background-checks) on the candidate. When the recruiting team initially requests the candidate's reference details, they will also begin the background check process and inform the candidate.

## After the Interviews

If, at the completion of the interview process, both the candidate and the hiring team wish to move forward with the candidate's application, an [offer](/handbook/hiring/offers/) will need to be prepared.