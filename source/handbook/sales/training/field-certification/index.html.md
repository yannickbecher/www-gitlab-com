---
layout: handbook-page-toc
title: "Field Certification Program"
---

# Field Certification Program 
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}




# Field Certification Program Overview 
To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a role based certification program that includes functional, soft skills, and technical training for field team members.  

Certification badges will align to the customer journey and critical “Moments That Matter” (MTMs) as well as role-based competencies that address the critical knowledge, skills, behaviors, processes, content, and tools to successfully execute each MTM.

### What is certification? 
The act of validating competency of an individual to prove that:
* Knowledge has been transferred (Do you know it?)
* The learner can apply that knowledge in simulated & live scenarios (Can you do it?)
* The learner has taken ownership of effective skill execution and can demonstrate proficiency over time (Can you do it independently and continue to execute over time?)

### Why Certification? 
Certification programs allow us to validate GitLab skills and knowledge, recognize individual mastery, and motivate continuous learning and skills growth all aligned to expectation setting and performance discussions. 

## Audience 
The Field Certification Program will include:
* Functional and soft skills training for Strategic Account Leaders and Account Executives  
* Functional and soft skills training for Solution Architects and Technical Account Managers

## Certification Levels 

| Level Name        | Skills Level    | Course/ Exam Numbering Scheme | Color Highlight | Validation Description                                                      |
|-------------------|-----------------|-------------------------------|-----------------|-----------------------------------------------------------------------------|
| Level 1: Learning | Knowledgable    | 101                           | Yellow          | Has essential skills                                                        |
| Level 2: Growing  | Intermediate    | 201                           | Orange          | Has deep skills in specialized area                                         |
| Level 3: Thriving | Advanced        | 301                           | Purple          | Has broad skills across multiple areas                                      |
| Level 4: Expert   | Mastery         | 401                           | Red             | Possesses "professional" level certification                                |

## Certification Assessments
To achieve GitLab “certified” status, candidates must complete both the online written exam (knowledge) and practical assessment (skill) with a passing score on each exam.
* Anyone can sign up and take the written exam but only candidates who successfully pass the online written exam will be granted access to the practical assessment
* The practical assessment will require team members to demonstrate the ability to perform Moments that Matter to GitLab’s standards 

## Moments That Matter (MTM)
Moments That Matters are episodes where a field team member creates engagements with a customer that make a lasting emotional impression on how they feel about them.
They are based on the concept explained in "Blueprints for a SAAS Sales Organization: How to Design, Build and Scale a Customer Centric Sales Orgnization" by Jacob Van Der Kooj and Fernando Pizarro. 

## GitLab Moments That Matter 
Please note: The GitLab Moments That Matter are still being ironed out. To see that latest, please check out the slide below. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQvjB6E9JlplzwqBHVv2fFGAEGZwqjg4AZQO-p_DqjX7znjZGOC_q2-d2xCbwr2LbfXCmyOvVxcirYb/embed?start=false&loop=false&delayms=3000&slide=id.g711c708e57_1_7" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Roadmap
*Coming soon*






