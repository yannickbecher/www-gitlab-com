---
layout: markdown_page
title: "Sales & Customer Success Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Goals of Sales & Customer Success Onboarding
Sales onboarding at GitLab is a blended learning experience (virtual, self-paced learning path paired with an immersive, hands-on in-person workshop called Sales Quick Start) focused on what new sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** within their first 30 days or so on the job so they are "customer conversation ready" and they are comfortable, competent, and confident leading customer engagements before the end of their first several weeks on the job. There are some formal learning components with a heavy emphasis on social learning (learning from others) and learning by doing to help bridge the knowing-going gap. Some of our learning goals pertaining to Sales Onboarding include:

*  Create relationships with colleagues and mentors and gain a strong understanding of GitLab culture
*  Develop a high-level understanding of the DevOps industry and GitLab competitors
*  Understand GitLab’s value framework (including, but not limited to, products, services, and GitLab values)
*  Understand personas and learn how to be audible ready with each of them on a discovery call
*  Gain a functional understanding of GitLab’s sales cycle and customer journey
*  Learn how to utilize company resources (including, but not limited to, our handbook, the GitLab tool itself, Chorus.ai, Zoom, Google Suite, and SFDC)
*  Gain a strong understanding of Command of the Message and MEDDPICC. For sales roles this includes becoming adept at trap-setting questions, discovery questions, and being audible-ready. For Customer Success roles this includes becoming familiar with value drivers, asking open-ended questions, and being audible ready. 

## Graduating from Sales Onboarding
In order to officially “graduate” from Sales Onboarding at GitLab, we have identified a few components that are considered milestones of achievement (many of these must be done concurrently to ensure completion):

Sales Roles:
*  Complete GitLab General Onboarding issue
*  Complete Sales Quick Start learning path in Google Classroom prior to the SQS Workshop
*  Complete the Sales Quick Start Workshop
*  Deliver GitLab Value Pitch
*  Articulate the Command of the Message Mantra
*  Complete a series of discovery calls:
   - Submit 1 recorded mock discovery call in SQS pre-work
   - Complete 1 mock discovery call at the SQS Workshop
   - Submit 1 “Live Lead” after the SQS Workshop within 30 days
*  Review and obtain approval from your manager for a territory and account plan
*  Close first deal within the first 90 days of starting at GitLab

Customer Success Roles: 
*  Complete GitLab General Onboarding issue
*  Complete Sales Quick Start learning path in Google Classroom prior to the SQS Workshop
*  Complete the Sales Quick Start Workshop
*  Deliver the role based Capstone 
   * TAM: Mock customer kickoff
   * SA: Build and deliver mock demo
   * PSE: coming soon 
*  Articulate the Command of the Message Mantra
*  Kickoff first customer engagement within the first 90 days of starting at GitLab


## Sales Onboarding Process
1.  The GitLab Candidate Experience team initiates a general GitLab onboarding issue for every new GitLab team member
    - See the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
1.  In the "Day 1 Accounts and Paperwork" section of the general GitLab onboarding issue, sales managers are instructed to create an Access Request (sales role-based templates are available in the list of template on [this page](https://gitlab.com/gitlab-com/access-requests)
1.  In the "Sales Division" section of that issue, Field Enablement is tagged with the action to add the new sales team member to the Sales Quick Start learning path in Google Classroom according to the SQS Workshop they plan to attend. This learning path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.
1.  The new sales team member will receive an email prompting them to login to Google Classroom to begin working through the Sales Quick Start learning path  
    - When possible, Sales Enablement will also update the new team member's onboarding issue and/or add a comment to explicitly reference the link to their Sales Quick Start learning path
    - In the "New Team Member" section, there is a specific action for the new hire to "Complete your Sales Quick Start learning path"
    - See what's included in the virtual, self-paced [Sales Quick Start learning path in Google Classroom](/handbook/sales/onboarding/#sales-quick-start-learning-path)
    - Non-Sales team members can choose to add themselves to the SQS Google Classroom Master Learning Path regardless of role if interested in understanding what new Sales Team Members are expected to complete prior to attending an SQS Workshop (Sales team members will be added to their appropriate cohort’s Google Classroom Learning Path by Sales Enablement)
        -  [SQS Google Classroom Master Learning Path](https://classroom.google.com/c/NjIxMTgzNzcyMzda)
        -  Code: 4fege7p

## Targeted Sales & Customer Success Roles
*  Targeted roles for the Sales Quick Start learning path include: 
   - Enterprise and Public Sector Strategic Account Leaders (SALs)
   - Mid-Market Account Executives (AEs)
   - SMB Customer Advocates (CAs)
   - Inside Sales Reps (ISRs)
   - Solution Architects (SAs) 
   - Technical Account Managers (TAMs) 
   - Professional Services Engineers (PSEs) (PSEs do not participate in the Sales Quick Start Workshop)

Sales Development Reps (SDRs) have their own separate onboarding process (but many of the same elements are shared).

## Sales Quick Start Workshop
*  NOTE: The Sales Quick Start (SQS) Workshop is MANDATORY for all new Sales Team Members. If you must cancel for any reason, please be sure to obtain Regional Director approval in writing (Ryan O'Nell for Commercial).
*  After completing the virtual, self-paced Sales Quick Start learning path, new sales team members (along with new Solution Architects, Technical Account Managers, and Sales Development Rep) participate in an interactive in-person workshop where participants practice applying this new knowledge in fun and challenging ways including mock calls, role plays, and group activities
   - As a result, new sales team members exit Sales Quick Start more confident and capable in their ability to lead an effective discovery call with a customer or prospect
   - Participants also benefit by establishing a community of similarly-tenured peers and more experienced mentors and colleagues to support each others' growth and development at GitLab
*  See what's included in the [Sales Quick Start in-person workshop](/handbook/sales/onboarding/#sales-quick-start-workshop-agenda)
*  **Note: The Results Value is our highest priority and we strive to deliver sales training and enablement remotely as much as possible and reserve in-person formats only for circumstances when we can't obtain the desired results via remote/virtual delivery)**
   - Iteration is expected to keep moving what we can to remote models once we can achieve the same or better results that way
*  Future iteration of this process will define what sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** (by sales role, customer segment, and geo as appropriate) after they complete the above and support those learning objectives as they continue their ramp to GitLab sales performance excellence!

## Sales Quick Start Workshop Schedule

| DATES | GEO | LOCATION |
| ------ | ------ | ------ |
| April 7-9, 2020 | EMEA / APAC | Virtual Session Only |
| May 2020 (exact dates TBD) | AMERICAS | Virtual Session Only |

## Swag for New Sales Team Members

As a part of Sales Onboarding, each new Americas Sales team member is allowed to order one Swag Marketing Kit through this form. As of right now, there is no Swag Marketing Kit available for our other regions, but the Marketing team is working towards developing one.

## Sales Quick Start Pre-Work Learning Path 

* [Sales Quick Start Learning Path](https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/)

## Sales Quick Start In-Person Workshop Agenda

This [SQS 2020 Agenda sheet](https://docs.google.com/spreadsheets/d/1f1O2VC_6Fjdhrpyi9vB81kvdJ4H-66F8ghv-h_-_bGw/edit?usp=sharing) contains the most up to date agenda for our in-person Sales Quick Start Workshop. Please check the tabs at the bottom of this document to see each day's agenda. This agenda is subject to change based on the individual needs of the class and the availability of SMEs, but we will make every effort to surface those changes in this document.
