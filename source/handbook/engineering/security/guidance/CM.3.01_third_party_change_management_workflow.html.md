---
layout: handbook-page-toc
title: "CM.3.01 - Third-Party Change Management Workflow Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# CM.3.01 - Third-Party Change Management Workflow

## Control Statement

Third-party vendor and service change scope, change type, and roles and responsibilities are pre-established and documented in a change control workflow; notification and approval requirements are also pre-established based on risk associated with change scope and type.

## Context

Having a structured workflow and guidance on change management helps reduce the risk of GitLab experiencing platform or application instability by increasing the predictability and reproducibility of the change management process.

## Scope

This control applies to  third-party systems that support the business of GitLab.com.

## Ownership

* Control Owner: `Finance`
* Process owner(s):
    * Finance
    * Business Operations
    * System Owners

## Guidance

For third-party change management, there are two types of changes: automated updates from the vendor and customized changes performed by either GitLab or the vendor. Automated updates are categorized under the SaaS's patch management and we can rely on the SOC report and release notes from the vendor. This control is for customized changes.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Change Management Workflow control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/781).

Examples of evidence an auditor might request to satisfy this control:

* Copy of the GitLab third-party change management workflow
* Sample of issues or other documentation showing the third-party change management workflow is followed

### Policy Reference

## Framework Mapping

* ISO
  * A.12.1.2
  * A.12.6.2
  * A.14.2.1
  * A.14.2.2
  * A.14.2.4
* SOC2 CC
  * CC2.3
  * CC8.1
* PCI
  * 1.1.1
  * 10.4.2
  * 6.4
  * 6.4.5
  * 6.4.5.1
  * 6.4.5.2
  * 6.4.5.3
  * 6.4.5.4
  * 6.4.6
