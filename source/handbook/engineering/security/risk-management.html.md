---
layout: handbook-page-toc
title: "Risk Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Risk Management Policy

## Purpose

The purpose of the risk management policy is to provide guidance regarding the overall management of risk to support the achievement of organization wide objectives, protect GitLabbers and GitLab assests to unecessary exposure, and to ultimately ensure that security risks that may impact GitLab's ability to achieve it's customer commitments are effecitvely identified and tracked through remediation to sustain and maintain the stability of GitLab's offerings.

## Scope

The risk management policy applies to all GitLab Departments given that activities performed across the organization as a whole may contribute in increased or reduced security risks. Given this, the policy is extended to all employees and contractors contributing to GitLab.

## Risk Governance
A defined risk governance structure has been put in place to outline the overall roles and responsibilities of individuals as it relates to organizational risk management. The current governance structure is:

|**Group/Individual**|**Responsibilities**|
|-----|----------|
|VP of Security|Performs a final review and approval of annual risk assessments|
|Director of Security|Provides oversight of  the overall risk management policy, reviews annual risk assessments, and occasionally reviews and approves risk assessments performed over vendors with significant risks|
|Security Compliance Team|Continuously work to improve the risk management policy, strategy and supporting risk frameworks|
|Managers|Drive direct reports in their respective business units to comply with the risk management policy to foster a culture where risks can be appropriately identified and escalated as part of day-to-day operating procedures|
|Employees and Contractors|Comply with the risk management policies and procedures|

## Risk Management Process
Key risk factors/considerations have been identified that need to be considered when assessing the risks across the organization. These considerations are included at every step of the risk management process. At a high level, risk management includes considerations over `Information Systems` used to support GitLab and day-to-day operations, `Business Processes` executed regularly that allows GitLab to sustain the business, and the `Organization` as a whole.

### Risk Factors

As part of the risk management process, Security Compliance has includes key risk factors/considerations from both internal and external perspectives. Internal factors include considerations such as the potential for fraud, quaility of personnel being hired, and even significant changes to internal processes and personnel. External factors include considerations such as changes in legislation and regulations that force changes to internal processes, economic changes that may impact GitLab as a business, and even natural disasters that may impact GitLab's availability.

### Integration with other systems and processes

Risk management is inherently factored into business planning through various mechanisms such as conducting a [Vendor Security Review](https://about.gitlab.com/handbook/engineering/security/third-party-vendor-security-review.html) when planning to onboard a new system to the GitLab environment. Additionally, as part of business planning, considerations over GitLab's compliance obligations are included. Risk management is also integrated with our audit and assurance procedures given that [Internal Audit](https://about.gitlab.com/handbook/internal-audit/) continuously assesses the state of internal controls impacting SOX compliance and that Third Party Assurance reports are provided to GitLab by external, independent auditors assessing the state of GitLab's Security Controls. While it may not be explicit, considerations of risk are innate in the internal processes that are executed through the controls that are in place over these processes. For a detailed overview of our controls, refer to the [Security Controls](https://about.gitlab.com/handbook/engineering/security/sec-controls.html#list-of-controls-by-family) handbook page.

## Risk Management Procedures

Risk management is performed via risk assessments. These assessments help to identify and manage risks in accordance with the various [risk factors](#risk-factors) that have been identified.

### Annual Risk Assessments

Risk assessments help GitLab identify, prioritize, and manage security risks. They're important to help protect sensitive data as outlined in our [Data Classification Policy](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html) and are integral to the operating effectiveness of various GitLab [security compliance controls](/handbook/engineering/security/sec-controls.html#risk-management), which are key to helping GitLab meet its compliance standards, such as SOC2 and PCI-DSS. At GitLab, the annual risk assessment process is based on a combination of [NIST SP 800-30 Rev. 1](https://csrc.nist.gov/publications/detail/sp/800-30/rev-1/final) and the [Mozilla Rapid Risk Assessment (RRA)](https://infosec.mozilla.org/guidelines/risk/rapid_risk_assessment), but customized to be collaborative, asynchronous, and performed using GitLab itself. The annual risk assessment work is completed within the [Risk Assessment](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments) repository. The output of this risk assessment is an annual risk assessment report report.

While an organizational wide risk assessment is performed anually, there are additional risk assessments that may be performed outside of the annual organizational risk assessment process. These assessments may occur for various reasons, such as:

- GitLab procures a new system to be used in the environment and as part of this, a [Vendor Security Review](https://about.gitlab.com/handbook/engineering/security/third-party-vendor-security-review.html) is performed
- Dependencies on risk assessments from other functions, such as [Internal Audit](https://about.gitlab.com/handbook/engineering/security/third-party-vendor-security-review.html) and the [annual penetration testing timing](https://about.gitlab.com/handbook/engineering/security/penetration-testing-policy.html), does not align with the annual risk assessment process
- Discussions that occur as part of day-to-day operating procedures related to existing processes and procedures result in new risks areas being identified which lead to an [ad-hoc risk assessment](#ad-hoc-risk-assessments) being performed over these risks

### Ad-hoc Risk Assessments

Ad-hoc risk assessments occur when GitLab identifies risks through the day-to-day operating procedures executed to maintain the business. As an example, an ad-hoc risk assessment was performed to discuss moving certain GitLab production systems out of Azure and onto GCP where most of the infrastructure that supports GitLab (the product) lives. An assessment was performed to vet the overall risk of not moving these systems to GCP and resulted in the decision to work towards migrating off of Azure and onto GCP. The current process to conduct an ad-hoc risk assessment is as follows:

* Ad-hoc Risk assessments are maintained as markdown files in the [Risk Assessment](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments) repository.
* To create a new risk assessment, create an MR in the Risk Assessment repository.
    * Using the [risk assessment template](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/blob/master/templates/risk-assessment-template.md), the MR should create a new file titled `[Service Name]-risk-assessment` in the `assessments/` directory.
    * To make additions or changes, push commits like you would any other MR.
    * Anyone can contribute to risk assessment MRs.
* The risk assessment process is designed to be [iterative](/handbook/values/#iteration). The MR should be merged when you or others feel there's enough information for it to be useful and then continually iterated on after being merged. A good baseline is for it to be merged when at least one risk has been identified and scored.
* For every identified risk, follow the risk [remediation and tracking](/handbook/engineering/security/#risk-remediation-and-tracking) process.
* To iterate on a risk assessment after it's been merged, see [Contributing to a Risk Assessment](/handbook/engineering/security/#contributing-to-a-risk-assessment).

### Risk Remediation and Tracking

* Create a new risk [remediation issue](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/issues/new?issuable_template=Risk%20Remediation%20Template).
* Update the [Risk Registry](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/blob/master/Risk%20Registry.md) with a link to the remediation issue and the risk status.

### Contributing to a Risk Assessment

* Ad-hoc Risk assessments are [located](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/tree/master/assessments) in the `assessments/` directory. To contribute to these risk assessments, please [create an MR](/handbook/communication/#everything-starts-with-a-merge-request) to propose the change or start a discussion.
   * [Everything is a draft](/handbook/values/#everything-is-in-draft), so iteration to risk assessments is encouraged.
   * Anyone can contribute to a risk assessment, including team members outside of the Security department.
   * If the status of an identified risk is changed, update the [Risk Registry](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/blob/master/Risk%20Registry.md).

Annual risk assessments are different because they are documented via Google Docs. The final risk report that is outputted as part of the annual assessments will live in the risk assessment repository and has mandatory sign-off requirements from various stakeholders and are considered finalized. Additional risks identified outside this annual process can be documented using the [ad-hoc risk assessment process](#ad-hoc-risk-assessments).

### Review and Revision of Risk Assessments

Risk assessments should be continually reviewed and revised to keep the information in risk assessments current and relevant. At a minimum, every ad-hoc risk assessment will be reviewed once per year and unremediated risks that were not subject to the [risk acceptance](#risk-acceptance) process will be consolidated into the next annual risk assessment cycle for continued tracking through to remediation. If new risks are identified or the status of an identified risk is changed, the [Risk Registry](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/blob/master/Risk%20Registry.md) will be updated accordingly.

### Closing a Risk Assessment

Sometimes it may be appropriate to close a risk assessment. Some reasons a risk assessment may be closed are the vendor is no longer used or the type of data stored or processed changes. To close a risk assessment, open an MR and: 

* Add `(CLOSED)` to the end of `# GitLab Risk Assessment`, to get `# GitLab Risk Assessment (CLOSED)`
* Immediately under the `# GitLab Risk Assessment (CLOSED)` section, write that the risk assessment is being closed, why it's being closed, and provide any supporting links.
* As risk assessments are a function of DPIAs, the closure of any risk assessments should be reviewed and merged by a Security Compliance manager.

[This MR](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/merge_requests/23) can be used as an example of this process.

## Risk Acceptance

Risk management is a way to identify and document most of the risks associated with how we perform our work as GitLab team-members. Every decision we make as a company involves risk and the purpose of documenting that risk is not to influence decision making or slow down processes unnecessarily, but rather to give GitLab decision makers as much information about that decision as possible so the best possible decision can be made.

A Risk Acceptance is a way to document when the business benefits outweigh the risks associated with a particular decision. In order to gather the evidence we need to show auditors we are making these decision with security risks in mind, we are processing risk acceptances through [risk acceptance issues](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/issues/new?issuable_template=Risk%20Acceptance%20Template). 

The goal with the risk acceptance is to make a ["steel man" case](/handbook/values/#assume-positive-intent) for why a particular decision might increase the overall risk to the organization. This is not meant to deter the appropriate exception approver from accepting that risk, but rather to make that decision as informed as possible. There will always be times when a process meant to reduce risk in 80% of cases simply won't apply to a particular decision and this exception process will help guide all parties involved through that decision making process.