---
layout: handbook-page-toc
title: "Key Meetings"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

For each [executive](/company/team/structure/#executives) — and some department leaders — to discuss the key metrics of that department in order to:

1. Make it much easier to stay up-to-date for everyone.
1. Be accountable to the rest of the company.
1. Understand month-to-month variances.
1. Understand performance against the plan, forecast and operating model.
1. Ensure there is tight connection between OKRs and KPIs.

## Key Metrics

1. [KPIs](/handbook/ceo/kpis/) of that function or department.
1. [OKRs](/company/okrs/) that are assigned to that function.

## Timing

Meetings are monthly starting on the 10th day after month end.
The EBA to the CFO is the Scheduling DRI for the Monthly Key Meetings.

## Scheduling

Whereever possible, the goal is to have all Key Meetings in a large block of time.
Having back-to-back Key Meetings has the following advantage:
* EBAs can add 1 hour prep time the day before for executives to prep for all the meetings, allowing them to be more prepared than they would be with less prep time for each meeting, leading to more effective meetings
* [Some evidence](https://blog.trello.com/why-context-switching-ruins-productivity) suggests it can take upwards of 20 minutes to re-engage after context switching

This is preferred, not mandatory.
The inability to consolidate Monthly Key Meetings *should never block its scheduling* but moving towards these is the goal.


## Invitees

Required invites are the CEO, the CFO, and the function head.
Optional attendees are all other members of [the e-group](/handbook/leadership/#e-group).
The meetings are open to all team members who are interested.
The meetings are also to be livestreamed to GitLab Unfiltered.
Functions that have these meetings are:

*  Corporate Development (Eliran Mesika - function DRI) 
*  Engineering (Eric Johnson - function DRI)
*  Finance (Paul Machle - function DRI)
*  Infrastructure (Gerardo Lopez-Fernandez - function DRI)
*  Marketing (Todd Barr - function DRI)
*  People Group (Carol Teskey - function DRI)
*  Product (Scott Williamson - function DRI)
*  Product Strategy (Mark Pundsack - function DRI)
*  Sales (Michael McBride - function DRI)
*  Support (Tom Cooney - function DRI)

If you would like to be added to a function's Key Meeting post in [#key-meetings](https://gitlab.slack.com/archives/CPQT0TRFX) on Slack.

## Meeting Format

There are two meeting formats.
The preferred meeting format leverages the [KPI Slides project](https://gitlab.com/gitlab-com/kpi-slides) which uses Reveal JS to automate the slide preparation and leverages version control to provide a history of changes over time.
Other teams leverage Google Slides for their meetings.

Important notes:
1. Before every key meeting, the [OKR page](/company/okrs/) for the quarter should be updated with the current status of the KRs.
1. A document will be linked from the calendar invite for participants to log questions or comments for discussion and to any additional track decisions & action items.
1. Wherever possible, the KPI or KR being reviewed should be compared to Plan, Target, or industry benchmark.
1. There is no presentation; the meeting is purely Q&A. Of course, people can ask to talk them through a slide. If you want to present, please [post a YouTube video](/handbook/marketing/marketing-operations/youtube/) like [Todd did](https://www.youtube.com/watch?v=hpyR39y_1d0) and link that from the slide deck, agenda, and/or Slack.
1. The functional owner is responsible for preparing the document 24 hours in advance of the meeting. The owner should update the meeting invite, send it to all guests so they know the materials are ready for review and, if possible, post the document in the #key-meetings channel.
1. For Key Meetings that are not livestreamed it is the DRI's or their EBA's responsibility to upload a private video to GitLab Unfiltered. 

Also see [First Post is a badge of
honor](/handbook/communication/#first-post-is-a-badge-of-honor).

### Automated KPI Slides

The [original demo and proposal of using the KPI Slide project](https://youtu.be/5BlFNhSfS8A) is on GitLab Unfiltered (internal).

The [KPI Slides project](https://gitlab.com/gitlab-com/kpi-slides) is a GitLab-internal project since it includes KPIs that are [not public](/handbook/communication/#not-public).
Those slides are deployed to GitLab pages (also internal, you must have access to the project to see the website).
Each group that presents has one markdown file with their KPIs.
Every month, groups create an MR to update that markdown file.
The following slides need to be updated with an MR:
* Month on the first slide
* Key Business Takeaways- should especially highlight any KPIs that need attention
* OKR statuses

The following Key Meetings are automated: (all links are internal)
* [CEO](https://gitlab-com.gitlab.io/kpi-slides/slides-html/ceo/#/)
* [Finance](https://gitlab-com.gitlab.io/kpi-slides/slides-html/finance/#/)
* [Infrastructure](https://gitlab-com.gitlab.io/kpi-slides/slides-html/infrastructure/#/)
* [Marketing](https://gitlab-com.gitlab.io/kpi-slides/slides-html/marketing/#/)
* [People](https://gitlab-com.gitlab.io/kpi-slides/slides-html/people/#/)
* [Product](https://gitlab-com.gitlab.io/kpi-slides/slides-html/product/#/)


### Google Slides

1. The functional owner will prepare a google slide presentation with the content to be reviewed.
1. The finance business partner assigned to the functional area will meet with the owner at least one week in advance and ensure that follow-ups from last meeting have been completed and that data to be presented has proper definitions and is derived from a Single Source of Truth.
1. The title of every slide should be the key takeaway
1. A label on the slide should convey whether the metric result is "on-track" (green), "needs improvement" (yellow), or is an "urgent concern" (red).
1. A [blank template](https://docs.google.com/presentation/d/1lfQMEdSDc_jhZOdQ-TyoL6YQNg5Wo7s3F3m8Zi9NczI/edit) still needs labels.
