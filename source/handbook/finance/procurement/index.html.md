---
layout: handbook-page-toc
title: The Procurement Team
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Requesting Procurement Services

Prior to engaging Procurement, please review the below guidelines:
1. Review the market capabilities defined by your overall spend *before* selecting your vendor.
1. Before sharing details and/or confidential information regarding our business needs, please obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing) from the potential vendor(s).
1. All vendors must adhere to the [GitLab Partner Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics).
1. Inform your vendor it is mandatory they contractually adhere to this if they would like to do business with us.
(Note these are typically not required in marketing events agreements unless the vendor is providing services).
1. Identify your bid requirements based on your estimated spend:
     >$0-$100K: No bid

     >$101K - $250: 2-3 Bids

     >Greater than $250K: RFP
1. Start working with the Procurement team by opening an issue based on the choices below.
If you have a question please ask via the #procurement slack channel.

### 1: Software or vendor that will process data on GitLab's behalf?

[Open an issue with this template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts) to begin the process.

### 2: Not processing data? 
> Examples: marketing programs, sponsorships, hotels, and professional services

[Open an issue with this template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts_marketing_events.md#)

### 3: Existing vendor negotiation?
> for renewals and true-ups

* Work with procurement BEFORE agreeing to business terms by opening an issue with [this template](https://gitlab.com/gitlab-com/finance/issues/new?issuable_template=vendor_contracts) 90-60 days before the existing contract expires.

Procurement will engage our software buying partner Vendr to ensure that the quoted pricing is competitive.
Vendr can help negotiate directly with vendors on both new subscriptions and renewals.
It is preferred that we negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term relationships with vendors.

## Deep Dive on the Procurement Process

### Legal Engagement for Vendor Contracts

Legal is responsible for reviewing vendor contracts and will adhere to legal playbook.

A contract cannot be signed until it has been approved by the legal team. Once the legal team approves the contract, legal will upload the contract with the approval stamp. Contracts will not be signed unless the legal approval stamp is included.

### Capacity & Back Log

In the event the procurement team is out of office (as highlighted in PTO calendar or Slack), and the matter is time sensitive, requestor should
contact #legal channel in Slack and provide:

1.  Link to Vendor Contract Approval Issue; and
2.  Reason for escalation, with timeline for requirement(s)

Legal will assign a team member to approve the procurement portion of the issue.

## Vendors & GitLab

### Vendor Onboarding

Vendors will be required to create an account within Tipalti in order to receive payment.

### Vendor Performance Issues

If there are performance and/or quality issues from an existing third-party vendor, procurement can support the resolution and/or re-evaluation of a replacement vendor.
Please log an issue and assign `a.hansen` for next steps.

## Related Docs and Templates

#### Documentation

* [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
* [Compliance](/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [General Guidelines](/handbook/general-guidelines/) - general company guidelines
* [Terms](/terms/) - legal terms governing the use of GitLab's website, products, and services
* [Privacy Policy](/privacy/) - GitLab's policy for how it handles personal information
* [Trademark](/handbook/marketing/corporate-marketing/#gitlab-trademark--logo-guidelines) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents

##### Contract Templates

- [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)
- [Data Processing Addendum](https://drive.google.com/file/d/1Of9hxDYpKDW4t9VF2Zay-lByZrxGx9DY/view?ts=5cdda534)
- [SaaS Addendum](https://drive.google.com/file/d/1NwaYid6qIJk9YscaRoY-uY5bEGsN1uu2/view?usp=sharing)

## Procurement KPI

Deliver quantified savings of > $3,000,000 over a rolling 12 month period.

Cost savings are achieved through the procure to pay process.
Savings are calculated as the savings achieved by comparing the initial vendor proposal price to the final purchase price.
In the event of a contract renewal, in addition to the above, savings can also be calculated as the savings achieved by comparing the previous cost per unit (eg. user, business metric, etc.) to the final cost per unit.
Savings negotiated at the cost per unit level are also cost savings to be calculated as savings.
Note these savings are not directly tied to budget.

Aligns with the following core business objectives:

* Control spend and build a culture of long-term savings on procurement costs.
* Streamline the purchasing process.
* Minimize financial risk.

## Procurement Main Objectives

Procurement operates with three main goals
1.  Cost Savings
2.  Centralize Spending
3.  Compliance
