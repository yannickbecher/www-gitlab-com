---
layout: markdown_page
title: "Executive Demo Instructions"
---

## Summary

This page focuses on how to create executive click-through demos.


## What is an executive demo

There are certain times when slides, video or other such media just will not do. When you are with a customer or on a trade show stage, a live product demonstration is far superior to slides or videos. Live demos, however, can be fraught with challenges, from technical issues to poor content or delivery. Click-through demos ensure every demo presentation, with groups small or large, is done perfectly every time, eliminating technical and content challenges. Click-through demos are reliable, easy to present and give the audience an absolutely life-like demonstration EVERY TIME.

## Who can create executive demos

Everyone can download [DemoEasel](https://www.demoeasel.com/download) and record and edit demos, you don't need license to create and edit demos, however, in order to generate demo packages, license is required. The Technical marketing team purchased licenses, the team will be happy to generate packages for you. The priority will be for customer facing demos, which can be reused by other team members.


## Executive Demo Instructions


### How Can I use this demo?

* Download the demo locally 
* Extract it
* Open ‘GitLab and AZDO v3’ - double click this file 
* If you have macOS Catalina - follow the steps in the next slide
* Click Present Demo
* Contact TMM team if you need help by doing the following:  Open an Issue in [product-marketing](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=), select **TMM_Support_request** template. Add the following title: "Click-through demo support". In the description, describe the customer challenge and the solution you demonstrate in the demo. Include the path of your demo files you uploaded.


### macOS Catalina

| In MacOs Catalina you will get an error when you open the demo file.  | ![GitLab Project](/images/tech-pmm/errorcatalina.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 .image-width80pct } |
|  To open it,  right click on the demo file -> Open  | ![GitLab Repository](/images/tech-pmm/demofilecatalina.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 .image-width80pct } |
|  A new dialog will appear,  click Open.   | ![GitLab Repository](/images/tech-pmm/dialogopencatalina.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 .image-width80pct } |
