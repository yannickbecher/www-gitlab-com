---
layout: handbook-page-toc
title: "Marketing Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Marketing Operations

Marketing Operations (MktgOps) supports the entire Marketing team to streamline processes and manage related tools. Due to those tools, we often support other teams at GitLab as well. MktgOps works closely with Sales Operations (SalesOps) to ensure information between systems is seamless, data is as accurate as possible and terminology is consistent in respective systems. Not only are we assisting with marketing operations but we are also involved in the operations of marketing, such as the budget and strategies.

## Important Resources
- [Marketing Metrics](/handbook/marketing/marketing-operations/marketing-metrics)
- [Marketing Owned Provisioning Instructions](/handbook/marketing/marketing-operations/marketing-owned-provisioning)
- [List Imports](/handbook/marketing/marketing-operations/list-import)

## Tech Stack  

For information regarding the tech stack at GitLab, please visit the [Tech Stack Applications page](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) of the Business Operations handbook where we maintain a comprehensive table of the tools used across the company.   

The main tools used by Marketing and integrated with Salesforce are:
- [Marketo](/handbook/marketing/marketing-operations/marketo)
- [Outreach.io](/handbook/marketing/marketing-operations/outreach)
- [LeanData](/handbook/marketing/marketing-operations/leandata)
- [PathFactory](/handbook/marketing/marketing-operations/pathfactory)
- Demandbase
- DiscoverOrg
- LinkedIn Sales Navigator
- [Drift](/handbook/marketing/marketing-operations/drift)
- Bizible

Other tools directly used by Marketing and maintained by Marketing Operations:  
- [Bizzabo](/handbook/marketing/marketing-operations/bizzabo)
- Cookiebot
- Disqus
- Eventbrite
- Funnelcake
- Google Adwords
- Google Analytics
- Google Search Console
- Google Tag Manager
- MailChimp
- MozPro
- Sprout Social
- Survey Monkey
- Tweetdeck
- WebEx
- [YouTube](/handbook/marketing/marketing-operations/youtube/)

### Requesting a new tool

If you are interested in or would like to request a new tool be added to the tech stack, [please submit an issue using the tools eval issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/new?issuable_template=tools_eval) in the Marketing Operations repository. Marketing Operations should be included in new tool evaluations to account for system integrations, budget, etc.

## Working with Marketing Operations

### MktgOps Motto: If it isn't an Issue, it isn't OUR issue.   

The MktgOps team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~MktgOps label anywhere within the GitLab repo. 

MktgOps uses a [global issue board](https://gitlab.com/groups/gitlab-com/-/boards/825719) and will capture any issue in any group/sub-group in the repo. There is also a [`Marketing Operations` project](https://gitlab.com/gitlab-com/marketing/marketing-operations) within the [`Marketing` project](https://gitlab.com/gitlab-com/marketing).     

Labels to use:  
- `MktgOps`: Issue initially created, used in templates, the starting point for any label that involved MktgOps
- `MktgOps - FYI`: Issue is not directly related to operations, no action items for MktgOps but need to be aware of the campaign/email/event
- `MktgOps - Reporting`: Any reporting request for MktgOps
- `MktgOps - List Import`: Used for list imports of any kind - event or general/ad hoc
- `MktgOps-Priority::1 - Top Priority`: Issue that is related to a breaking change, OKR focus, any other prioritized project by MktgOps leadership. This category will be limited because not everything can be a priority.
- `MktgOps-Priority::2 - Action Needed`: Issue has a specific action item for MktgOps to be completed with delivery date 90 days or less from issue creation date. This tag is to be used on projects/issues not owned by MktgOps (example: list upload).
- `MktgOps-Priority::3 - Future Action Needed`: Issue has a specific action item for MktgOps, the project/issue is not owned by MktgOps and delivery or event date is 90 days or more from issue creation.
- `MktgOps::1 - Planning`: Issues that are currently being scoped/considered but are not being actively worked on.
- `MktgOps::2 - On Deck`: Issues that have been scoped/considered and will be added to the In Process queue next. 
- `MktgOps::3 - In Process`: Issues that are actively being worked on in the current week/sprint (week? Month? Two-week period?)
- `MktgOps::4 - UAT`: Issues that MktgOps has completed its required tasks for and is ready for review and approval by the Requester/Approver.
- `MktgOps::5 - On Hold`: Issue that is not within existing scope of Mktg OPS current targets, blocked by MktgOps-related task/issue, or external (non-GitLab) blocker.
- `MktgOps::6 - Blocked`: Issue that is currently being worked on by Mktg Ops and at least one other team wherein MktgOps is waiting for someone else/another team to complete an action item before being able to proceed.
- `MktgOps::7 - Completed`: MktgOps has completed their task on this issue although the issue may not be closed. 


## Operations Work Cadence   

The MktgOps team works in two week sprints which are tracked as **Milestones** at the `GitLab.com` level. Each Ops individual contributor (IC) is responsible for adding issues to the milestone that will be completed in the two week time frame. If needed, the IC will separate the main issue into smaller pieces that are *workable* segments of the larger request.   

The MktgOps team will only create a milestone one beyond the current iteration, so at any given time there will be the **current** milestone and **upcoming** milestone, any other issue that is not included will be added into future milestones &/or added as work is completed in the current milestone.   

A milestone cannot be closed nor marked complete until all associated handbook updates have been completed. Within every milestone there is an issue to track all changes and keep a running record of the handbook updates made. The handbook change issue will be created using the `milestone_handbook_update` issue template and automatically added to the [Handbook Change Epic](https://gitlab.com/groups/gitlab-com/-/epics/140).  

### Operational Timeline of Changes  

Periodically Marketing Operations makes significant changes to our system and processes that affect overall tools, data and reporting or uncovers significant changes that affected reporting. As such we have an [Operational timeline of events](https://drive.google.com/open?id=1vhGvEszndMJ4B9EshGFSdTTABwUzBzDObz93vkMSFGA). The MktgOps team updates this document as needed as changes are made. 


## How-tos & FAQs

### Webcast Setup   
When creating a live or ondemand webcast it is important to integrate the event program across the platforms/programs used - GitLab Marketing site (`about.gitlab.com`), Marketo (Marketing Automation), Zoom (Webcast video software) and Salesforce (CRM). This provides transparency about the webcast to anyone with access to Salesforce, namely the Sales and Marketing teams.  

For a comprehensive overview on how to set up a webcast, please visit the [Business Operations section](/handbook/business-ops/resources/#Webinars) of the handbook.  

### Requesting an Email

o request an email, create an issue in [Digital Marketing Programs project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=Email-Request-mpm), using the `Email-Request-mpm.md` issue template. Be as complete as possible providing all requested detail related to the email, goal, target audience and any other pertinent details. Please review the `Email Review Protocol` section below for more detail. 

Email requests should be submitted **no less than** 72 hours* before intended send date so the new request can be added into the responsible Marketing Program Manager's (MPM) and for select cases (see email review protocol below) into Content team's workflow. The responsible MPM will be determined by type of email requested, [see division of duties](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/#responsibilities).

All links in email sends, going to about.gitlab.com will need to be appended with utm parameters, following the nomenclature outlined in this [document](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0). This is the way we track and give attribution to emails.

Below are the information from the issue template that will need to be filled out before the MPM will create the email in the appropriate system:

- **Sender Name**: Typically we use GitLab Team for most outgoing communications; for Security Alerts we use GitLab Security. Choosing a name that is consistent with the type and/or content of email being sent is important, if unsure make a note and we will make recommendation.
- **Sender Email Address**: What email address should be used?
- **Approvers**: All approvers must be listed on the email request. At least one individual who will receive the replies to the email must be listed an as approver. For example, if the email is coming from security@, someone who will receive replies to the email should be listed as one of the approvers. See approval table below.
- **Subject Line**: 50 character max is preferred (30-40 characters for mobile devices)
- **Email Body Copy**: Can be a text snippet within issue, clearly identified comment on issue or attach a Google Doc with copy. The copy must be approved before requesting the email.
- **Target Date to Send Email**: at a minimum a few days notice is preferred because we need to balancing the number of emails being sent to our database so they are not perceived (or marked) as spam; however, a simple email can be turned in a few hours if absolutely necessary
- **Recipient List**: Emails can be sent to one of the [existing segments](/handbook/marketing/marketing-operations/marketo#geographic-dma-list ) or a recipient list can be provided as a .csv file
    -  Audience should be appropriately segmented and tokens selected for personalization (if applicable) 
    -  All subscribers are selected list are opted-in to receive your message
    -  If supplying a .csv file, the file must include the following fields:  Email address, First Name (or Full Name)
    -  If personalizing the email to reference a specific project or page, that field must be included in the .csv file and clearly marked using the same terminology used in the email copy. The email copy must clearly identify {{Project}} or {{Page}} where the applicable personalization should be inserted.

**Urgent security emails are exempt from this SLA.*

#### Types of email requests

- **Marketing Emails**: Marketing emails are designed to generate leads. The request process outlined is used for ad-hoc marketing emails (not events, webcasts, integrated campaigns, etc as these all have a separate established process). These emails are sent through Marketo using the marketing database or [existing segments](/handbook/marketing/marketing-operations/marketo/#geographic-dma-list).
- **Terms of Service or Privacy Policy Updates**: Terms of Service or Privacy Policy emails are sent to the user base and are not marketing-related. These emails are sent through MailChimp and may require additional approvals, based on the content or number of recipients.
- **Support emails**: Support emails are typically sent to a subset of impacted users and are not marketing-related. These emails are sent through MailChimp.
- **Security emails**: Security emails are sent either to the entire user base or a subset of users and are not marketing-related. They are often urgent, but in the case of the monthly security release, they are scheduled. The monthly security release email is sent through Marketo. Urgent notifications are typically sent through MailChimp.
 
#### Approvals and notifications for email requests

Marketing related ad-hoc emails are sent at the discretion of the MPM team.

Terms of Service or Privacy Policy updates that impact all users must be announced on the company meeting, in the `#company-announcements` and `#community-advocates` Slack channels, and approved according to the table below prior to submitting the Email Request.

Support and Security emails sent to a small subset of users should be announced in `#community-advocates` and `#support-managers` Slack channels, and mentioned in `#company-announcements` if relevant.

The approval table below applies to non-Marketing emails.

|  **Users to be contacted** | **Approval by** |
| --- | --- |
|  <1,000 | reply-to owner |
|  1,001-4,999 | PR, reply-to owner, community advocate |
|  5,000-499,999 | PR, reply-to owner, community advocate, director+ in originating department |
|  500,000+ | PR, reply-to owner, community advocate, director+ in originating department, e-group member |

#### Email review protocol

All MPMs and reviewers should adhere to the following protocol for every marketing email that is sent to ensure brand consistency and quality in our email program. 

##### Goals & Audience

*  Goal of the campaign is clearly defined with 1 main CTA
    *  If part of a nurture series, each email has main CTA with a main CTA for the entire series
*  Success of the campaign is defined with measurable KPIs
*  Personalization added (if applicable)
*  All suppressions have been applied
*  Subscribers are opted-in
*  All SPAM and privacy laws are being recognized (GDPR, CAN-SPAM, CASL, etc.)
*  We will send event and webcast invite emails to those who are in our database. Accounts with open opportunities in **late stages 4-6** will not be included in the email send. We do this because we know it takes many marketing touches to help bring an opportunity to closure.

*Note: If you would like additional people to receive a test email prior to sending it out, please specify that in the request template and refer them to this protocol for reviewing the email.*

##### Envelope

*  From address and name are recognizable as the business
*  Subject line
    *  Descriptive, clear, and concise
    *  Length: Less than 50 characters (aim for 30-40 for mobile devices)
    *  No obvious spammy words (if in doubt, use Litmus to test)
    *  Personalization added where applicable
*  Preview/preheader text
    *  Length: depends on the email client but anywhere from 40-90 characters
    *  Descriptive, clear, and concise

##### Body

*  Any content pulled from a template is replaced with main message
*  Only relevant content is included. Follow our brand guidelines for [tone of voice](/handbook/marketing/corporate-marketing/#tone-of-voice) when evaluating copy. Put yourself in the subscriber’s shoes. Ask the question, “What’s in it for me?”
*  The main call-to-action is descriptive, clear, and ties directly back to the campaign goal(s)
*  Company contact information with physical address is included
*  No grammar or spelling issues
*  Unsubscribe link is visible and works

##### Design

*  Message renders properly across desktop, mobile, and tablet views
*  Message renders properly across main email clients (use Litmus to determine top email clients used by subscribers)
*  Calls-to-action are easily identified and interact with
*  All links work and are being tracked appropriately (UTMs applied where needed)
*  Any dynamic content or personalization included merges properly
*  Images work and have descriptive ALT text
*  Tracking and analytics are in place and work
*  Font and colors are matched or closely matched to our [brand guidelines](/handbook/marketing/corporate-marketing/#brand-guidelines)

#### Internal Email List

There is an internal email list, called `INTERNAL - Email Send List` in Marketo that will be included on every outbound email that is sent. If you are a team member and wish to be included on every email that is sent, please post a request to the `#marketing-programs` slack channel and the MPMs will review. To avoid skewing email results (particularly on smaller sends), this list will be kept to a minimum.

If you are an internal team member and wish to subscribe to a segment or segments please subscribe through the [preference center](/company/preference-center/) and you will only be included in those dedicated email sends.


## Marketing Expense Tracking

| GL Code | Account Name | Purpose |
| :--- | :--- | :--- |
| 6060 | Software Subscriptions |All software subscriptions |
| 6100 | Marketing|Reserved for Marketing GL accounts|
| 6110 | Marketing Site|Not used - All agency fees and contract work intended to improve the marketing site |
| 6120 | Advertising|All media buying costs as well as agency fees and software subscriptions related to media buying |
| 6130 | Events|All event sponsorships, booth shipping, event travel, booth design, event production as well as agency fees and software costs related to events |
| 6140 | Email|All 3rd party email sponsorships as well as agency fees and software costs related to mass email communications and marketing automation |
| 6150 | Brand|All PR, AR, content, swag and branding costs |
| 6160 | Prospecting|Not used - All costs related to prospecting efforts |

### Invoice Approval

Marketing Operations approves any invoices that have not already been coded and approved through a Finance issue or that exceed the original cost estimate. We make use of Tipalti for this process. Team leads will confirm that services were performed or products were received also through Tipalti. Campaign tags are used to track costs related to events and campaigns.

## Lead Scoring, Lead Lifecycle, and MQL Criteria
A Marketo Qualified Lead is a lead that has reached a certain threshold, we have determined to be 90 points accumulated, based on demographic, firmographic and/or behavioral information. The "MQL score" defined below is comprised of various actions and/or profile data that are weighted with positive or negative point values.
Any record with a `Person Score` greater than or equal to 50 points will be inserted into the routing flow. Using LeanData every time a `Person Score` is updated, LeanData will run a check to see if the record needs to be processed through the flow.

### MQL Scoring Model
The overall model is based on a 100 point system. Positive and negative points are assigned to a record based on their demographic and firmographic information, and their behavior and/or engagement with GitLab marketing.

The MQL scoring model below is correct as of 30 September 2018. Additional changes are being made and the following will be updated over time.

#### MQL = 90 pts
{:.no_toc}

##### Positive Point Values
{:.no_toc}
  * 100pts
     * Completes self-hosted or SaaS trial request form^
     * Completes a Contact form
     * Completes a Professional Services Request form
     * Completes a Public Sector Request form
  * 90 pts
     * Fill out [high intent](#glossary) content form related to Force Management 
     * Attends [high intent](#glossary) live webcast, livestream or demo
  * 50 pts
     * Title contains “VP, Manager, Director, Senior, or Head”
     * Visits Enterprise trial page but doesn’t complete form
  * 45 pts
     * Watches high intent on-demand webcast, livestream or demo
  * 40 pts
     * `Follow Up Requested` in Conference/Field Event/Speaking Session campaign - manual process when lists are uploaded
     * Attends Live in-person/Owned Event - manual process when lists are uploaded
  * 30 pts
     * `Visited Booth` in Conference/Field Event campaign - manual process when lists are uploaded
     * `Attended` in Speaking Session Event campaign - manual process when lists are uploaded
     * Downloads any gated asset
  * 15 pts
     * Registers for any livestream, webcast or demo
     * Attends low intent live webcast
     * Watches low intent on-demand webcast
  * 10 pts
     * Opts into Newsletter
     * Opts into Security Newsletter
     * Opts into webcast emails
     * Opts into live event/conference emails
     * Visits about.gitlab.com/pricing page
     * Visits about.gitlab.com/features page
  * 5 pts
     * Valid company email
  * 2 pts
     * Visits about.gitlab.com/installation page

##### Negative Point Values
{:.no_toc}
  * -5 pts
     * Email is @ “gmail, hotmail, yahoo, aol” (or other free-domained email)
  * -10 pts
     * Title is “blank, numerical, developer, engineer”
  * -25 pts
     * Email unsubscribe
     * Multiple career page visits in 7 days

^In-product trial requests for SaaS and self-hosted are applied using batch method that runs daily at 6a Pacific time.

## Campaign Cost Tracking
Marketing Program Managers track costs associated with campaigns - such as events, content, webcasts, etc. Campaign tags can be applied to Expensify reports, corporate credit card charges, and vendor bills processed by Accounts Payable. Campaign expenses that are incurred by independent contractors should be clearly noted with the appropriate tag and included in their invoices to the company. We then use these accumulated campaign tag costs for budget to actual analysis as well as to update the Salesforce campaigns with actual costs.

**The following steps are used to create and manage campaign tags:**

1. Event Owners create the campaign tag in the budget document as well as add a link to a Finance issue if it exists.
1. Finance is notified by the budget document to open the tag in NetSuite, which then updates Expensify nightly.
1. Event Owners create the Finance issue for approvals and the MPM issue for tracking and add the same tag to both.
1. MPM receives confirmation from responsible team (i.e. Field Marketing, Content, etc.) that the budget for the campaign has been approved and uses this as the exact same name to set up the Salesforce campaign.

**Things to Note:**

* All costs, including travel expenses for those working the event, must be tagged in order to capture the true cost of campaigns. Although travel expenses related to putting on the event hit a different GL code, they should be budgeted within the event line.
* Tagging expenses that are processed by Accounts Payable require Marketing to provide explicit instruction on when to apply tags, which should happen during the normal course of reviewing and approving vendor invoices.
* For event or campaign expenses that do not have a tag, include a note to Accounts Payable clearly stating that campaign tags are not applicable to the expense. In some cases, a general tag like Swag_Corporate may be more a more appropriate tag to track against budget.

## Marketing Gearing Ratios
Gearing ratios are used as business drivers to forecast long term financial goals by function. Refer to the [FP&A handbook](/handbook/finance/financial-planning-and-analysis/#business-drivers) for further details on how gearing ratios enable planning and forecasting. 

The gearing ratios for marketing are as follows:

- **Inquiries per MQL**: at the top of the marketing funnel, this is the conversion rate at which [Inquiries](/handbook/business-ops/resources/#glossary) become Marketo Qualified Leads ([MQL's](/handbook/business-ops/resources/#mql-definition)).

- **MQL to SAO**: this is the mid-funnel conversion rate at which MQL's become Sales Accepted Opportunities ([SAO's](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)).

- **SAO to Closed-Won**: at the bottom of the marketing funnel, this is the rate at which SAO's move through the full pipeline to [closed won](/handbook/sales/#forecast-categories-definitions), resulting in a sale. 

- **Pipe-to-Spend**: is the ratio of pipeline created that is attributed to demand generation program spend. The target ratio is 5:1 for pipe-to-spend.

- **XDR MQL Disposition/Month**: is the capacity of an ([XDR](/handbook/marketing/revenue-marketing/xdr/)) to convert MQL's into SAO's. Total MQL's converted to SAO's / Qty of XDR's

- **New IACV Average Deal Size**: is the average deal size of new customer transactions by segment and geography

- **Marketing E/R and long-term profitability target**: is marketing operating expense divided by revenue (ratable) as shown on the income statement. The long term target profitability target for Marketing E/R is 13%. This target includes the operating expense related to free usage of gitlab.com. See gitlab financial model for yearly targets of this gearing ratio.

## Email Management

Email database management is a core responsibility for MktgOps. Ensuring GitLab is following email best practices, in compliance with Global spam laws and overall health of active database are all priorities.   

Email creation, campaigns, follow up reporting and sending is the responsibility of the Marketing Program Managers. To request an email of any kind, please see the [instructions](/handbook/business-ops/resources/#requesting-an-email) in the Business Ops section of the handbook.


### Email Communication Policy  

At GitLab, we strive to communicate with people in a way that is beneficial to them. Most of our email marketing communications follow an explicit opt-in policy, although at times, we will communicate via email to people who have not explicitly opted-in. We do this to offer something of value (ex. an invite to a workshop, dinner, the opportunity to meet an industry leader, etc. not an email inviting to read a blog post) to the person. We always include the unsubscribe link in our communications, and we respect the unsubscribe list. In addition to the unsubscribe button at the bottom of all of our emails, we have available our [Email Subscription Center](/company/preference-center/), where people can control their email communication preferences. There are currently four [email segments](/handbook/marketing/marketing-sales-development/marketing-operations//#email-segments).

### Email Segments

Database segments and how someone subscribes to specific segment:  

- **Newsletter**: Users can [subscribe to the newsletter](/company/contact/) through the blog, Contact us page, and CE download page.
- **Security Alerts**: [Subscribe to security notices](/company/contact/#security-notices) on the GitLab Contact us page.
- **Webcasts**: When someone registers to a live or on-demand webcast
- **Live Events**: When someone registers to attend a live event, meet up or in-person training. Use of this segment is narrowed down by geolocation so notification and invitation emails are specific to related area.  

### Types of Email

**Breaking Change Emails**  
These are transactional emails, almost always to our user base, that provide very selective needed information. This is an operational-type email that overrides the unsubscribe and would not need to comply with marketing email opt-out. Usage example: GitLab Hosted billing change, Release update 9.0.0 changes, GitLab Page change and Old CI Runner clients.
It is very important to have Engineering and/or Product team (whoever is requesting this type of email) help us narrow these announcements to the people that actually should be warned, so we are communicating to a very specific targeted list.

**Newsletter**  
Sent bi-monthly (every 2 weeks). Content Team is responsible for creating the content for each Newsletter.  

**Security Alerts**  
Sent on an as needed basis containing important information about any security patches, identified vulnerabilities, etc. related to the GitLab platform. These emails are purely text based and again are transactional in nature.

**Webcasts**   
Invitation and/or notification emails sent about future webcasts.   

**Live Events**   
Invitation emails to attend a live event (VIP or Executive Lunch), meet-up, or in-person training. These emails are sent to a geo-locational subset of the overall segment. This type of email is also used when we are attending a conference and want to make people aware of any booth or event we may be holding and/or sponsoring.


## Website Form Management

The forms on about.gitlab are embedded Marketo forms. Any changes to the fields, layout, labels and CSS occur within Marketo and can be pushed live without having to make any changes to the source file on GitLab. When needing to change or embed a whole new form, ping MktgOps on the related issue so appropriate form and subsequent workflows can be created.

Each Marketo form should push an event after successful submission to trigger events in Google Analytics. We use the following event labels to specify which events to fire.

1. `demo` for static demos on `/demo/` and `/demo-leader/`
1. `webcasts` for forms on any page in `/webcast/`
1. `trial` for the form on `/free-trial/`
1. `resources` for forms on any page in `/resources/`
1. `events` for forms on any page in `/events/`
1. `mktoLead` legacy custom event label used on `/public-sector/` and Newsletter subscription form submission events.
1. `mtkoformSubmit` legacy custom event label used on `/services/` and `/sales/` contact forms.

We add the following line above `return false` in the form embed code. Please update the event label from `demo` to reflect the appropriate form completion.

```
dataLayer.push({event: 'demo', mktoFormId: form.getId()});
```

In the event Marketo has an outage and/or the forms go offline, the forms with highest usage/visibility (Free Trial and Contact Us) have been recreated as Google forms that can be embedded on the related pages as a temporary measure to minimize any effect till the outage is past.

## Initial Source
`Initial Source` is first "known" touch attribution or when a website visitor becomes a known name in our database, once set it should never be changed or overwritten. For this reason Salesforce is set up so that you are unable to update both the `Initial Source` and `Lead Source` fields. If merging records, keep the `Initial Source` that is oldest (or set first). When creating Lead/Contact records and you are unsure what `Initial Source` should be used, ask in the `#lead-questions` Slack channel.

The values listed below are the only values currently supported. If you attempt to upload or import leads or contacts into Salesforce without one of these initial sources you will encounter a validation rule error. If you think that there needs to be a new Initial Source added to this list and into Salesforce please slack the appropriate team member(s) listed in the [Tech Stack](/handbook/business-ops/tech-stack-applications/#tech-stack-applications).

The `Initial Source` table below is current as of 9 October 2019.

Status in the table below means:
- Active = can be selected from picklist
- Inactive = cannot be selected from picklist, but a record may exist with this source

| Source                          | Definition and/or transition plan                                                                            | Status*  |
|:--------------------------------|:-------------------------------------------------------------------------------------------------------------|:---------|
| Advertisement                   | to be evaluated                                                                                              | Active   |
| AE Generated                    | Sourced by an Account Executive through networking or professional groups                                    | Active   |
| CE Download                     | Downloaded CE version of GitLab                                                                              | Active   |
| CE Usage Ping                   | Created from CE Usage Ping data                                                                              | Active   |
| CE Version Check                | to be evaluated                                                                                              | Inactive |
| Clearbit                        | transition to `Prospecting` -> sub field `Clearbit`                                                          | Active   |
| Conference                      | Stopped by our booth or received through event sponsorship                                                   | Active   |
| CORE Check-Up                   | will be activated for new records created by the Instance Review in-product                                  | Inactive |
| Datanyze                        | transition to `Prospecting` -> sub field `Datanyze`                                                          | Active   |
| Demo                            | Filled out form to watch demo of GitLab                                                                      | Active   |
| DiscoverOrg                     | transition to `Prospecting` -> sub field `DiscoverOrg`                                                       | Active   |
| Education                       | Filled out form applying to the Educational license program                                                  | Active   |
| EE Version Check                | to be evaluated                                                                                              | Inactive |
| Email Request                   | Used when an email was received through an alias (*will be deprecated*)                                      | Active   |
| Email Subscription              | Subscribed to our opt-in list either in preference center or various email capture field on GitLab website   | Active   |
| Employee Referral               | to be evaluated                                                                                              | Active   |
| Event partner                   | to be evaluated                                                                                              | Inactive |
| Field Event                     | Paid events we do not own but are active participant (Meetups, Breakfasts, Roadshows)                        | Active   |
| Gated Content - General         | Download an asset that does not fit into the other Gated Content categories                                  | Active   |
| Gated Content - eBook         | Download a digital asset categorized as an eBook                                 | Active   |
| Gated Content - Report          | Download a gated report                                                                                      | Active   |
| Gated Content - Video           | Watch a gated video asset                                                                                    | Active   |
| Gated Content - Whitepaper     | Download a white paper                                                                                       | Active   |
| Gemnasium                       | Previous a Gemnasium customer/prospect merged into our database when acquired                                | Active   |
| GitLab Hosted                   | GitLab Hosted customer/user                                                                                  | Active   |
| GitLab Subscription Portal      | Account created through the Subscription app (check for duplicates & merge record if found)                  | Inactive |
| GitLab.com                      | Registered for GitLab.com account                                                                            | Active   |
| Gitorious                       | Previous a Gitorios customer/prospect merged into our database                                               | Active   |
| gmail                           | unknown, to be deprecated                                                                                    | Inactive |
| InsideView                      | transition to `Prospecting` -> sub field `InsideView`                                                        | Inactive |
| Leadware                        | transition to `Prospecting` -> sub field `Leadware`                                                          | Active   |
| Legacy                          | to be evaluated                                                                                              | Active   |
| LinkedIn                        | transition to `Prospecting` -> sub field `LinkedIn`                                                          | Active   |
| Live Event                      | transition to correct category based on first event attended -> `Owned Event`; `Field Event` or `Conference` | Active   |
| MovingtoGitLab                  | to be evaluated                                                                                              | Inactive |
| Newsletter                      | to be evaluated                                                                                              | Active   |
| OnlineAd                        | to be evaluated                                                                                              | Inactive |
| OSS                             | Open Source Project records related to the OSS offer for free licensing                                      | Active   |
| Other                           | Should never be used but is a legacy source that will be deprecated                                          | Active   |
| Owned Event                     | Events that are created, owned, run by GitLab                                                                | Active   |
| Partner                         | GitLab partner sourced name either through their own prospecting and/or events                               | Active   |
| Promotion                       | to be evaluated                                                                                              | Active   |
| Prospecting                     | Account research and development prospecting work                                                           | Pending  |
| Prospecting - LeadIQ            | transition to `Prospecting` -> sub field `LeadIQ`                                                            | Active   |
| Public Relations                | to be evaluated                                                                                              | Active   |
| Referral                        | to be evaluated                                                                                              | Inactive |
| Registered                      | transition to correct event type source                                                                      | Inactive |
| Request - Contact               | Filled out contact request form on GitLab website                                                            | Active   |
| Request - Professional Services | Any type of request that comes in requesting to engage with our Customer Success team                        | Active   |
| Sales                           | to be evaluated                                                                                              | Inactive |
| SDR Generated                   | Sourced by an SDR through networking or professional groups                                                  | Active   |
| Security Newsletter             | Signed up for security alerts                                                                                | Active   |
| Seminar - Partner               | not actively used - transition to `Owned Event` or `Field Event`                                             | Active   |
| SocialMedia                     | to be evaluated                                                                                              | Inactive |
| Swag Store                      | to be evaluated                                                                                              | Inactive |
| Trial - Enterprise              | In-product or web request for self-hosted Enterprise license                                                 | Active   |
| Trial - GitLab.com              | In-product SaaS trial request                                                                                | Active   |
| Unknown                         | need to evaluate what records are in this status - it should never be used                                   | Inactive |
| Unsubscribe Form                | to be evaluated                                                                                              | Inactive |
| Web                             | transition to `Web Direct`                                                                                   | Active   |
| Web Chat                        | Engaged with us through website chat bot                                                                     | Active   |
| Web Direct                      | Created when purchase is made direct through the portal (check for duplicates & merge record if found)       | Active   |
| Webcast                         | Register for any online webcast (not incl `Demo`)                                                            | Active   |
| Word of Mouth                   | to be evaluated                                                                                              | Active   |
| Zoominfo                        | transition to `Prospecting` -> sub field `Zoominfo`                                                          | Inactive |

## Lead and Contact Statuses
The Lead & Contact objects in Salesforce have unified statuses with the following definitions. If you have questions about current status, please ask in #lead-questions channel on Slack.

| Status | Definition |
| :--- | :--- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketo Qualified through systematic means |
| Accepted | Actively working to get in touch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |

## Campaigns
Campaigns are used to track efforts of marketing tactics - field events, webcasts, content downloads. The campaign types align with how marketing tracks spend and align the way records are tracked across three of our core systems (Marketo, Salesforce and Bizible) for consistent tracking. Leveraging campaign aligns our efforts across Marketing, Sales and Finance.
### System Set-up

#### Salesforce Campaigns
There are [**MASTER** Campaign templates](https://gitlab.my.salesforce.com/70161000000CnPg) created for each of the campaign types with the correct progression statuses pre-designed. When creating a new campaign, clone the template & modify the name to match the program/event. If you inadvertently set up the campaign using the wrong campaign type, you will need to update the member status (in advanced) and resync through Marketo. Marketing Ops can assist if needed.

SFDC template quick links (only available when logged in):
- [Cohort] (link to be added)
- [Conference](https://gitlab.my.salesforce.com/70161000000CnPH)
- [Field Event](https://gitlab.my.salesforce.com/70161000000CnPM)
- [Gated Content] (link to be added)
- [Geographic](https://gitlab.my.salesforce.com/7014M000001llpy)
- [Inbound Request](https://gitlab.my.salesforce.com/70161000000VwZW)
- [List Build](https://gitlab.my.salesforce.com/70161000000VwYO)
- [Owned Event](https://gitlab.my.salesforce.com/70161000000CnPR)
- [PathFactory Listener](https://gitlab.my.salesforce.com/7014M000001lmxy)
- [Referral Program] (link to be added)
- [Speaking Session](https://gitlab.my.salesforce.com/70161000000CnPW)
- [Trial](https://gitlab.my.salesforce.com/70161000000VwYT)
- [Virtual Sponsorship](https://gitlab.my.salesforce.com/70161000000CnP7)
- [Webcast](https://gitlab.my.salesforce.com/70161000000CnPb)


#### Marketo Programs

The Marketo programs for the corresponding campaign types have been prebuilt to include all the possible necessary smart campaigns, email programs, reminder emails and tokens that are to be leveraged in the building of the program.

Marketo program quick links (only available when logged in w/ `Marketing Activities` access):
- Programs to be added with links


### Campaign Type & Progression Status
A record can only progress **one-way** through a set of event statuses. A record *cannot* move backward though the statuses.

i.e. Record is put into `Registered` cannot be moved backward to `Waitlisted`


#### Cohort
A method of tracking a group (cohort) of targeted known records and/or records included in an ABM strategy. All touchpoints related to this specific campaign are excluded from Bizible tracking.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Nominated                 | ACCOUNTS/CONTACTS Sales has identified for inclusion that Marketing would otherwise be suppressing because of late-stage open opps &/or active sales cycle |        |
| Marketing List               | ACCOUNTS/CONTACTS Marketing has identified for inclusion based on the target audience, the "ABM list", demographic, etc.                    |          |
| Organic Engaged           | LEADS/CONTACTS added to the campaign through the listening campaigns that engage with the pages &/or assets for the integrated campaign that do not contain `utm_` params        | Yes      |


#### Conference
Any large event that we have paid to sponsor, have a booth/presence and are sending representatives from GitLab (example: AWS). This is tracked as an *offline* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Meeting Requested               | Meeting set to occur at conference                                                    |          |
| Meeting No Show                 | Scheduled meeting at conference was cancelled or not attended                         |          |
| Meeting Attended                | Scheduled meeting at conference was attended                                          | Yes      |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |

#### Field Event
This is an event that we have paid to participate in but do not own the registration or event hosting duties (example: Rancher event). This is tracked as an *offline* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        |          |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |

#### Gated Content
White Paper or other content offer. This is tracked as an *online* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Downloaded                      | Downloaded content                                                                    | Yes      |

#### Geographic  
We have specific geographic DMA lists that are used for field marketing and marketing programs to target event invitations. This is **not** tracked with Bizible touchpoints or channels. This campaign type is only used for visibility of our DMA lists - [click to see full list](/handbook/marketing/marketing-operations/marketo#geographic-dma-list) of DMAs available. 


| Member Status | Definition       | Success |
| :------------ | :-----------     | :------ |
| Member        | A record is a member of this DMA list | n/a this is a cohort campaign type not awarding touchpoints | n/a - this campaign type *does not* have a success metric |


#### Inbound Request
Any type of inbound request that requires follow up. This is tracked as an *online* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Requested Contact               | Filled out Contact, Professional Services, Demo or Pricing Request                    | Yes      |


#### List Build
A static list built for ad hoc requests by the FMM or MPM team. This campaign type **does not** apply any touchpoints and is **not** tracked as a Bizible channel. 

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Nominated               | Any record proactively identified by Sales to be included in the campaign                    |        |
| Marketing List                       | Any record identified by targeting filters applied by Marketing Operations to build the initial list                                             |          |
| Organic Engaged               | Occasionally used when we are wanting to track & include any records engaging with specific marketing web pages                    | Yes      |


#### Owned Event
This is an event that we have created, own registration and arrange speaker/venue (example: Gary Gruver Roadshow). This is tracked as an *online* Bizible channel.


| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |


#### PathFactory Listener
This campaign type is used to track consumption of specific PathFactory assets. This is tracked as an *offline* Bizible Channel and touchpoint. Details related to types of assets being tracked can be found on the [Marketing Operations - PathFactory](/handbook/marketing/marketing-operations/pathfactory/#listening-campaigns) page. 

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Content Consumed                   | Status when the corresponding Marketo listener picks up the contents consumption.                                  | Yes         |



#### Referral Program
This campaign type is used for our third party prospecting vendors or meeting setting services (Like BAO, DoGood). This is tracked as an *offline* Bizible Channel and touchpoints.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Target List               | Identified as persona we want to speak with                    |          |
| Meeting Set               | Vendor has set & confirmed a meeting time                    |          |
| Meeting No Show               | Scheduled meeting was cancelled or not attended                    |          |
| Meeting Attended               | Scheduled meeting at conference was attended                     | Yes      |


#### Speaking Session
This campaign type can be part of a larger Field/Conference/Owned event but we track engagement interactions independently from the larger event to measure impact. It is something we can drive registration. It is for tracking attendance at our speaking engagements. This is tracked as an *offline* Bizible channel.


| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Registered                      | Registered or indicated attendance at the session                                     |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended speaking session event                                                       | Yes      |
| Follow Up Requested             | Had conversation with speaker or requested additional details to be sent post event   | Yes      |


#### Trial
Track cohort of Trials for each product line (Self-hosted or SaaS) to see their influence. In-product trials are tracked as an **offline** Bizible touchpoint. Webform Self-hosted trials are an **online** Bizible touchpoint.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Target List               | LEAD or CONTACT record that has been identified for marketing campaign prospecting                    | Yes      |


#### Virtual Sponsorship
A virtual event that we sponsor and/or participate in that we do not own the registration but will generate a list of attendees, engagement and has on-demand content consumption post-live virtual event. This is tracked as an *offline* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing targeted email                                                              |          |
| Waitlisted                      | Holding state if registration is full will be moved to `Registered` if space opens    |          |
| Registered                      | Registered for event                                                                  |          |
| No Show                         | Registered but did not attend event                                                   |          |
| Attended                        | Attended event                                                                        |          |
| Visited Booth                   | Stopped by booth for any reason                                                       | Yes      |
| Follow Up Requested             | Requested additional details about GitLab to be sent post event                       | Yes      |
| Attended On-demand              | Watched/consumed the presentation materials post-event on-demand                      | Yes      |

#### Webcast
Any webcast that is held by GitLab or a sponsored webcast with a partner. This is tracked as an *online* Bizible channel.

| Member Status                   | Definition                                                                            | Success  |
|:--------------------------------|:--------------------------------------------------------------------------------------|:---------|
| No Action                       | default starting position for all records                                             |          |
| Sales Invited                   | Invitation/Information about event sent by Sales/SDR                                  |          |
| Sales Nominated                 | Sales indicated record to receive triggered event email sent by Marketing             |          |
| Marketing Invited               | Marketing geo-targeted email                                                          |          |
| Registered                      | Registered through online form                                                        |          |
| No Show                         | Registered but did not attend live webcast                                            |          |
| Attended                        | Attended the live webcast                                                             | Yes      |
| Attended On-demand              | Watched the recorded webcast                                                          | Yes      |


## Bizible Attribution

 In 4Q18, we are making updates to the Bizible Channel rules, but currently, these channels and subchannels are pulled into Salesforce and can be further filtered by using `medium` for those channels with overlap or with `Ad Campaign name` to search for specific UTMs or campaigns:

| Bizible Online Channel or subchannel | Type of marketing |SFDC Marketing Channel-path |
|---|---|---|
|`CPC`|Google Adwords or other Paid Search|CPC.Adwords|
|`Display`|Display ads in Doubleclick, Terminus, etc|Display|
|`Paid Social`|Ads in Facebook or LinkedIn |Paid Social.[Name of site]|
|`Organic`|Organic search|Marketing Site.Organic|
|`Other`|Not specifically defined |[Name of channel].Other|
|`Partners`|Google or events|	Marketing Site.Web Direct|
|`Email`|Nurture, Newsletter, Outreach emails|Email.[Name of email type]|
|`Field Event`|From Field event, will show Salesforce campaign as touchpoint source|Email.[Field Event]|
|`Conference`|From conference, will show Salesforce campaign as touchpoint source|Conference|
|`Social`|Any referral from any defined social media site| Social.[Name of site]|
|`Sponsorship`|Paid sponsorships, Display, and Demand gen as well as Terminus|Sponsorship|
|`Web Direct`|Unknown or direct (NOTE: this is not the same as Web direct/self-serve in SFDC, this is a Web referral where the original source was not captured)|Marketing Site.Web Direct|
|`Web Referral`|Referral from any site not otherwise defined|Marketing Site.Web Referral|

