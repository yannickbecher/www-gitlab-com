---
layout: markdown_page
title: "Parental Leave and Return Tool Kit for Managers and Team Members"
---

## On This Page
On this you will be provide tools to support parents before, during and after maternity leave, shared parental leave and
adoption leave, etc.  

 

## Manager Tool Kit
Do you have a team member leaving and want to ensure you understand how to support?  There are so many things to consider when we think about parental leave as a manager. This toolkit is for everyone who is managing someone who is expecting to become, or has recently become, a parent. 

### To Do List
* Review our [parental leave benefits/policy](https://about.gitlab.com/handbook/benefits/#parental-leave)      
* **Congratulate your team member** This is a time when many parents report feeling vulnerable, so your support will make all the difference.  Suggested things to say when you are told the news: Congratulations, How are you feeling?, How can we support you?, Do you know how to access the relevant maternity/shared parental/adoption policy?, Is the pregnancy/adoption confidential for now?
* **Virtual Baby Shower** Although we are fully distributed, there are many ways we can celebrate with our team members as people who work in office.  One of those ways could be a virtual baby shower.  
* **Arrange Out of Office Coverage** This may vary for work functions (i.e sales, etc) Consider what happens if the person needs to leave earlier than planned due to medical issues.









## Team Member Tool Kit
* Review our [parental leave benefits/policy](https://about.gitlab.com/handbook/benefits/#parental-leave)
* Slack channel to connect with other parents `#intheparenthood`.

