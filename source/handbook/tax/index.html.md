---
layout: handbook-page-toc
title: "The GitLab Tax Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
# Keep calm and love taxes!

# Contacting the Tax Team

The tax department is responsible for GitLab’s overall tax strategy including all components of tax compliance, tax planning and accounting for income taxes. Tax regulations taxation differ between countries, which can make this area complex. The tax team is here to support you, make it simple for you and guide you through the landscape of taxes. In case you have any ad-hoc questions please feel free reach out on the #tax channel on Slack. For the sake of clarity please do not use it to seek tax advice for personal matters. We will try to to our best to answer your questions on taxation of your #stock options though. For any in-depth discussions please reach out to the team.

# Organisation

The Tax Department is part of the Finance team and headed by the Director of Tax.  The Director of Tax reports to the Chief Financial Officer. For more information please check GitLab’s [Org Chart](/company/team/org-chart/). In addition, the employees responsible for entity finance functions have a ‘dotted’ lines towards the Director of Tax. The table below provides an overview of the responsibilities of the tax function in a [RADCIE-model](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#radcie):

| RADCIE Model  | Director of Tax | Controller | Everyone |
|-------------| :----------------:|:----------:| :-------:|
| Tax Planning | A / R            |            |  C / I   |
| Tax Returns | A / R             |            |  C / I   |        
| Tax Risk Management| A / R      |            |  C / I   |
| Tax Reporting | A / R           |            |  C / I   |
| Tax Accounting | A / R          |            |  C / I   |
| Tax Payments |                  |  A / R     |  C / I   |

The [Accounting and External Reporting Manager](/job-families/finance/accounting-and-external-reporting-manager/) has the responsibility to deal with statutory tax reporting. However, the Accounting and External Reporting Manager informs the Director of Tax and keeps the Director of Tax up to date on tax related subjects. In order to ensure the uniform taxation of GitLab and to avoid adverse consequences and tax risks, the Director of Tax is empowered to make recommendations for GitLab. This applies to all areas of taxation, e.g. Corporate Income Tax, Transfer Pricing, Value Added Tax, GST, Tax Accounting, Tax Audits, et cetera.

# Responsibility

The scope of the Tax Function includes, but is not limited to continuous evaluation of i) GitLab's effectiveness of GitLab’s tax planning, ii) GitLab's tax risk management and iii) GitLab's tax risk controls; while supporting the Finance team’s stated goals and objectives. In exercising its activities the responsibilities of the Tax Department can be described as follows:

* Primary responsibility for establishing GitLab’s international tax strategy and corporate structure;
* Responsible for transfer pricing policy taking into account OECD standards;
* Responsible for managing PE risks in accordance with OECD standards;
* Provide support to stakeholders for initiatives on mergers, acquisitions and corporate restructuring;
* Oversight of the tax returns for the Company’s in all operational jurisdictions;
* Responsible for completion of national and/local state income tax returns including all related analysis and support;
* Responsible for VAT, Sales, Use and Property tax functions;
* Responsible for audits of international, federal and state income tax and state and local filings;
* Responsible for accounting for income taxes (ASC 740) in the US and International subsidiaries;
* Coordination of accounting for income taxes for international subsidiaries with third party vendors;
* Responsible for building and growing team of professional and paraprofessional staff;
* Liaison with operating management on tax issues and accounting staff on tax accounting issues;
* Ensuring that appropriate internal controls are in place over accounting for income taxes;
* Departmental liaison with IT staff on all technical matters relating to tax applications;
* Ensure the Company implements and maintains controls compliant with Sarbanes-Oxley.

# Tax Procedures

As described in the responsibility section, the Tax Department supports the Finance team to reach it goals and OKR's as set by the Management. To support the goals of using the GitLab Handbook as source of truth, some of the tax procedures have been described below. There are more to come as this is an iterative process.

## Tax Procedure for Maintenance of GitLab's Corporate Structure

Maintenance of Corporate Structure covers the following procedure 

1. Management decides to change the corporate structure (e.g. for purpose of setting up an entity, merger, acquisition, other);
1. Director of Tax reviews possibilities and confirms with external consultant;
1. Director of Tax suggests roadmap for the request to change the corporate structure;
1. Director of Tax requests for proposal for services from third party service providers;
1. Director of Tax gathers input from stakeholders;
1. Director of Tax discusses input with third party service provider;
1. Director of Tax submits issue in gitlab.com to start contract approval process;
1. Director of Tax starts and manages project.

GitLab's corporate structure per August 2019 is as follows (countries and incorporation dates included):

```
GitLab Inc (USA 2014-09-10)
  ├── GitLab Federal LLC (USA 2019-01-04)
  ├── GitLab Japan (JP 2019-11-18)
  ├── GitLab South Korea (in formation)
  └── GitLab BV (NL 2014-02-19)
      ├── GitLab IT BV (NL 2019-08-07)
      ├── GitLab Ltd (UK 2016-08-31)
      ├── GitLab GmbH (DE 2017-08-09)
      ├── GitLab Pty Ltd (AUS 2018-08-06)
      ├── GitLab Canada Corp. (CA 2019-07-08)
      └── GitLab Ireland Ltd (in formation)

  ```
## Establishment of a NewCo

What are the prerequisites for a decision to establish a NewCo in a country where GitLab does not have a corporate footprint yet? It basically comes down to a combination of two factors: 

**Local presence** is a result of organic and exponential growth of the [GitLab team](/company/team/). At a given point in time the workforce in a country has grown to a substantial level where the demand for an entity is getting too large to ignore.

**Sales revenue** from certain [territories](/handbook/business-ops/#territories) where GitLab is active, can also lead to a demand from the business to establish an entity. A clear reason from tax risk management is to prevent that GitLab runs a permanent establishment risk in any of the territories.

To monitor developments and keeping stakeholders informed, weekly cross-functional meetings are held with Legal, Payroll, People Ops and Tax. FP&A is included about planned hiring for new markets.

## Tax Procedure for Compliance with Transfer Pricing

One of the consequences of GitLab's global footprint is that intercompany transactions are conducted between GitLab entities. For instance, finance team members in the USA support the German entity with daily activities; or backend engineers support development of GitLab's IP that is owned by GitLab BV. International tax law requires that intercompany transactions are performed at arm’s length. At arm's length means that GitLab's companies should agree on terms and conditions of these services that independent enterprises would also apply (i.e. pay for use of services, goods or intangibles). Intercompany transactions can be grouped into four categories:

1. Tangibles: e.g. finished goods, raw materials, fixed assets
1. Intangibles: e.g. IP, know-how, trademark, trade name
1. Financing: e.g. loans, guarantees
1. Services: e.g. management, research & development, general & administration, distribution, sales & marketing

### Intercompany Transactions at GitLab
At GitLab the supply of tangibles is non-existent while the other transaction categories are available at GitLab. The intercompany transactions between the entities are provided in the table below:

| Entity     | Intangible | Financing | Management | DS&M | R&D | G&A |
|------------|:----------:|:---------:|:----------:|:----:|:---:|:---:|
| GitLab Inc.|            | X         |  X         |  X  |  X  |  X  |
| GitLab Federal LLC|     |           |            |  X  |     |     |
| GitLab Japan|           |           |            |  X  |     |     |
| GitLab South Korea |    |           |            |  X  |     |     |
| GitLab BV   |     X     |   X       |            |  X  |     |  X  |
| GitLab IT BV|           |           |            |     |  X  |     |
| GitLab Ltd  |           |           |            |  X  |  X  |     |
| GitLab GmbH |           |           |            |  X  |  X  |     |
| GitLab PTY Ltd |        |           |            |  X  |  X  |     |
| GitLab Canada |         |           |            |  X  |  X  |     |

### Transfer Pricing Concept
#### Distribution, Sales & Marketing
GitLab BV owns the IP and therefore all GitLab entities that resell GitLab's products and services are required to pay an arm's length remuneration to GitLab BV. The remuneration is determined as follows:

1. An amount necessary to allow the GitLab entities to earn a net operating profit equal to 2.5%
1. In the event that net profit is below 2.5% of its net sales;
- remuneration shall be waived and GitLab BV shall pay a marketing subsidy so that GitLab US’s net operating profit is 2.5% of sales, or
- advances from GitLab BV to US shall be forgiven to produce the net operating profit of 2.5% of sales

#### Research & Development
For consideration of R&D services GitLab BV the arm's length remuneration is determined at cost-plus 6% on R&D costs.

#### General & Administration
For consideration for G&A services recipient entities will pay an arm's length remuneration of cost-plus 5% on G&A costs.

#### Intercompany Settlement Procedure
Each calendar month the intercompany transactions between GitLab entities are settled in accordance with GitLab's  Transfer Pricing Concept.

1. Accounting Manager prepares the balances in the Intercompany Settlement Google sheet
1. These balances derive from the AP and AR aging reports from Netsuite that are imported into the sheet
1. To the extent possible, intercompany positions in AP/AR between the entities are netted
1. The Accounting Manager sends to Controller for review and approval in the Google sheet
1. The Accounting Manager sends to Director of Tax for review and approval in the Google sheet
1. The Accounting Manager sends to CFO for review and approval in the Google sheet
1. The Accounting Manager sends to CEO for review and approval in the Google sheet
1. Payments are subsequently queued for disbursement from  the relevant bank accounts
1. Disbursements are conducted according to the Signature authorization matrix. 
1. Transaction is booked in NetSuite

## Tax Procedure for Reporting Indirect Taxes to Tax Authorities

GitLab's entities are subject to statutory reporting requirements of indirect tax in their home countries (i.e. GST and VAT).

### USA territory
In the USA the filing of GST returns is managed via [Avalara](https://www.avalara.com/us/en/index.html) software. Avalara AvaTax automatically calculates sales and use tax for transactions, invoices, and other activities registered on [Zuora](https://www.zuora.com/). State returns are automatically filed by Avalara.

### International Territory
All non-US entities are supported by local tax consultants to prepare and file indirect tax returns. The Netherlands, United Kingdom, and Australia have a quarterly reporting cycle. Germany requires a monthly reporting cycle. The GitLab Controller keeps track of the reporting deadlines.  The procedure for filing the statutory indirect tax returns is as follows:

1. Calculate VAT on Sales
  * GitLab Controller pulls the transactions from Avalara
  * The report from Avalara generates the taxable sales
  * The report from Avalara generates the VAT/GST on Sales amount
2. Calculate VAT on purchases
  * GitLab Controller exports VAT on Purchases ledger from [Netsuite](https://system.netsuite.com/pages/customerlogin.jsp)
  * GitLab Controller generates copies of all invoices relating to purchase transactions
3. Share reports and documentation from steps 1 and 2 above with tax consultant
4. Tax consultant drafts VAT return
5. Draft VAT return is reviewed by GitLab Controller
6. GitLab Controller approves or rejects draft
7. Rejection > start with step 4 // Approval > tax consultant files VAT return
8. Tax consultant provides copy of VAT return filed to GitLab Controller
9. Tax consultant informs VAT payable amount to GitLab Controller
10. GitLab Controller puts payment into banking system
11. GitLab Controller requests approval for payment from CFO
12. Once approved by CFO the VAT due is wire transferred directly to the Tax Authorities

## Tax Procedure for Reporting Taxable Gains on Option Exercises

1. Options are exercised by following this [procedure](/handbook/stock-options/#how-to-exercise-your-stock-options)
1. CFO provides options exercise file on monthly basis to Payroll & Payments Lead
1. Payroll & Payments Lead ensures wage tax compliance for employees per country as defined in the table below
1. Employee receives gain from exercise according to the common monthly salary payment procedure

Access this [page](/handbook/tax/stock-options/) for a country-by-country tax analysis when exercising GitLab stock options.

## Tax Procedure for Corporate Income Taxes to Tax Authorities

All GitLab entities have engaged tax consultants in their country of establishment that prepare and file Corporate Income Tax returns. The filing statutory filing deadlines in the countries of establishment are as follows:

| Entity  | Filing Deadline| Final Payment Deadline | Estimated Instalments Deadline |
|----------|----------------|------------------------|--------------------------------|
| GitLab, Inc.| 15 April    | By the 15th day of the 12th month of the tax year | Equal estimated instalments due on the 15th day of the 4th, 6th, 9th, and 12th month of the tax year|
| GitLab Federal LLC | See above | See above | See above |
| GitLab Japan | Within two months after the end of company's accounting period | Within two months after the end of company's accounting period | Within two months after the end of the sixth month of the corporation's accounting period |
| GitLab BV | Five months after the end of the company's fiscal year | Within two months of the date of the assessment notice | N/A |
| GitLab IT BV | See above | See above | See above |
| GitLab GmbH | July 31 | Stated on the assessment notice | Quarterly instalments due on the tenth day of March, June, September, and December |
| GitLab Ltd | January 31 | Depends on profitability > e.g. when annual taxable profit exceeds 1.5m GBP, Ltd is required to make quarterly instalment payments | Idem dito |
| GitLab Pty Ltd | 15th day of the 7th month following the end of the income year | 1st day of the 6th month following the end of the income year | Monthly or quarterly |
| GitLab Canada | Six months after the end of the fiscal year | Two months months after the end of the fiscal year | Monthly |
| GitLab South Korea | Within three months after the end of the fiscal year | Along with the filing of the return | N/A |

The procedure for filing the statutory indirect tax returns is as follows:

1. Data request from the Tax Consultant to the Accounting & External Reporting Manager
1. Accounting & External Reporting Manager informs Director of Tax on data request
1. Accounting & External Reporting Manager gathers data to provide to Tax Consultant
1. Director of Tax supports Accounting & External Reporting Manager with data gathering where necessary
1. Data sent by Accounting & External Reporting Manager to Tax Consultant
1. Tax Consultant prepares Corporate Income Tax Return
1. Draft Corporate Income Tax Return sent by Tax Consultant to Accounting & External Reporting Manager
1. Accounting & External Reporting Manager review Corporate Income Tax Return and informs Director of Tax
1. Director of Tax reviews Corporate Income Tax Return and upon approval requests approval from CFO
1. Upon approval of by the CFO the Accounting & External Reporting Manager confirms the Tax Consultant to file Corporate Income Tax Return with the local Tax Authorities
1. Tax Consultant files Corporate Income Tax Return with the local Tax Authorities
1. Upon receipt of Assessment Notice - if applicable - Tax Consultant informs Accounting & External Reporting Manager
1. Accounting & External Reporting Manager ensures payment of the Assessment Notice in case of amount payable
1. GitLab Controller puts payment into banking system
1. CFO approves payment order in banking system


## Tax Procedure for the R&D Wage Tax Credit in the Netherlands ("WBSO")

In the Netherlands a tax credit applies to qualifying research and development activities ("WBSO"). In order to qualify for the WBSO companies need to file an application with the [Netherlands Enterprise Agency](https://english.rvo.nl/subsidies-programmes/wbso). In order to qualify, it needs to be proved that the R&D activities meet the following conditions:

* the proposed R&D activities take place in your own company;
* the technological development is new to your organisation;
* the development is accompanied by technical problems;
* the R&D work has yet to take place (in other words, you must always submit a WBSO application in advance).

When the application is approved, the R&D tax credit can be deducted from the wage tax due through the monthly basis payroll. The process for the WBSO from filing the application to applying the deduction in the payroll are described below.

1. GitLab appoints a dedicated engineering manager to oversee all R&D activities in the Netherlands
1. The dedicated engineering manager drafts the project description for eligible activities and estimates the R&D hours
1. The project description is reviewed by an external [consultant](/handbook/people-group/#organizing-wbso), Finance and People Group.
1. Upon approval of the project description the external consultant files the application with the Netherlands Enterprise Agency
1. When the Netherlands Enterprise Agency approves the application for the WBSO, a grant letter is sent to GitLab BV
1. The grant letter has to be shared with the Dutch payroll agent
1. The Dutch payroll agent applies the WBSO in the monthly basis payroll
1. GitLab BV's employees working on the eligible projects have to keep track of their hours spent on the project in the [hour tracker](/handbook/people-group/#organizing-wbso)
1. The hour tracker needs to be monitored on monthly basis by Finance in order to prevent an overclaim or underclaim of the WBSO
