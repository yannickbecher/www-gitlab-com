layout: handbook-page-toc
title: "New to Leadership and Management"

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## New to Leadership and Management

In this section we will review various topics that impact new managers.  For first time managers sometimes the transition from a high performing individual to leading a team can be challenging.  One of the biggest challenges is that many new managers are taking over teams that they were once a member of and have already formed friendships.  Although it can be hard to separate your new role from your past role it is important that you make the change.  You will need to accept that you are in a different role and your mindset will have to shift from an individual contributor to manager.

# Managers are responsible for:

* Planning how to meet team goals or OKRs
* Organizing workloads amongst team members
* Hiring and staffing the team to meet goals
* Engagement and development of current team members
* Performance management and the ability to address quickly and early
* Driving an inclusive culture
* Displaying GitLab values
* Using clear and concise communication

# As a new manager you need to know:

*  Your actions or inactions are a reflection on you and the company
*  As a leader, you set the tone for your entire team 
*  The key to successful leadership is influence, not authority [Ken Blanchard](https://leaderchat.org/2010/08/11/4-keys-to-better-leadership/)
*  It's not the job you Do, it's How you Do the job.  

There are some great articles regarding being new to leadership. Please review the following links for additional reading.  

*  [15 Tips for New Managers](https://www.thebalancecareers.com/tips-for-new-managers-part-1-2275957)
*  [Becoming a Manager: What No One Tells You](https://www.jodymichael.com/blog/becoming-manager-no-one-tells/)
*  [How to Transition From Solo Expert to Effective Manager](https://www.thebalancecareers.com/transitioning-from-solo-expert-to-effective-manager-4116889)
*  [Functions of Managers](https://www.cliffsnotes.com/study-guides/principles-of-management/the-nature-of-management/functions-of-managers)
*  [The Mental Shift From Individual Contributor to Manager](https://greatmanager.co/the-mental-shift-from-individual-contributor-to-manager-df89b4421713)

## Legalities of Hiring

If you have not yet reviewed the [Hiring Pages](https://about.gitlab.com/handbook/hiring/#hiring-pages) in the handbook please familiarize yourself with the information. This page is a great overview of job families, vacancies, country hiring guidelines and etc...  A best practice once you are a manager is to schedule time with your aligned recruiter and sourcer.  In this meeting you can provide an overview of your team, potential hiring opportunities and sourcing strategies.  Recruiting alignment can be found [here](https://about.gitlab.com/handbook/hiring/recruiting-alignment/)

As a manager you are now responsible for hiring team members to fill open vacancies.  You most likely have already been involved in interviewing candidates for Gitlab, however now you are the decision maker on who is hired onto your team and into GitLab.  As a manager you should be mindful of the following:

*  Everyone on your interview team has completed the interview training at GitLab
*  You understand what to say and what not to say during an interview
*  If unsure, don't ask it
*  Don't make promises or imply anything
*  Ask consistent questions of all candidates
*  Here is a great article to reference for additional information [Keep the interview legal](https://hiring.monster.com/employer-resources/recruiting-strategies/interviewing-candidates/legal-job-interview-questions/)

## Harassment

You are responsible as a manager to create and sustain a work environment in which all GitLab team members understand and know the ethical and legal behaivor that is required of them. As a manager you need to familiarize yourself with the  [Code of Business Conduct & Ethics](https://about.gitlab.com/handbook/people-group/code-of-conduct/#code-of-business-conduct--ethics).  One issue managers may face is harrassment.  GitLab's [Harassment](https://about.gitlab.com/handbook/people-group/code-of-conduct/#harassment) section of the Code of Business Conduct & Ethics makes it clear that team members have a right to a harassment free work environment.  For further details regarding harassment please read the [Anti-Harassment Policy](https://about.gitlab.com/handbook/anti-harassment/).  As a manager if are made aware of or witness any form of harassment you must  reach out to your aligned people business parter immediately to discuss the issue.  You can find your aligned people business partner on the [People Business Partner Alignment](https://about.gitlab.com/handbook/people-group/how-to-reach-us/#people-business-partner-alignment-to-division) section of the handbook. Take a moment and review the [reporting violations](https://about.gitlab.com/handbook/people-group/code-of-conduct/#reporting-violations) section of the handbook to familiarize yourself with reporting and investigation process.  

## Discrimination

You are responsible as a manager in ensuring that all team members are treated consistently.  Please familiarize yourself with the [Discrimination](https://about.gitlab.com/handbook/people-group/code-of-conduct/#discrimination) section of the Code of Business Conduct & Ethics.  Like harassment a manager is required to immediately reach out to their aligned People Business Partner if they believe any discrimination has occurred.  As a hiring manager here a few questions you can ask yourself to ensure all team members are treated consistently:

*  Am I holding this team member to stricter or different performance and behavioral standards than their peers?
*  Am I treating this team member differently than I am treating team members with similar skills, abilities and accomplishments?
*  Have all team members been given the necessary resources to be successful?
*  Is recognition and appreciation being applied in a balanced way?

## Non-Retaliation

GitLab is committed to a work environment that is free of harassment, discrimination and non-retaliation.  Please review and familarize yourself with the [Commitment to Non-Retaliation](https://about.gitlab.com/handbook/people-group/code-of-conduct/#commitment-to-non-retaliation).  

If managers have any questions regarding their role related to the above topics, process or other related questions please schedule time with your people business partner.  Your people business parter is here to help guide you and provide counsel related to all team member related issues.  

## Diversity & Inclusion