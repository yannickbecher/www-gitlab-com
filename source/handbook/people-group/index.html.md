---
layout: handbook-page-toc
title: "People Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Role of the People Group
{: #role-peopleops}

In general, the People Group team and processes are here as a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to GitLab. On that note, please don't hesitate to [reach out](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group) with questions! In the case of a conflict between the company and a team member, the People Group works "on behalf of" the company.

## Need Help?
{: #reach-peopleops}

Welcome to the People Group handbook! You should be able to find answers to most of your questions here. You can also check out [pages related to People Group](/handbook/people-group/#other-pages-related-to-people-operations) in the next section below. If you can't find what you're looking for please do the following:

- [**The People Group**](https://gitlab.com/gitlab-com/people-ops) holds several subprojects to organize the people team; please create an issue in the appropriate subproject or `general` if you're not sure. Please use confidential issues for topics that should only be visible to GitLab team-members. Similarly, if your question can be shared please use a public issue. Tag `@gl-peopleops` or `@gl-hiring` so the appropriate team members can follow up.
  * Please note that not all People Group projects can be shared in an issue due to confidentiality. When we cannot be completely transparent, we will share what we can in the issue description, and explain why.
  * [**Employment Issue Tracker**](https://gitlab.com/gitlab-com/people-ops/employment/issues): Only Onboarding, Offboarding and Management Onboarding Issues are held in this subproject, and they are created by People Ops specialists only. Interview Training Issues, are held in the [Training project](https://gitlab.com/gitlab-com/people-ops/Training) and created by the Recruiting team. Please see the [interviewing page](/handbook/hiring/interviewing/#typical-hiring-timeline) for more info.
- [**Chat channel**](https://gitlab.slack.com/archives/peopleops); Please use the `#peopleops` slack chat channel for questions that don't seem appropriate for the issue tracker. For access requests regarding Google or Slack groups, please create an issue here: https://gitlab.com/gitlab-com/access-requests. For questions that relate to Payroll and contractor invoices please direct your question to the `#payroll_expenses`, and `#finance` channel for Carta. Regarding questions for our recruiting team, including questions relating to access, or anything to do with Greenhouse, referrals, interviewing, or interview training please use the `#recruiting` channel. For more urgent general People group questions, please mention `@peoplegeneral` to get our attention faster.
- If you need to discuss something that is confidential/private, you can send an email to the People Group (see the "Email, Slack, and GitLab Groups and Aliases" Google doc for the alias).
- If you only feel comfortable speaking with one team member, you can ping an individual member of the People Group team, as listed on our [Team page](/company/team/).
- If you wonder who's available and/or in what time zone specific team members of the People Group are, you can easily check it via [this tool](https://timezone.io/team/peopleops). If you want to use this tool for your own team or want to sign up as part of the People Group make sure to sign up [here](https://timezone.io/). When signing up please add your title to the name field, so everyone can see which role you are in. 
- If you need help with any technical items, for example, 2FA, please ask in `#it_help`. The channel topic explains how to create an issue. For urgent matters you can mention `@it-ops-team`.


## How to reach the right member of the People Group

This table lists the aliases to use, when you are looking to reach a specific group in the People Group. It will also ensure you get the right attention, from the right team member, faster.

| Subgroup                          | GitLab handle   | Email            |  Slack Group handle | Greenhouse |
|-----------------------------------|-----------------|------------------|-----------------|-----------------|
| [People Business Partners](https://gitlab.com/gitlab-com/people-ops/General) | @gl-peoplepartners | peoplepartners@domain | n/a | n/a |
| [Compensation and benefits](https://gitlab.com/gitlab-com/people-ops/Compensation) | @gl-compensation | compensation@domain | n/a | n/a |    
| [People Operations Specialists](https://gitlab.com/gitlab-com/people-ops/General) | @gl-peopleops  | peopleops@domain | @peopleops_spec | n/a |
| [People Experience Associates](https://gitlab.com/gitlab-com/team-member-epics/employment) | @gl-people-exp  | people-exp@domain | @people_exp | n/a |
| [Diversity and Inclusion](https://gitlab.com/gitlab-com/diversity-and-inclusion) | No alias yet, @mention the [Diversity and Inclusion Partner](/job-families/people-ops/diversity-inclusion-partner/) | diversityinclusion@domain | n/a | n/a |
| [Learning and Development](https://gitlab.com/gitlab-com/people-ops/Training) | No alias yet, @mention the [Learning and Development Generalist](/job-families/people-ops/learning-development-specialist/) | learning@domain | n/a | n/a |
| [Employer Branding](https://gitlab.com/gitlab-com/people-ops/recruiting) | No alias yet, @ mention the [Employer Branding Lead](/job-families/people-ops/employment-branding-specialist/) | employmentbranding@domain | n/a | n/a |
| [Recruiting](https://gitlab.com/gitlab-com/people-ops/recruiting) | @gl-recruiting | recruiting@domain | @recruitingteam | n/a |
| [Candidate Experience Specialist](https://gitlab.com/gitlab-com/people-ops/recruiting) | @gl-ces | ces@domain |@ces | @ces* |
| Recruiting Operations| @gl-recruitingops | recruitingops@domain | @recruitingops | @recruitingops |

## People Business Partner Alignment to Division

| Contact                         | Division - Department  | 
|-----------------------------------|-----------------|
| Jessica Mitchell | Marketing, Product and Meltano |
| Julie Armendariz | Finance, Legal and People |
| Carolyn Bednarz | Sales - Enterprise, Commercial and Field Operations|
| Lorna Webster | Sales - Alliances, Customer Success and Channel |
| Roos Takken | Engineering |

## People Experience vs. People Operations Core Responsibilities 

Please note that the source of truth for role responsibilites are the job families for [People Operations Specialist](https://about.gitlab.com/job-families/people-ops/people-operations/) and [People Experience Associate](https://about.gitlab.com/job-families/people-ops/people-experience-associate/). The table below is meant to provide a high-level overview of core responsibilities for each team.

| People Experience team                        | People Operations Specialist team   | 
|-----------------------------------|-----------------|
| [Onboarding](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/) | [Country Conversions](https://about.gitlab.com/handbook/contracts/completing-a-contract/#country-conversions) |
| [Offboarding](https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/) | [Relocation Conversions](https://about.gitlab.com/handbook/contracts/completing-a-contract/#relocation-conversions) |
| [Letters of Employment](https://about.gitlab.com/handbook/people-group/frequent-requests/#letter-of-emplyoment) | [Exit Interviews](https://about.gitlab.com/handbook/offboarding/#exit-survey)|

## Celebrations
{: #celebrations}

How the GitLab team celebrates work anniversaries and birthdays can be found on this [page](/handbook/people-group/celebrations)

## Frequently Requested
{: #frequently-requested}

Please review the [frequently requested section](/handbook/people-group/frequent-requests) of the People Handbook before reaching out to the team. The page includes information on accessing a team directory, requesting a letter of employment, mortgage forms, the companies reference request policy, ordering business cards, and changing your name in GitLab systems.


## Other pages related to People Operations

- [Benefits](/handbook/benefits/)
- [Code of Conduct](/handbook/people-group/code-of-conduct/)
- [Promotions and Transfers](/handbook/people-group/promotions-transfers/)
- [Global Compensation](/handbook/people-group/global-compensation/)
- [Incentives](/handbook/incentives)
- [Hiring process](/handbook/hiring/)
- [Group Conversations](/handbook/people-group/group-conversations)
- [Leadership](/handbook/leadership/)
- [Learning & Development](/handbook/people-group/learning-and-development/index.html)
- [Onboarding](/handbook/general-onboarding/)
- [Offboarding](/handbook/offboarding/)
- [OKRs](/company/okrs/)
- [People Group Vision](/handbook/people-group/people-group-vision)
- [360 Feedback](/handbook/people-group/360-feedback/)
- [Guidance on Feedback](/handbook/people-group/guidance-on-feedback)
- [Collaboration & Effective Listening](/handbook/people-group/collaboration-and-effective-listening/)
- [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-group/gender-pronouns/)
- [Travel](/handbook/travel/)
- [Underperformance](/handbook/underperformance)
- [Visas](/handbook/people-group/visas)
- [People Group READMEs](/handbook/people-group/readmes/)

## Leadership Toolkit

Our [leadership toolkit](/handbook/people-group/leadership-toolkit) has tools and information for managers here at GitLab.

## Boardroom addresses
{: #addresses}

- For the SF boardroom, see our [visiting](/company/visiting/) page.
- For the NL office, we use a postbox address listed in the "GitLab BV address" note in the Shared vault on 1Password. We use [addpost](https://www.addpost.nl) to scan our mail and send it to a physical address upon request. The scans are sent via email to the email alias listed in the "Email, Slack, and GitLab Groups and Aliases" Google doc.
- For the UK office, there is a Ltd registered address located in the "GitLab Ltd (UK) Address" note in the Shared vault on 1Password
- For the Germany office, there is a GmbH address located in the "GitLab GmbH Address" note in the Shared vault on 1Password


## Guidelines for Vendor meetings

These guidelines are for all Vendor meetings (e.g. People Group/Recruiting IT system providers, 3rd party suppliers, benefits providers) to enable us to get the best out of them and make the most of the time allotted:

Guidelines:

1. We request external vendor meetings to use our video conferencing tool so we can quickly join the call and record the session if we need to. Confirm with vendor that they agree we can record the call. The DRI for the vendor meeting will generate the zoom link and share with the vendor.

1. Decide ahead of the meeting who should be invited, i.e. those likely to get the most out of it.
 
1. Ahead of the meeting, we should agree internal agenda items/requirements/priorities and provide to the external provider.

1. In order to make the best use of time, we wish to avoid team introductions on the call, particularly where there are a number of us attending. We can include a list of attendees with the agenda and give it to the vendor before or after the meeting.

1. When a question or issue is raised on the agenda, if the person who raised it is present they will verbalize it live on the call; or if they are not present, then someone will raise it for them. This is a common GitLab practice.

1. Where possible, we request that the vendor provides their slides / presentation materials and any other related information after the meeting.

1. Do not demo your tool live, create a pre-recorded walk-through of the tool and make it available to GitLab before the meeting so we can ask questions if needed.

1. Be cognizant of using inclusive language.

1. We respectfully request that everyone is mindful of the time available, to help manage the call objectives effectively.

## NPS Surveys

NPS stands for "Net Promoter Score". GitLab has two forms of NPS surveys: eNPS for all employees (where "e" stands for "Employee"), and an onboarding NPS. These surveys gauge team member satisfaction and how likely team members are to recommend GitLab to others as a place to work.

People Ops will send out an eNPS survey twice yearly to all team members.

The onboarding NPS survey is a 30-day survey for new hires. To help People Ops understand your experience and improve the onboarding process, please complete the [onboarding survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) after you have been working for GitLab for at least 30 days. This is also documented in our [onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)

## Gifts

Managers: Should you wish to send flowers to a team member in the even of a birth, death, or other significant event. Please complete the [GitLab Gift Request form](https://docs.google.com/forms/d/e/1FAIpQLScxwCUNF-9IV-y-XNswQwkzwA-a6ahuPd8HFGEuxw3EMjukrA/viewform) to provide information about the flower request. The People Experience Associates will then send the flowers as per the submitted reqeust. This significant event must apply to the team member or to an immediate relationship to the GitLab team member. 

Other gifts can be arranged to a max value of 75 - 125 USD, please specify details on the [GitLab Gift Request form](https://docs.google.com/forms/d/e/1FAIpQLScxwCUNF-9IV-y-XNswQwkzwA-a6ahuPd8HFGEuxw3EMjukrA/viewform) or if you see fit you can allocate the cash value amount to the team member to spend and expense on something of their own choice eg. a dinner, a family activity, etc. 

The budget for sending these gifts will come from the department budget of the gift-receiving team member. To make sure the reports in Expensify are routed correctly, People Experience Associates will be sure to fill in the correct division and department. Team members' division and departmental information can be found in their BambooHR profile under the Job tab, in the `Job Information` section.

## Using BambooHR

We use [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.
We don't have one contact person but can email BambooHR if we want any changes made in the platform. The contact info lives in the Secretarial Vault in 1Password.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action, prepare a communication to the team that is approved by the Chief People Officer.

Team Members have access to their profile in BambooHR and should update any data that is outdated or incorrect. If there is a field that cannot be updated, please reach out to the People Ops Analyst with the change.

The mobile app has less functionality than the full website in regards to approvals. Only time off requests can be approved through the mobile app. If other types of requests need to be approved, those would have to be done through the desktop version of the website. 

## Using RingCentral

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
  want to make changes (for door handling, pick extension 101).
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
  the current settings which show all the people and numbers that are alerted when the listed User's
  number is dialed.
- Add the new forwarding number, along with a name for the number, and click Save.

## WBSO (R&D tax credit) in the Netherlands

For roles directly relating to Research and Development in the Netherlands, GitLab may be eligible for the [WBSO (R&D Tax Credit)](http://english.rvo.nl/subsidies-programmes/wbso).

### Organizing WBSO

**Applications**

As of 2019 GitLab must submit three applications each year and the deadlines for those are as follows:

1. **31 March 2019**, for the May - August 2019 period (Product Manager for Create Features)
1. **31 August 2019**, for the September - December 2019 period (Product Manager for Gitaly)
1. **30 November 2019**, for the January - April 2020 period (Product Manager for Geo Features)

There is a [translated English version of the application template](https://docs.google.com/document/d/15B1VDL-N-FyLe84mPAMeJnSKjNouaTcNqXeKxfskskg/edit) located in the WBSO folder on the Google Drive. The applications should be completed by a Product Manager, responsible for features or a service within GitLab, who can detail the technical issues that a particular feature will solve. Assistance on completing the application can also be sought from the WBSO consultant (based in the Netherlands). The contact details for the consultant can be found in a secure note in the People Ops 1Password vault called WBSO Consultant. The People Operations Specialist team will assist with co-ordinating this process. It is currently owned by Finance.

**Hour Tracker**

Each year a spreadsheet with the project details and hours logged against the work done on the project(s) will need to be created. This is for the entire year. The current hour tracker is located in the WBSO folder on the Google Drive, and shared only with the developers that need to log their hours (located in the Netherlands), People Operations Analysts, Finance and the WBSO consultant. Once the projects have been completed for the year, the WBSO consultant will submit the hours and project details to claim the R&D grant from the [RVO](https://english.rvo.nl/). The WBSO consultant will contact People Operations Analysts should they have any queries.


## Disaster Recovery Plan 

GitLab provides free transport to all team members to get out of the location they are in, via a vendor of choice e.g.: Uber/ Taxify/ Lyft/ Via etc. to a safe location. 
This section outlines what action will be taken during a natural disaster:
* A People Business Partner or a People Group Representative will reach out in the timezone of the team members affected by a natural disaster/ etc. via email by using this [template](https://docs.google.com/document/d/1z-iP7Tkc_B-fX4mpcoAe3GtAF-KRdJD3p96xSI14EqQ/edit) as soon as possible (usually 4 to 24 hours). A slack message will also be posted on the location slack channel and if unavailable in the #team-member-updates channel or direct message. The People Business Partner or People Group Representative should also keep the leaders of legal, security and the people group updated on their course of action and whether all team members have been reached during this process. 
* If there is no response from the team member/s within 24 hours, the People Business Partner or a member of the People Operations Specialist team will send an email to the team member/s personal email address. Due to the vast number of time zones we operate in, and for transparency, please cc peopleops@domain in all communications.
* If there is still no response after 48 hours, a People Operations Specialist will reach out to next-of-kin (this info can also be found in BambooHR) via email, and if no response, followed by a phone-call. 

