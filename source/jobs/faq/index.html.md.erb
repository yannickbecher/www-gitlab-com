---
layout: markdown_page
title: Jobs - Frequently Asked Questions
description: Browse job openings at GitLab and apply to join a very productive, ambitious team, where independence and flexibility are both valued and required.
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What's it like to work at GitLab?

GitLab's [100% remote culture](/company/culture/all-remote/) and our workplace methodologies are highly unique. You should not expect to transfer the norms of colocated corporations into a work from anywhere scenario. Those who thrive at GitLab take the opportunity to drop prior workplace baggage at the door, embrace a liberating and empowering [set of values](/handbook/values/), and **give themselves permission to truly operate differently**. 

So differently, in fact, that many of GitLab's most effective [processes](/company/culture/all-remote/management/) would be discouraged or forbidden in conventional corporations. **It's not a trap**. It's the future of work. 

Explore the resources below for a deeper understanding of [life at GitLab](/company/culture/#life-at-gitlab) — the world's largest all-remote company.

1. [GitLab's guide to starting a new remote role](/company/culture/all-remote/getting-started/)
1. [Adopting a self-service and self-learning mentality](/company/culture/all-remote/self-service/)
1. [Culture and experience as seen on the GitLab Blog](/blog/categories/culture/)
1. [GitLab's guide to making remote work](/company/culture/all-remote/)

### What our team members are saying 

Interested in hearing about life at GitLab straight from our people? Feel free to read through the reviews on our [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) and [Comparably](https://www.comparably.com/companies/gitlab) profiles. 

The freedom to work from anywhere has impacted the lives of many of our team members. Here are some of [their stories](/company/culture/all-remote/stories/).  

## How to apply for a position

First of all, thank you for your interest in joining the GitLab team! 
The best way to apply for a position is directly through our [jobs page](/jobs), where you'll find a link to our open roles. 

Don't see a position that's the right fit for you? No worries. We're growing quickly and adding new openings each week, so keep an eye on the jobs page. 

Please note that we don't retain unsolicited resumes, so you'll need to apply directly to the position you're interested in each time.

**To apply for a current vacancy:**

1. Go to our [jobs page](/jobs) and [view our open opportunities.](/jobs/apply)
1. Click on the position that interests you! Please refer to the [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) to be sure we're able to hire in your location.
1. You'll be redirected to the vacancy description and application form, where you will fill out basic personal information and provide your resume, LinkedIn profile, and/or cover letter, as well as answer any application questions. You'll also answer a voluntary Equal Employment Opportunity (EEO) questionnaire. While the EEO questionnaire has `US` in its title, it's open to all applicants from around the world.
1. Once you have finished, click "Submit Application" at the bottom. 

## The next step

Our team will respond to all applications as soon as possible. Check out our [typical hiring timeline](/handbook/hiring/interviewing/#typical-hiring-timeline) for more details on how the hiring process works. 
Once you're past the application review stage, you're welcome to contact the recruiting team at any time for an update on your application. 

In the meantime, here are a few other pages in our handbook that might help you get to know how we work at GitLab: 
- [Our values](/handbook/values/) 
- [Communication](/handbook/communication/)
- [Company strategy](/company/strategy/)
- [Our product](/product/) 

Please note that if you reach out to a GitLab team member or a company social media account about your application, even if you have already submitted it on our jobs page, you'll receive the following reply:

>Thank you for your interest in GitLab! It's best if you apply for the position directly via our [jobs page](/jobs/). This will ensure the right GitLab team member reviews your profile and gets back to you. Unfortunately, I can't refer you for the position, as we have not had a chance to work together. To ensure we stay [inclusive](/handbook/values/#diversity--inclusion), I also cannot influence your application.

## What we look for during the interview process

In addition to whether your skills and interests align with the needs of the role, there are a number of things we look for during the hiring process to determine whether you'll be successful in our unique culture and all-remote setting.

Check out [this overview](/company/culture/all-remote/hiring/#what-qualities-do-you-look-for-in-remote-hires) in the all-remote section of our handbook to learn more about the qualities we look for when hiring someone for a remote role. 


## Country hiring guidelines

At GitLab, we hire people from all over the world and all walks of life.
Diversity & inclusion is one of our [core values](/handbook/values/).
However, as an [all-remote company](/company/culture/all-remote/) we do face challenges with hiring in certain countries.
Each country has unique and complex rules, laws, and regulations, that can affect our ability to conduct business, as well as the employability of the citizens of those countries.

We are growing rapidly and continuously expanding our hiring capabilities in other geographies.
However, at this time we are unable to hire employees and contractors in the specified countries below:

<% Gitlab::Homepage::Jobs::HiringStatus.no_hiring_list.map {|no_hirable| no_hirable['country'] }.sort.each do |country_name| %>
- <%= country_name %>
<% end %>

We encourage you to continue to check our handbook as we establish our presence in other countries.

### Country hiring status

We have a more detailed [Hiring Status](/jobs/hiring-status/) page with a list of countries we have evaluated and a
summary on why we are blocked in some locations.

In countries listed in our [contract_factors.yml file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/contract_factors.yml), we have a payroll and can employ you as an employee. In all other countries, we can hire you as a contractor.

<a name="no-recruiters"></a>

## We don't accept solicitations by recruiters

At GitLab, we do not accept solicitations from recruiters, recruiting agencies, headhunters, or outsourcing organizations.
If you email us about this type of opportunity, [we'll reply](https://gitlab.com/gitlab-com/people-group/recruiting/blob/master/Templates%20for%20responding%20to%20third%20party%20agencies) with [a link to this paragraph](/jobs/faq/#no-recruiters) to indicate that we'd like to be removed from the contact list. 

