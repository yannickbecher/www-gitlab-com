---
layout: markdown_page
title: "Diversity and Inclusion"
---

## On this page
{:.no_toc}

- TOC
{:toc}


![Our Global Team](/images/summits/2019_new-orleans_team.png){: .illustration}*<small>In May of 2019, our team of 638 GitLab team-members from around the world had our annual company trip in New Orleans!</small>*

## Diversity & Inclusion mission at GitLab

Diversity & Inclusion is a fundamental body of work to the success of GitLab.  We want to include it in every way possible and in all that we do.  We strive to create a transparent environment where all globally dispersed voices are heard, and welcomed.  We additionally aim to create an environment where people can show up as their full selves each day and can contribute to their best ability.  With over 100,000 organizations utilize GitLab across the globe and we aim to have a team that is representative of our users.

We also support Diversity because it complements our other [values](https://about.gitlab.com/handbook/values/), specifically Collaboration, Efficiency and Results.
There is empirical evidence that diversity in leadership [supports innovation](https://www.bcg.com/en-us/publications/2018/how-diverse-leadership-teams-boost-innovation.aspx), [promotes better decision making](https://www.cloverpop.com/hacking-diversity-with-inclusive-decision-making-white-paper) and [improves financial results](https://www.mckinsey.com/~/media/McKinsey/Business%20Functions/Organization/Our%20Insights/Why%20diversity%20matters/Why%20diversity%20matters.ashx).

## GitLab's definition of Diversity & Inclusion

The phrase "Diversity and Inclusion" (or D&I) refers to the workplace terminology for the initiative to create a diverse workforce and an environment where everyone can be their full selves.

**Diversity** refers to characteristics of the people who make up GitLab and how they identify. Race, gender, age, ethnicity, religion, national origin, disability, sexual orientation are *some* examples of how the data might be categorized when looking at GitLab's diversity.
Sometimes we can see things that make us diverse and sometimes we can't.

**GitLab uses the term "underrepresented" and it is meant to be a way of recognizing that we need more of what we do not have so that we can be at our best.**

The context is "at GitLab" or "in a specific department or team at GitLab."
This term is generally used in the context of reporting on how GitLab is working on understanding and improving the sourcing, interviewing, hiring, and retention of those who either want to work or currently work at GitLab.
Institutes like the National Science Foundation use the word "underrepresented" when discussing research around diversity so we have chosen to use it as well in order to be able to set goals around the data we have and understand where we need to work harder.

   *  A single person **should not** be referred to as a "diverse person" or a "diversity hire" which would imply they are not included in the current community or that they are only employed because of a factor that is not directly related to their skills and their ability to do their job.
   *  People should not be singled out or "othered" by labels with cold terminology in personal interactions.

For additional information about how GitLab uses this data to make progress, please see our [handbook page](https://about.gitlab.com/handbook/incentives/#explanation-and-examples-of-underrepresented-groups) with more details.

**Inclusion** is the ability to recognize, respect, and value differences in those around us.
Additionally, it acknowledges that a company composed of a diverse group of people can lead to conflict of ideas which, if productively engaged with, can build innovation.
Inclusion also means being aware of both positive and negative biases and how those biases impact who we hire, work with, and retain.

GitLab believes that many perspectives coming together creates a more innovative environment to work in with more satisfied teammates, leading to a better product and increased profitability.

## Values

Inclusive teams are naturally more engaged, collaborative and innovative.
We aim to align [our values](/handbook/values/) to be reflective of our company wide commitment to fostering a diverse and inclusive environment.

In addition, the very nature of our company is to facilitate and foster inclusion.
We believe in asynchronous communication, we allow flexible work hours. GitLab team members are encouraged to work when and where they are most comfortable.

## Fully distributed and completely connected

The GitLab team is fully distributed across the globe, providing our team the opportunity to connect with each others cultures, celebrations and unique traditions.
We collaborate professionally and connect personally!

Our unique all-remote team opens our door to everyone.
Candidates are not limited by geography and we [champion this approach](/company/culture/all-remote/), to the extent that it’s possible, for all companies!

By having no offices and allowing each GitLab team member to work and live where they are most comfortable, GitLab offers a uniquely inclusive culture.
   * All-remote means that you [will not sacrifice career advancement](/handbook/people-group/learning-and-development/) by working outside of the office, as even GitLab executives are fully remote.
   * All-remote creates a workplace where caregivers, individuals with physical disabilities, etc. are not disadvantaged for being unable to regularly commute into an office.
   * GitLab's approach to [Spending Company Money](/handbook/spending-company-money/) enables all team members to create a work environment uniquely tailored for them.
   * All-remote enables those who must relocate frequently for family and personal reasons to take their career with them.
   * All-remote allows movement and relocation to physical settings that contribute to an individual's health (e.g. moving to a location with an improved air quality index).


Learn more about GitLab's [all-remote culture](/company/culture/all-remote/).

## GitLab team member data

Please see our [identity data](/company/culture/inclusion/identity-data).


## What we are doing with Diversity & Inclusion

*  Inclusive interviewing - We are building an inclusive workforce to support every demographic. One major component is ensuring our hiring team is fully equipped with the skills necessary to connect with candidates from every background, circumstance. We strive to ensure our hiring team is well versed in every aspect of Diversity, Inclusion and Cultural competence. We are helping the unconscious become conscious. Our number one priority is a comfortable and positive candidate experience.
   * To aid in our inclusive hiring practices, we've implemented [Greenhouse Inclusion](https://about.gitlab.com/handbook/hiring/greenhouse/#greenhouse-inclusion).
*  Inclusive benefits - We list our [Pregnancy & Maternity Care](/handbook/benefits/inc-benefits-us/#pregnancy--maternity-care) publicly so people don't have to ask for them during interviews.
*  Inclusive language - In our [general guidelines](/handbook/general-guidelines/) we list: 'Use inclusive language. For example, prefer "Hi everybody" or "Hi people" to "Hi guys". And speak about courage instead of [aggression](https://www.huffingtonpost.com/2015/06/02/textio-unitive-bias-software_n_7493624.html). Also see the note in the [management section of the leadership page](/handbook/leadership/#management-group) to avoid military analogies.
   * For an additional resource, we also have a presentation on [Inclusive Language](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)
*  We launched our Global Diversity and Inclusion Advisory Group - A team of company influencers who can be instrumental in driving D&I efforts from a global perspective.
   * We are empowering employees with Employee Resource Groups based on diversity dimensions

### ERGs - Employee Resource Groups

| ERG | Slack Channel | Sign Up |
| ------ | ------ | ------ |
| GitLab Pride |  #lgbtq | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/forum/#!forum/erg-pride) |
| GitLab Women+ |  #women | [Sign up for future meetings (google form)](https://docs.google.com/forms/d/e/1FAIpQLSdSiwRUYdEQmCRlLBydBRHmwd9R5-3OOMyVrxEHc2XedahbDA/viewform?usp=sf_link) |
| GitLab MIT - Minorities in Tech |  #minoritiesintech | [Sign up for future meetings (google form)](https://docs.google.com/forms/d/e/1FAIpQLSeISCkah2AO1dYKeXVGF6LkkahsxD4xIOc84QKz1SQpuXkZAQ/viewform?usp=sf_link) |
| GitLab DiversABILITY |  #diverse_ability | [Sign up for future meetings (google form)](https://docs.google.com/forms/d/e/1FAIpQLSfCGclrZJoLkOnd3tYpS8Z4K5-7MW7geTAv1dFgXHi0C_8RjQ/viewform?usp=sf_link) |
| GitLab Gender Minorities | #gender-minorities-employee-resource-group | [Sign up for future meetings (Google group)](https://groups.google.com/a/gitlab.com/forum/?hl=en#!forum/genderminoritieserg) |

### Speaking with ERG members in the hiring process

Our hiring process includes an **optional** step where candidates can request to meet with an ERG team member. Candidates can request this at any time throughout the process, we will also proactively offer this to a candidate when they reach the reference check stage. Whether or not the candidate decides to take us up on this offer will have no impact on our overall hiring decision.  

When a candidate requests to meet with an ERG team member, their Recruiter or Candidate Experience Specialist will share a message in the respective ERG Slack channel. To aide with scheduling, the message will include the candidate’s time zone and a request for volunteers who would be willing to speak to that person for a 25-minute Zoom call. Once a volunteer has been found the Recruiter or Candidate Experience Specialist will share the ERG members’ Calendly link, GitLab team page profile, and request the candidate book in a 25-minute call with the GitLab team member.

As a GitLab team member taking part in these calls, we advise you to start with a short introduction to you and your role here at GitLab. From here, we advise you to let the candidate lead the conversation as the goal is for you to answer their questions and offer insight into how we work.

These calls don’t require you to submit a scorecard in Greenhouse. If a candidate mentions something that you see as a red flag (e.g. they outline a past action of theirs that goes against our values) or share something that would help us set them up for success, we advise you to share the details of this with the hiring manager for the role they’re interviewing for. It will be the responsibility of the Hiring Manager to review this and decide whether we need to alter the hiring or offer process for the candidate.

* Interested in starting an Employee Resource Group or learning more?  See our [ERG Guide](https://about.gitlab.com/company/culture/inclusion/erg-guide/).

### Global Diversity & Inclusion Advisory Group

* The Global D&I Advisory Group is designed to provide greater representation of the diversity and inclusion of GitLabs team members.  The group will assist in implementing global diversity and inclusion strategy, policies and initiatives.
* Review and provide feedback on new initiatives
* Suggest/Propose and drive initiatives needed based on the region in which you reside.

> Members of the advisory board have a tag on the [team page](/company/team) and [there is also a full list](/company/culture/inclusion/advisory-group-members).
> Want to know more about how the group is guided?  Please review [the D&I Advisory group charter](https://docs.google.com/document/d/1G5OPWDQcE2yAR0R3pjoEPYXfbCwiPEJj7VAkln1B7y8/edit?usp=sharing).

### What We Are Working on with Diversity & Inclusion:

*  Creating an environment where all voices can be heard and feel comfortable speaking
*  Ensuring equal access to opportunities
*  GitLab Safe Spaces: teamwide opportunities for GitLab team members to share perspectives, concerns and different outlooks
*  Creating concrete instructions to help GitLab team members learn what they can do to reduce the impact of their biases

### Military veterans and spouses

GitLab welcomes military veterans from around the world, as well as military spouses, to learn more about [life at GitLab](/company/culture/#life-at-gitlab) and to apply for [vacancies](/jobs/). We recognize the values gained from military experience, and we foster an [inclusive atmosphere](/company/culture/all-remote/building-culture/) to thrive in when returning to civilian life.

Our [all-remote culture](/company/culture/all-remote/) provides an ideal work environment for military veterans and spouses. By empowering team members to live and work where they are most comfortable, veterans and spouses can work in a safe, nurturing environment that they [choose and design](/company/culture/all-remote/workspace/).

We encourage military veterans and spouses to [read testimonials](/company/culture/all-remote/people/#military-spouses-and-families) from GitLab team members to understand the benefits of all-remote when [joining the workforce](/company/culture/all-remote/getting-started/) following military service.

*GitLab is actively [iterating](/handbook/values/#iteration) within Diversity & Inclusion and Recruiting to ensure that additional underrepresented groups are pursued, embraced, and positioned for success.*

## Training and learning opportunities

*  [Inclusion training](https://www.youtube.com/watch?v=gsQ2OsmgqVM&feature=youtu.be)
*  Inclusive interviewing
*  Coaching for inclusion
*  Conflict resolution
*  Understanding [unconscious bias](https://about.gitlab.com/handbook/communication/unconscious-bias/)
*  Psychological workplace safety
*  Salesforce Trailhead has publicly available diversity training on topics such as [Cultivating Equality at Work](https://trailhead.salesforce.com/en/content/learn/trails/champion_workplace_equality), [Inclusive Leadership Practices](Ihttps://trailhead.salesforce.com/en/content/learn/modules/inclusive-leadership-practices), [Unconscious Bias](https://trailhead.salesforce.com/en/content/learn/modules/workplace_equality_inclusion_challenges), and [many other diversity related trainings](https://trailhead.salesforce.com/en/search?keywords=diversity)
* [Inclusive Language](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)
* [Delivering Through Diversity](https://www.mckinsey.com/~/media/McKinsey/Business%20Functions/Organization/Our%20Insights/Delivering%20through%20diversity/Delivering-through-diversity_full-report.ashx) McKinsey and Company research on Diversity and its value.
* [Salesforce Equality at Work Training](https://trailhead.salesforce.com/trails/champion_workplace_equality).
 To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.
* [Business Value of Equality.](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_diversity_and_inclusion) This module has three units. The third is specific to Salesforce values and mission and is not required or suggested for our training.
* [Impact of Unconscious Bias](https://trailhead.salesforce.com/en/trails/champion_workplace_equality/modules/workplace_equality_inclusion_challenges)
* [Allyship](https://about.gitlab.com/handbook/communication/ally-resources/) and [Equality Ally Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_ally_strategies)
* [Inclusive Leadership Practices](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/inclusive-leadership-practices)
* To be truly inclusive is to be aware of your biases as well as strategies for stopping the effects of those biases. As part of our efforts, we recommend everyone to partake in [the Harvard project Implicit test](https://implicit.harvard.edu/implicit/takeatest.html) which focuses on the hidden causes of everyday discrimination.

### Live Trainings

* [Inclusion Training](https://about.gitlab.com/company/culture/inclusion/inclusion-training/)
* [Ally Training](https://about.gitlab.com/company/culture/inclusion/ally-training/) - 2020-01-28

## Community

GitLab team members are distributed across the globe, giving us access to an array of opportunity.
We encourage collaboration with global organizations and programs that support underrepresented individuals in the tech industry.
GitLab also provides additional support through the Diversity Sponsorship program.
[GitLab Diversity Sponsorship program](/community/sponsorship/).
We offer funds to help support the event financially, and if the event is in a city we have a GitLab team member, we get hands-on by offering to coach and/or give a talk whenever possible.

## Internal to GitLab and want to learn more?

*  Stay updated via our slack channel - `#diversityandinclusion`
*  Have questions or suggestions for diversity and inclusion?  Please email `diversityinclusion@gitlab.com`
*  Monthly D&I Initiatives Company Call. This call will allow time for GitLab team members to gain an understanding of what we are doing with D&I here at GitLab. This call is the second Wednesday of every month @10am EST.
*  D&I Office Hours - This call will allow time for questions, suggestions and themed topics to discuss.  This call is the second Tuesday of every month. Both calls can be dialed into using this [zoom link](https://gitlab.zoom.us/j/7864690288).

## Definitions

*  [Gender and Sexual Orientation Identity Definitions and FAQ](https://about.gitlab.com/handbook/people-group/gender-pronouns/)
*  [Leadership](https://about.gitlab.com/company/team/structure/#organizational-chart) is defined as manager and above.
*  [Geographically](https://about.gitlab.com/company/culture/inclusion/identity-data/) is defined as those countries we use in our identity data.
*  [Women](https://gitlab.bamboohr.com/home/) are defined as how you identify in Bamboo HR.

## Performance Indicators

When measuring diversity-focused performance indicators, we focus on top-of-funnel metrics, like pipeline, because they're [leading indicators](https://www.leadingagile.com/2018/02/leading-lagging-indicators/) which are better measures of where we are heading.
It also helps reduce risk that we hire to the performance indicator, instead of hiring the best candidate.

### Women globally as a whole at GitLab (TBD %) (this includes functionally and by region)
This is calculated as the total woman at GitLab on the last day of the calendar month divided by total employees at GitLab on the last day of the calendar month.
The target is to be determined.

### Women in leadership 30% by 2022
This is calculated as the total woman in leadership at GitLab on the last day of the calendar month divided by total team members in leadership at GitLab on the last day of the calendar month.

### Women voluntary attrition (TBD %)
GitLab tracks [12 Month Voluntary Team Member Turnover](/handbook/people-group/people-group-metrics/#team-member-voluntary-turnover) companywide, but also looks at it specifically for women.
It is calculated the same way as [12 Month Voluntary Team Member Turnover](/handbook/people-group/people-group-metrics/#team-member-voluntary-turnover) but the numerator and denominator are only including only team members that identify as women in Bamboo HR.

### Pay equality
Measured by percentage "compa ratio" (+/- 2 points within 100%)

### Engagement survey inclusion score > X% (TBD)
Questions related to inclusion on the semi-annual [Engagement Survey](/handbook/people-group/engagement/) have a favorable score.
The exact target is to be determined.
