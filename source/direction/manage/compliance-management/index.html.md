---
layout: markdown_page
title: "Category Direction - Compliance Management"
---

- TOC
{:toc}

Last Reviewed: 2020-02-14

## Compliance Management: Introduction

Thanks for visiting this direction page on Compliance Management in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/720) for this category.

Compliance is a concept that has historically been complex and unfriendly. It evokes feelings of stress, tedium, and a desire to avoid the work entirely. There is often a disconnect between your company's policies and the features and settings that exist within a service provider like GitLab. Further compounding this challenge is a lack of visibility provided about the state of your account groups, projects, teams, etc within an external service or application. Compliance Management aims to bring compliance-specific data and features that focus on raising awareness and visibility of the compliance state of your GitLab groups and projects to help you make data-informed decisions about your organization's use of GitLab.

Our goal is to change the current paradigm for compliance to create an experience that's simple, friendly, and as frictionless as possible. Managing your compliance program should be easy and give you a sense of pride in your organization, not a stomach ache.

## Problems to solve
Enterprises operating in regulated environments need to ensure the technology they use complies with their internal company policies and procedures, which are largely based on the requirements of a particular legal or regulatory framework (e.g. GDPR, SOC 2, ISO, PCI-DSS, SOX, COBIT, HIPAA, FISMA, NIST, FedRAMP) governing their industry. Customers need features that enable them to comply with these frameworks beginning with defining the rules, or controls, for their GitLab environment.

Currently, there's no way for an organization to manage the compliance status for groups and projects. There's no mechanism in place for an organization to know, within GitLab, what groups or projects are subject to particular compliance requirements.

* What success looks like: GitLab administrators and group owners should be able to associate groups and projects with specific compliance frameworks, such as SOC 2, GDPR, HIPAA, SOX, etc. This association should be tied to certain compliance controls that govern how the groups and projects operate.

When customers adhere to internal or external compliance frameworks, often times a need for customization arises. One organization's process for an activity can vary greatly from another's and this can create friction or usability issues. To facilitate better usability within GitLab towards compliance efforts, we'll be introducing features that enable customers to define specific policies or requirements their users and instance should abide by.

* What success looks like: Customers will be able to specify specific rules the instance must follow at an instance, group, project, and user-level to maintain tighter control of their environments. These rules should come from "standard" settings GitLab provides and provide an option to customize those rules as necessary to suit individual customer needs.

In almost all cases, compliance controls for an organization focus on reducing overall risk, enforcing separation of duties, and implementing remediation processes. Without features to support these efforts, administrators for these organizations are faced with few workarounds: they can tolerate higher risk, lock down an instance and manage additional administrative overhead at the price of velocity, or build and maintain layers of automation on their own. For enterprises to thrive, this is a problem that demands a high-quality, native solution in GitLab.

* What success looks like: Customers should be able to transfer internal company processes to GitLab's environment to meet their needs, but without the complexity and headache of massive configuration time.

Compliance-minded organizations are data-informed and need visibility into all of their business operations to make the best decisions. Currently, GitLab does not aggregate the specific information organizations need to make these compliance decisions or monitor compliance status for their groups and projects. The information exists in many cases, but is simply not consolidated and presented in a way that makes compliance a simple, friendly task. Organizations have to dig for information in many disparate areas of the GitLab application and then need to build custom API-driven solutions to extract the data they need for internal compliance management or for reporting to auditors.

* What success looks like: GitLab provides a native, in-app experience for data and insights centered around compliance for organizations. This could manifest in various ways, such as dashboards and charts to track the progress and status for groups, projects, MRs, commits, etc. These insights should be derived by measuring activities in GitLab against specific compliance controls, such as separation of duties, access controls, SDLC policy, and more. 

## Maturity
Compliance Management is currently in the **minimal** state. This is because we don't yet have a way to associate groups and projects with specific compliance frameworks, but there are existing settings and features that support compliance use cases.

* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

In order to bring Compliance Management to the **viable** state, we will be implementing features that allow GitLab group owners and administrators to assign specific compliance controls to projects using pre-defined, sensible defaults based on existing compliance framework requirements. These controls should introduce simple, but meaningful controls to govern activity within a project, such as ensuring merge request approval rules are adhered to and cannot be bypassed without explicit approval.

* You can follow the **viable** maturity plan in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2423).

Once we've achieved a **viable** version of Compliance Management, achieving a **Complete** level of maturity will involve collecting customer feedback and reacting to the current roadmap and providing a [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) to help customers manage the compliance status of their groups and projects. Assuming we're on the right track, we'll likely scale the solution in two dimensions:

* Increase the number of settings and features you can affect by assigning a framework to a group or project. 
* Increase the comprehensiveness of "out-of-the-box" Compliace Management to incorporate evidence collection, reporting, and automation of auditing tasks.
* Improve the visibility and consolidation of compliance-relevant data points in the Compliance Dashboard.
* Simplify repeatable workflows, such as evidence collection, audit project management, and project creation in a regulated context.

Finally, once we've achieved a ruleset that's sufficiently flexible and powerful for enterprises, it's not enough to be able to define these rules - we should be able to measure and confirm that they're being adhered to. Achieving **Lovable** maturity likely means further expansion of the two dimensions above, plus visualizing/reporting on the state of Compliance Management across the instance.

## What's Next & Why

Compliance Management will initially be focused on the SOC2, SOX, and HIPAA compliance frameworks because these three frameworks appear to be some of the most common among GitLab customers. Additionally, the features we build for these frameworks will inherently add value to other organizations who are managing compliance with other frameworks due to the fundamental nature of many requirements the various frameworks share. For example, adding access control features to support HIPAA could potentially satisfy requirements for PCI-DSS and NIST.

We'll be introducing better control at the [project level](https://gitlab.com/groups/gitlab-org/-/epics/2087) with features like [controls definition](https://gitlab.com/groups/gitlab-org/-/epics/2491) and [compliance checks in merge requests](https://gitlab.com/gitlab-org/gitlab/issues/34830). We'll also focus on [group-level](https://gitlab.com/groups/gitlab-org/-/epics/2187) considerations such as [preventing project maintainers from changing critical settings](https://gitlab.com/gitlab-org/gitlab/issues/39060) like merge request approvals.

Another core focus will be on the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) where we will continue to iterate to bring necessary compliance context into a single view for easy analysis.

## How you can help
This vision is a work in progress, and everyone can contribute:

* Please comment and contribute in the linked issues and epics on this category page. Sharing your feedback directly on GitLab.com is the best way to contribute to our vision.
* Please share feedback directly via email, Twitter, or on a video call. If you’re a GitLab user and have direct knowledge of your need for compliance and auditing, we’d especially love to hear from you.
* Join our [Compliance Special Interest Group (SIG)](https://gitlab.com/gitlab-org/ux-research/issues/532) where you have a direct line of communication with the PM for Manage:Compliance
