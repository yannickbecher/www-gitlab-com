---
layout: markdown_page
title: "Product Section Direction - CI/CD"
---

{:.no_toc}

- TOC
{:toc}

## CI/CD Overview

The CI/CD section focuses on the following stages of the [DevOps Lifecycle](/stages-devops-lifecycle/):

- Code build/verification ([Verify](/direction/cicd#verify))
- Packaging/distribution ([Package](/direction/cicd#package))
- Software delivery ([Release](/direction/cicd#release))

You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](/images/cicd/gitlab_cicd_workflow_verify_package_release_v12_3.png "Pipeline Infographic")

CI/CD represents a large portion of the DevOps market with significant growth upside, and is also the gateway to the rest of our [Ops](/direction/ops) stages and features inside of GitLab. As of late 2019, the application release automation/CD and configuration management (mainly aligned to the Release stage) segment supported 984.7MM USD revenue in 2019, projected by 2022 to grow to 1617MM. The Continuous Integration, integrated quality/code review/static analysis/test automation, and environment management (mainly aligned to the Verify stage) represented an even larger 1785MM market in 2019, projected to grow to 2624MM. With a combined projected total of 4241MM by 2022, it's critical that we continue to lead here. __([market data, GitLab internal link](https://docs.google.com/spreadsheets/d/14j-C-9AzSRI2pEvX2zh42O34Y_EgDocEqffwv_KyJJ4/edit#gid=0))__

Team size and tracking against plan can be seen on our [hiring chart](https://about.gitlab.com/handbook/hiring/charts/cicd-section/). Maturity for each of the stages can be found on our [maturity page](/direction/maturity).

## Competitive Space and Positioning

CI/CD solutions have continued to rapidly innovate. Kubernetes itself, a massive driver of industry change, just turned five years old in 2019. Docker, now considered a mature and foundational technology, was only released in 2013. We do not expect this pace to slow down, so it's important we balance supporting the current technology winners (Kubernetes, AWS, Azure, and Google Cloud) with best-of-breed, built-in solutions, while at the same time avoiding over-investment on any single technology solution that could become legacy. Our [Multi-Platform Support theme](/direction/cicd/#multi-platform-support) highlights the technologies we're monitoring and investing in.

There are dangers to falling out of balance; Pivotal resisted embracing Kubernetes early and has [suffered for it](https://fortune.com/2019/07/29/ford-pivotal-write-down/). At the same time, technologies like Jenkins that deeply embraced what are now considered legacy paradigms are enjoying the long tail of relevancy, but are having to [scramble to remain relevant for the future](https://jenkins.io/blog/2018/08/31/shifting-gears/).

In summary, this space is highly competitive with large competitors moving towards a single application, and smaller competitors innovating nimbly on features. GitHub in particular has a strong product positioning and adoption, and is our rival that we need to monitor most closely. Spinnaker (and to a certain extent Drone) are doing very well innovating on CD technology. Spinnaker in particular is growing fast and enabling [collaborative CI/CD workflows using automation](https://opensource.com/article/19/8/why-spinnaker-matters-cicd) similar to what we want to provide; we need to mature our CD offering before they become dominant by ensuring there is no reason to prefer Spinnaker over our own solutions. At the same time, some competitors such as Jenkins are falling behind in relevance. We have an opportunity to replace these kinds of tools with our own more modern solution, and we need to ensure that we make this as simple as possible.

For Spinnaker you can read our strategy and competitive analysis on the [direction page for Continuous Delivery](/direction/release/continuous_delivery/#spinnaker). Against Jenkins, the equivalent content is avilable on the [direction page for Continuous Integration](/direction/verify/continuous_integration/#cloudbees-jenkinscodeship)

## Strategy

Our strategy for CI/CD right now is all about enabling easy-to-discover workflows that support doing powerful, complex CI/CD actions with a minimum of manual configuration. We want to take advantage of our single application so that, while each team may have their own views or dashboards in the product that support their day to day, the information about what is deployed where is available everywhere and to everyone, embedded naturally into the product where it's relevant. For example, a person thinking about upcoming releases may interact mostly with an environment-based overview that helps them see upcoming changes as they flow environments, but that same information exists everywhere it is relevant: 

- Testers looking at an individual issue can see which environment(s) that issue has been deployed to
- Developers reviewing a merge request have the Review App at their fingertips
- Feature flags link back to the issues and merge requests that introduced them for context
- Upcoming releases have burndown charts right in the overview
- Evidence collection for auditors happens automatically throughout the pipeline, with nothing special to set up

The end result is that even complex delivery flows become part of everyone's primary way of working. There isn't a context shift (or even worse, a switch into a different tool) needed to start thinking about delivery - that information is there, in context, from your first commit. The centerpiece of this strategy is our [Get Things Done Easily theme](/direction/cicd/#get-things-done-easily).

### Monthly Q&A Video

We have a monthly internal Q&A where we discuss strategy and how things are going. You can see the latest video below:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8wpJ7EmGx9s" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Plan & Themes

Given our vision and strategy, the top priorities in our plan are:

- Continue to double down on the strengths in our section, effectively competing with the companies in our market. Our strengths and opportunities are enumerated in the themes below, and this would also include making it easy to move to GitLab CI/CD from other products.
- Get to product leadership in CD through two main paths: credible competition with products like Spinnaker, and to build the first complete Progressive Delivery solution.
- Build flows in the product that help connect the dots between SCM, CI, Packaging, and CD. We will work on our user experience funnel, figuring out where people are dropping off and losing the thread that results in them not getting the most out of GitLab. We know for example we are undeserving non-k8s container deployments.

To achieve this we have organized around several themes that support our direction. These are listed in rough priority order below:

<%= partial("direction/cicd/themes/cool_things") %>

<%= partial("direction/cicd/themes/progressive_delivery") %>

<%= partial("direction/cicd/themes/multi_platform") %>

<%= partial("direction/cicd/themes/integrated_solutions") %>

<%= partial("direction/cicd/themes/speedy_pipelines") %>

<%= partial("direction/cicd/themes/compliance_as_code") %>

### What we aren't working on

Due to competing priorities it's not possible to deliver all the wonderful features that we would otherwise want to. We are an open core company, so contributions on these topics are welcome, but given where we are now we don't currently see these making our one year plan.

On the Release side, a [Service Now Integration](https://gitlab.com/gitlab-org/gitlab/issues/8373) to control rollbacks or other pipeline actions is not in the cards. Similarly, Pages improvements such as the one to [improve experience around GitLab Pages forking vs. templates ](https://gitlab.com/gitlab-org/gitlab/issues/197172) will not be delivered in the next twelve months, and although it would be great, [Mobile Publishing](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Arelease%20management&label_name[]=mobile%20publishing) will likely also not be leveled up, along with other mobile-focused features such as feature-flags ([gitlab#24984](https://gitlab.com/gitlab-org/gitlab/issues/24984)) or review apps specifically for mobile ([gitlab#199108](https://gitlab.com/gitlab-org/gitlab/issues/199108), [gitlab#20295](https://gitlab.com/gitlab-org/gitlab/issues/20295), and [gitlab#33300](https://gitlab.com/gitlab-org/gitlab/issues/33300)).

For Package, the [Dependency Firewall](/direction/package/dependency_firewall) issues likely to come later as we focus on making the [Dependency Proxy](/direction/package/dependency_proxy) itself functional and complete. Package manager formats and features that are not NPM, Maven, C++, .NET, Linux, Ruby, Python, PHP are also not top of the list unless they come from a community contribution. If you're interested in contributing one of these, we are working to make this as easy as possible so please let us know and we will jump in to help.

When it comes to Verify, features like [tracking accessibility results over time](https://gitlab.com/gitlab-org/gitlab/issues/36171), [directly connecting the IDE to the Visual Review App](https://gitlab.com/gitlab-org/gitlab/issues/119127), and [automatic flaky test minimization](https://gitlab.com/gitlab-org/gitlab/issues/3583) will not be part of our one year plan. For the Runner, we have an incredible number of multi-platform targets we are going after, and realistically z/OS won't be able to fit in.

In general, we're also taking a "wait and see" approach with GitOps in terms of new product features or rethinking fundamental aspects of the pipeline. That isn't to say you can't achieve GitOps workflows in the product today (see our [multi-part series on the topic](/blog/2019/11/04/gitlab-for-gitops-prt-1/) for more details there), but it does mean that we aren't going to be pushing all of our users into a GitOps way of working at this point.

## Stages & Categories

The CI/CD section is composed of three stages, each of which contains several categories. Each stage has an overall strategy statement below, aligned to the themes for CI/CD, and each category within each stage has a dedicated vision page (plus optional documentation, marketing pages, and other materials linked below.)

<%= partial("direction/cicd/strategies/verify") %>

<%= partial("direction/cicd/strategies/package") %>

<%= partial("direction/cicd/strategies/release") %>

## What's Next

It's important to call out that the below plan can change any moment and should not be taken as a hard commitment, though we do try to keep things generally stable. In general, we follow the same [prioritization guidelines](/handbook/product/product-management/process/#prioritization) as the product team at large. Issues will tend to flow from having no milestone, to being added to the backlog, to being added to this page and/or a specific milestone for delivery.

<%= direction["all"]["all"] %>
