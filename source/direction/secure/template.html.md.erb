---
layout: markdown_page
title: Product Section Direction - Secure
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Proactively identify vulnerabilities and weaknesses to minimize risk</b>
    </font>
</p>

GitLab Secure enables accurate, automated, and continuous assessment of your applications and services enabling you to proactively identify vulnerabilities and weaknesses to minimize your security risk.  Secure is not an additional step in your process nor an additional tool to introduce to your process. it is woven into your DevOps cycle thus allowing you to adapt your security testing and processes to your developers (and not the other way around).

![Secure Overview](/images/direction/secure/secure-overview.png)

## Section Overview

The Secure Section focuses on identifying security findings (e.g., vulnerabilities and weaknesses) within applications and services prior to moving them to operations. Furthermore, Secure can (and will) provide security visibility for applications and services already deployed to production. Secure’s goal is to proactively identify vulnerabilities and weaknesses before they are exploited.  This is done by:

* Applying a proactive approach to security with a focus on identifying security findings
* Analyzing applications and services including cloud-native infrastructure for security vulnerabilities and weaknesses
* Leveraging common methods including ethical hacking and fuzz testing as well as static and dynamic analysis

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/0dpwm0VwdAg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Groups

The Secure Section is made up of one DevOps stage, Secure, and six groups supporting the major categories of DevSecOps including:

* Static Analysis - Assess your applications and services by scanning your source code for vulnerabilities and weaknesses.
* Dynamic Analysis - Assess your applications and services while they are running by leveraging the [Review App](https://docs.gitlab.com/ee/ci/review_apps/) available as part of GitLab’s CI/CD functionality.
* Composition Analysis - Assess your applications and services by analyzing dependencies for vulnerabilities and weaknesses, confirming only approved licenses are in use, and scanning your containers for vulnerabilities and weaknesses.
* Fuzz Testing - Assess your applications and services by inputting unexpected, malformed, and/or random data to measure response or stability by monitoring for unexpected behavior or crashes.
* Attack Surface - Assess your applications, services, and cloud-native infrastructure for vulnerabilities and weak configurations (e.g., expired or weak SSL/TLS settings).
* Vulnerability Research - Leverage GitLab research to empower your Secure results by connecting security findings to industry references like [CVE IDs](https://cve.mitre.org).

### Resourcing and Investment

The existing team members for the Secure Section can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=secure-section)
* [User Experience](https://about.gitlab.com/company/team/?department=secure-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=secure-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=secure-enablement-qe-team)

## 3 Year Section Themes

### Security Is A Team Effort

Everyone benefits when security is a team effort as testing happens regularly, issues are found earlier, and code shipped is more secure.  To make this possible, security must be approachable, not overburden teams, and results must be easy to interpret.  Security testing tools and processes must be [adapted to your developers](https://www.gartner.com/document/code/450792?ref=ddisp&refval=450792) (and not the other way around).  This means bringing security into the workflow of your developers such that they can stay within their context without having unnecessary steps added to their daily work.  Furthermore, results provided as security findings must be presented in a way that they can be interpreted without needing a PhD in cybersecurity.  This includes providing enough detail to begin identifying the root cause (including identifying the section of code causing the security finding) and suggesting remediation steps (including auto-remediation) as well as pointing to industry standards related to the security finding.  By implementing an integrated DevSecOps lifecycle with actionable results, security becomes everyone’s responsibility.

As examples, GitLab will provide:
* Security findings within the merge request view allowing developers to stay within their context.
* Security gates enabling the ability to require approval to merge when security findings are above a pre-set severity.
* Industry references with security findings to aid in root cause analysis and remediation.
* Auto-remediation, where applicable, enabling faster resolution of security findings.
* Summary views, or dashboards, showing the overall health of the project, group, and instance enabling security teams to contribute and interact with development teams.
* Historical reporting for security findings such that root cause can be identified and addressed, preventing recurring issues.

### Shift Left. No, More Left Than That.

The “shift left” approach is not a new concept within software testing and DevOps best practices.  It is commonly thought of when discussing the [DevSecOps lifecycle](https://www.devsecops.org/blog/2016/5/20/-security). This usually includes security testing earlier in the software development lifecycle with the goal of identifying security vulnerabilities and weaknesses prior to shipping code to operations.  Today’s techniques include static application security testing (SAST), dynamic application security testing (DAST), interactive application security testing (IAST), dependency scanning, and license compliance.  The continuation of the “shift left” approach requires a harder shift left, bringing security testing as close as possible to the developer.  This enables earlier detection of security vulnerabilities and weaknesses, thus lowering the cost of remediation (as well as reducing work for the entire team as security findings are addressed prior to reaching the QA and security teams).

As examples, GitLab will provide:
* Static analysis beyond traditional SAST functionality, enabling more ways to [white-box test](https://en.wikipedia.org/wiki/White-box_testing) source code.
* Expanded identification functionality, enabling secret detection beyond API tokens, passwords, and cryptographic keys with improved secrets management leveraging cloud-native technologies.
* Real-time feedback to developers via integrated development environments (IDEs), enabling developers to write secure code prior to code commit.

### Shift Right. Yes, Right. Right Into Operations.

Security testing doesn’t stop once code is shipped.  New vulnerabilities, security weaknesses and, attacker techniques are constantly discovered, leaving operations and their associated applications and services open to being compromised.  Also, as organizations continue to shift to the cloud and employ cloud-native strategies, new attack surfaces are exposed that did not exist within the traditional data center.  These include items like cloud storage permissions and unwanted network services. Applications, services, and their associated cloud-native infrastructure need to be assessed just as a user (or an attacker) would interact with them.  This means performing the same tasks that an attacker would perform including reconnaissance, vulnerability assessment, and penetration testing.  Implementing a continuous assessment strategy of operations is needed to provide full visibility into all potential risk.

As examples, GitLab will provide:
* Dynamic analysis categories, including DAST and DAST API, providing the ability to assess the operations side of the DevOps lifecycle.
* New categories purposefully designed to assess the operations network including automatic penetration testing and vulnerability assessment functionality.
* Cloud-native assessment capabilities allowing users to truly understand their attack surface.
* Active monitoring of public key infrastructure (PKI), providing full visibility into the cryptographic health of your cloud-native environment (and associated containers).

### Provide Active Intelligence to Enable Data-Driven Decisions

Security findings, without context, can lead to making incorrect decisions on remediation, leaving applications and services vulnerable. With more data made available, better decisions can be made while prioritizing security findings, enabling users to best manage their security risk.  Furthermore, this enables developers to write secure code, build operations to package dependencies free of vulnerabilities, and security team members to test deeper than previously achievable.  Leveraging machine learning provides active intelligence, enabling users to make smart, data-driven decisions at the right time, lowering overall security risk.  Machine learning, to be successful, relies on big data to build accurate models. The GitLab community includes developers, build operators, and security teams all working together within their organizations to code, build, deliver and secure application and services going into operations.  As a global community, developers can learn from each other to identify and better secure code. Dependency vulnerabilities and weaknesses can be avoided, and security teams can test smarter, faster, and deeper.  Machine learning, powered by anonymized data, provides active intelligence enabling data-driven decisions.

As examples, GitLab will provide:
* Machine learning that will be opt-in when it relates to using anonymized user data from the GitLab community.
* Actionable intelligence to identify insecure coding practices to help developers write more secure code in real-time.
* Dynamic application security testing (DAST) tailored to the application or service being tested, identifying security findings which may have been missed with traditional testing techniques.
* Real-time data about software vulnerabilities and weaknesses related to technologies used within users’ projects, groups, and instances.
* Auto-remediation of discovered software vulnerabilities and weaknesses within project source code, dependencies (e.g., libraries and packages), and containers.
* Autogeneration of signatures and policies to address security findings within Secure Section categories that can be automatically applied to Defend Section categories.

### BYOT - Bring Your Own (Security) Tools

At GitLab, we believe everyone can contribute and security is everyone’s responsibility.  We empower GitLab users by providing security tools which include Static Analysis, Dynamic Analysis, Container Scanning, Dependency Scanning, and License Scanning.  However, we recognize our users may have existing security tools and may want to continue to use them.  As such, we strive to play well with others.  Security tool vendors will be provided standardized components (e.g., APIs) within the GitLab complete DevOps platform enabling easy integration into our Security Dashboard and Merge Request controls including security approvals.  This allows our users to have alternate security tools that either replace, or augment, our Secure tools.

As examples, GitLab will provide:
* A walkthrough (e.g., wizard) enabling users the ability to configure and enable integrations they wish to use as part of their DevSecOps processes.
* Standardize REST APIs, following CRUD principles, for integrating third-party security scanners the ability to start scanning via GitLab CI/CD as well as submit their security findings into the GitLab complete DevOps platform.
* Security vulnerability reporting framework, following JSON notation, allowing third-party security scanners to submit their security findings in such a way that their results can be integrated into Secure’s Security Dashboard and Merge Request controls including security approvals.

If you are interested in [contributing](https://docs.gitlab.com/ee/development/contributing/) such an integration, please [create an issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new?nav_source=navbar) so we can collaborate on questions and adding any enabling functionality.

## 3 Year Strategy

In three years the Secure Section market will:
* Have the application security testing (AST) market continue to grow, adding new techniques into its definition
    * This includes a bigger focus on API readiness with API fuzzing and security testing growing in popularity
    * See a resurgence of security testing within operations / production
* Continue the "Shift Left" trend, with Static Analysis (SAST) becoming more prominent
* Adopt machine learning techniques within the development phases of the DevOps lifecycle

As a result, in three years, Gitlab will:
* Identify insecure coding practices in real-time and provide recommendations to correct them prior to merge request
* Provide fuzz testing across the entire application technology stack (protocol, API, application), including both black-box and white-box testing techniques
* Expand Secure to support testing in development as well as in operations / production
    * Introduce categories that enable continuous verification (e.g., hourly, daily, weekly) of operations / production
* Expand focus within preexisting categories to apply knowledge / results from one scan type to optimize another scan type's configuration

## 1 Year Plan

### What's Next for Secure

To meet our [audacious goals](https://about.gitlab.com/company/strategy/#big-hairy-audacious-goal), the Secure Section will focus on the following over the next 12 months:
* **Application Security Testing (AST) Leadership** - We will take a leadership position within the Application Security Testing (AST) market.  This will be accomplished by focusing on moving [Static Analysis (SAST)](https://about.gitlab.com/direction/secure/#sast), [Dynamic Analysis (DAST)](https://about.gitlab.com/direction/secure/#dast), and [Dependency Scanning](https://about.gitlab.com/direction/secure/#dependency-scanning) categories to [Complete](https://about.gitlab.com/direction/maturity/) maturity as well as introducing a [Fuzz Testing](https://about.gitlab.com/direction/secure/#fuzz-testing) category and bringing it to [Viable](https://about.gitlab.com/direction/maturity/) maturity.
* **Dogfooding** - We will [“practice what we preach”](https://www.dictionary.com/browse/practice-what-you-preach), including leveraging Secure Categories in all things GitLab does.  This tight circle will provide immediate feedback and increase our rate of learning.
* **Security for everyone** - In order to make security accessible to everyone across the DevOps lifecycle, we will bring all Secure OSS scanners to Core (self-hosted) / Free (GitLab.com).
* **API first** - API proliferation will continue as more companies focus on ways to integrate technologies.  As such, we will focus on security testing of APIs including verifying standards like the [OWASP Top 10](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project) and [API fuzz testing](https://gitlab.com/gitlab-org/gitlab/issues/33906).
* **Historical trending** - Provide a focus on identifying patterns in security findings with a goal of helping everyone code securely.  Make recommendations on remediation with a goal of providing automatic remediation wherever possible.
* **Provide Dynamic Analysis in production** - Enable Dynamic Analysis Categories to empower users to scan and assess applications and services deployed to production / operations.
* **Machine learning** - Machine learning (ML) will be leveraged, as part of static analysis, to identify insecure coding practices and help developers write more secure code.  This will be opt-in and will enable the power of the GitLab global community.
* **Integrations** - Enable third parties to easy integrate their security solutions into Ultimate (self-hosted) / Gold (GitLab.com). This includes providing APIs and a standard reporting framework so everyone can bring their preferred security tools into Ultimate (self-hosted) / Gold (GitLab.com).
* **Differentiate on value** - Running a security test is just the beginning.  We want to provide a first-class experience and enable users to make data-driven decisions to secure their applications and services as well as their enterprise.  Our [Security Dashboards](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboard-ultimate) and [Merge Request Approvals](https://docs.gitlab.com/ee/user/application_security/index.html#security-approvals-in-merge-requests) is just the beginning.

### What We're Not Doing

The following will NOT be a focus over the next 12 months:
* **Protocol fuzzing** - Fuzzing the entire application technology stack is part of Secure’s 3 Year Strategy; however, we will focus on applications and APIs first.  The shift to protocol fuzzing will occur as Viable and Complete [maturities](https://about.gitlab.com/direction/maturity/#legend) are achieved on DAST and API security testing.
* **Security services** - The cybersecurity staffing shortage [continues to grow](https://www.forbes.com/sites/martenmickos/2019/06/19/the-cybersecurity-skills-gap-wont-be-solved-in-a-classroom/#79a380dd1c30) with no solvable solution yet defined.  To solve this issue, organizations have been relying on security services to fill this gap in their security processes.  As part of Secure’s 3 Year Strategy, we want to address this for the GitLab community by offering cybersecurity augmentation powered by GitLab Secure categories.
* **Responsible disclosure** - GitLab will become both a CVE Numbering Authority (CNA) for GitLab applications as well as for researchers and technologists to use when reporting new vulnerabilities (within any application, service, or operating system).  As part of Secure’s 3 Year Strategy, we want to support the entire vulnerability lifecycle to enable ease-of-use when it comes to [responsible disclosure](https://en.wikipedia.org/wiki/Responsible_disclosure).

Please explore the individual [Category](https://about.gitlab.com/direction/secure/#categories) Direction pages for more information on 12 month plans.

## Target audience

GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the [opportunities](#opportunities) listed above, the Secure section has features that make it useful to the following personas today.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟨 SecOps Teams
1. 🟨 QA engineers / QA Teams
1. ⬜️ Security Consultants

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables collaboration between developers, security teams, SecOps teams, and QA Teams.
1. 🟩 Developers / Development Teams
1. 🟩 Security Teams
1. 🟩 SecOps Teams
1. 🟩 QA engineers / QA Teams
1. 🟨️ Security Consultants

### Developers

Nonetheless, we want to support developers and provide feedback during the
application development.
[**Security Reports**](https://docs.gitlab.com/ee/user/project/merge_requests/#security-reports)
in merge request widgets and pipelines allow early access to security
information that can be used to fix problems even before they are merged into
the stable branch or released to the public, embracing the idea of [shift left testing](https://en.wikipedia.org/wiki/Shift_left_testing).

This approach is valuable to highlight how specific changes could affect the
security of the application.

Personas
* [Sasha - Software Developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Delaney - Development Team Lead](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)

### Security teams

We want to support security teams as first class citizens. GitLab should be
their primary tool to manage monitoring and remediation of security issues.
Using the **Security Dashboard** security specialists know exactly which is the
most important thing they need to take care of, while Directors of Security can
manage workflows and analyze historical data to figure out how to improve the
response time.

This is a vulnerability-centric approach where items are grouped and ordered to
suggest what's most important in a group, or in the entire instance.

Personas
* [Sam - Security Analyst](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)
* [Alex - Security Operations Engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#alex-security-operations-engineer)


<%= partial("direction/categories", :locals => { :stageKey => "secure" }) %>


## Other top-level features

In the Secure stage there are features that are cross-category. They allow Security
Teams and Developers to manage security with a holistic approach.
The prioritization of a security issue doesn't fully depend on the type of
vulnerability, but on its severity and so which is the impact on the application.

That's why we want to create features where vulnerabilities are in one single place,
no matter if they are coming from SAST, Dependency Scanning, Container Scanning, etc.

### Auto Remediation

When a vulnerability is automatically detected by GitLab, users can be aware
of that using the Security Dashboard or looking at security reports. But this
still requires manual intervention to create a fix and push it to production,
and during this time there is a vulnerable window where attackers can leverage
the vulnerability.

Auto Remediation aims to automate vulnerability solution flow, and automatically create
a fix. The fix is then tested, and if it passes all the tests already defined
for the application, it is deployed to production.

GitLab can then monitor performances of the deployed app, and revert all the
changes in case performances are decreasing dramatically, warning the user
about the entire process and reducing the need for manual actions.

Read more in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/759).

<figure class="video_container">
   <iframe width="560" height="315" src="https://www.youtube.com/embed/ZgFqyXCsqPY?start=4216" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

Slides are available [here](https://docs.google.com/presentation/d/1VuUKRnMlISEK4ECPaJVOYv3Pom4Zh6JXBFbD58RuibI/edit#slide=id.g4286011fc8_8_0).

### Security Dashboards

Security Dashboards, available at [group](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
and [project](https://docs.gitlab.com/ee/user/project/security_dashboard.html) level,
are the primary tool for Security Teams and Directors of Security. They can use those
dashboards to access the current security status of their applications, and to start a
remediation process from here.

The dashboard is also giving stats and charts to figure out how the team is
performing to keep the security level to a proper level.

### Bill of materials

Software is often based on many components that are reused. Every modern programming
language makes it easy to pull and use external libraries via package managers.
There is a growing need to know exactly what is included in the final app, and the
relevant information about those third-party components, like the version number,
the license, and the security status.

The bill of materials (BOM) makes this information available and accessible, so
compliance can perform validation that the app can be released and deployed.

Read more in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/858).

### Air-gapped (or offline) environment support

Users of [GitLab Self-Managed](https://about.gitlab.com/install/) may have installations with limited or no internet connectivity.  These environments might be in their lab, their datacenter, or even in their preferred cloud provider.  GitLab refers to these deployments as [air-gapped environments](https://en.wikipedia.org/wiki/Air_gap_(networking)) or offline deployments however other common names include limited connectivity environment, local area network (LAN) environment, or intranet environment.  These environments have physical barriers preventing internet access or potentially have security policies (e.g., firewalls) which limit access.

We are actively working to allow users to run all of our Secure categories in physically disconnected networks. This will enable users within the other environment types like limited connectivity to be successful using Secure categories by following the same documentation and use cases.

Learn more about this in [GitLab Docs](https://docs.gitlab.com/ee/user/application_security/offline_deployments/) as well as in the [epic](https://gitlab.com/groups/gitlab-org/-/epics/1359) for Secure's current status supporting these types of environments.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "secure" }) %>

<p align="center">
    <i><br>
    Last Reviewed: 2020-03-17<br>
    Last Updated: 2020-03-17
    </i>
</p>
