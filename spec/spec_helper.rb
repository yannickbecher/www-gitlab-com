$LOAD_PATH << File.expand_path('..', __dir__)

require 'middleman-core'
require 'middleman-core/rack'

require 'middleman-autoprefixer'
require 'middleman-blog'
require 'middleman-syntax'

require 'lib/bamboohr'
require 'lib/changelog'
require 'lib/gitlab/file_cache'
require 'lib/homepage'
require 'lib/redirect'

require 'spec/support/capybara'

RSpec.configure do |config|
  config.define_derived_metadata(file_path: %r{/spec/features}) do |metadata|
    metadata[:feature] = true
    metadata[:type] = :feature
  end

  # Really slow, only do it once, and only when needed
  config.before(:suite) do
    # Skip if there are no examples with `feature` tag
    next if config.world.example_groups.none? { |example| example.metadata[:feature] }

    root = File.expand_path(File.join(File.dirname(__FILE__), ".."))
    Middleman::Logger.singleton(Logger::WARN, false)

    if ENV['CAPYBARA_LOCALHOST'].try(:match?, /^(true|yes|1)$/i)
      # Run tests against already-running localhost, instead of
      # (very, very slowly) starting Middleman under the default driver
      Capybara.current_driver = :selenium
      Capybara.app_host = 'http://localhost:4567'
      Capybara.run_server = false
    else
      middleman_app = ::Middleman::Application.new do
        set :root, root
        set :environment, :test
      end
      Capybara.app = Middleman::Rack.new(middleman_app).to_app
    end

    Capybara.save_path = "tmp/capybara"
    Capybara::Screenshot.instance_variable_set(:@capybara_root, File.expand_path(Capybara.save_path, root))
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.order = :random
  Kernel.srand config.seed

  config.filter_run focus: true
  config.run_all_when_everything_filtered = true
end
